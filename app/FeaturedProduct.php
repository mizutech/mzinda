<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class FeaturedProduct extends Model {

	//
	protected $fillable = ['product_id','style'];
	
	
	public function products(){
		return $this->hasMany('Product');
	}

}
