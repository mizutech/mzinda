<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Councillor extends Model {

	//
	protected $fillable = ['district_id','ward_id','firstname','lastname','image','age'];
	
	
	public function ward(){
		return $this->belongsTo('App\Ward');
	}

	public function councillor_messages(){
		return $this->hasMany('App\CouncillorMessage');
	}
}
