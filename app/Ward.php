<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model {

	//
	protected $fillable = ['district_id','name','size','population','latitude','longitude'];
	
	
	public function district(){
		return $this->belongsTo('App\District');
	}

	public function councillor(){
		return $this->hasOne('App\Councillor');
	}

}
