<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;
use App\Category;
use App\District;
use Cart;
use Carbon\Carbon;


class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		

    	 view()->share('categories', Category::all());
         view()->share('districts',District::all());
    	 view()->share('answer' , false);
         view()->share('today', Carbon::now());
         view()->share('this_url',Request::url());



	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
               // $this->app->bind('path.public', function() {
               //  return $_SERVER['DOCUMENT_ROOT'];
               // });
              $this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
