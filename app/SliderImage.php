<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model {

	//
	protected $fillable = ['name','top_text','main_text','sub_text','button_text','link','size','active','text_active'];
}
