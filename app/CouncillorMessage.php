<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CouncillorMessage extends Model {

	//
	protected $fillable = ['user_id','councillor_id','message'];
	
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function councillor(){
		return $this->belongsTo('App\Councillor');
	}

	public function message_responses(){
		return $this->hasMany('App\MessageResponse','message_id');
	}
}
