<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageResponse extends Model {

	//
	protected $fillable = ['user_id','message_id','message'];
	
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function councillor_message(){
		return $this->belongsTo('App\CouncillorMessage','message_id');
	}
}
