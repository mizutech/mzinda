<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeContent extends Model {

	//
	protected $fillable = ['identifier','heading','sub_heading','image','content','keyword','button_active','button_text','active'];
}
