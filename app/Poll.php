<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PollOption;
use Carbon\Carbon;

class Poll extends Model {

	//
	protected $fillable = ['category_id','admin_id','title','image','description','start_date', 'end_date',
	'active','comments_active','members_only'];

	protected $dates = ['start_date'];
	
	

	public function scopeArchived($query){
		
			$query->where('archived', '=', 1);
		
	}
	
	public function scopeUnArchived($query){
		
			$query->where('archived', '=', 0);
		
	}
	
	public function scopeActive($query){
		
			$query->where('active', '=', 1);
		
	}

	public function scopeInActive($query){
		
			$query->where('active', '=', 0);
		
	}

	public function setStartDateAttribute($date){
		
		$this->attributes['start_date']= Carbon::createFromFormat('Y-m-d',$date);
	}
	
	
	public function category(){
		return $this->belongsTo('App\Category');
	}
	
	public function polloptions(){
		return $this->hasMany('App\PollOption');
	}

	public function pollVotes(){
		return $this->hasMany('App\PollVote');
	}
}
