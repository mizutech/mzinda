<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

	//
	
	protected $connection = 'sms';
	protected $table = 'incident';
	public $timestamps = false;
	protected $guarded= ['id'];

	public function comments(){
		return $this->hasMany('App\Comment','incident_id');
	}

}
