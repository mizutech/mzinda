<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSync extends Model {

	//
	
	protected $connection = 'sms';
	protected $table = 'message';
	public $timestamps = false;
	protected $fillable= ['message_to','message_from','message','message_date','message_type'];

	

}
