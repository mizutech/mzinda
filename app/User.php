<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname', 'lastname','email', 'password','email','telephone','role','verified'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	public function state(){

	}
	
	// public function getFullName(){

	// 	return Auth::user()->firstname.' '.Auth::user()->lastname;
	// }

	public function pollvotes(){
		return $this->hasMany('App\PollVote');
	}

	public function councillor_messages(){
		return $this->hasMany('App\CouncillorMessage');
	}

	public function message_responses(){
		return $this->hasMany('App\MessageResponse');
	}
}
