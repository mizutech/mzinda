<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PollVote extends Model {

	//

	protected $fillable= ['poll_id','user_id','poll_option_id'];

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function poll_option(){
		return $this->belongsTo('App\PollOption');
	}

	public function poll(){
		return $this->belongsTo('App\Poll');
	}
	

}
