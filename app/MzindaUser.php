<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MzindaUser extends Model {

	//
	
	protected $connection = 'sms';
	protected $table = 'users';
	public $timestamps = false;
	protected $fillable= ['name','email','username','password','logins','notify','confirmed','public_profile','approved'];

	

}
