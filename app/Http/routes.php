<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');

Route::get('viewresults/{id}', 'PagesController@getViewPollResults');

Route::get('verification/error', 'Auth\AuthController@getVerificationError');
Route::get('verification/{token}', 'Auth\AuthController@getVerification');


Route::controllers([
	
	'auth' => 'Auth\AuthController',
    'api' => 'ApiController',
	'password' => 'Auth\PasswordController',
	'home' => 'HomeController',
	'sms'  => 'SmsController'
]);

Route::group(['middleware' => 'auth_admin'], function () {

    //Admin Routes
    Route::get('/administrator', 'AdminController@show');
    Route::get('/administrator/create_admin', 'AdminController@createAdmin');
    Route::get('/administrator/edit_admin/{id}', 'AdminController@edit');
    Route::get('/administrator/delete_admin/{id}', 'AdminController@delete');

    Route::post('/administrator/create_admin', 'AdminController@store');
    Route::post('/administrator/edit_admin/{id}', 'AdminController@update');

    //Content Management Routes
    Route::Resource('admin/home-contents', 'HomeContentsController');
    Route::Resource('admin/users', 'UsersController');
    Route::Resource('admin/slider', 'SliderImagesController');
  

    Route::Resource('admin/articles', 'ArticlesController');
    Route::Resource('admin/categories', 'CategoriesController');
    Route::Resource('admin/councillors', 'CouncillorsController');
    Route::post('admin/update-rating', 'CouncillorsController@updateRating');
    Route::Resource('admin/wards', 'WardsController');
    Route::Resource('admin/polls', 'PollsController');
    Route::post('admin/polls/updateActive', 'PollsController@updateActive');
    Route::post('admin/polls/updateArchived', 'PollsController@updateArchived');


});

//login

Route::get('administrator/auth/login', 'AdminController@getLogin');
Route::get('administrator/auth/logout', 'AdminController@getLogout');
Route::post('administrator/auth/login', 'AdminController@postLogin');

Route::group(['middleware' => 'auth.custom'], function () {
	
		Route::controllers([			

			'/'	=> 'PagesController',
		]);
});



