<?php namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		
		//if the user is not an admin,redirect him to the homepage('/') 
		if($this->auth->guest()){
			return redirect('/');
		}
		
		return $next($request);
	}

}
