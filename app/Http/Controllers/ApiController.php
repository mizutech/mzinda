<?php namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\SmsSync;
use App\Reporter;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PollVoteRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\UserVoteRequest;
use App\Category;
use App\Poll;
use Carbon\Carbon;
use Response;
use App\Article;
use App\Report;
use Illuminate\Http\Request;


class ApiController extends Controller {
	
	
	public function getReports(){

		$reports = Report::with('comments')->orderBy('id','DESC')->get();
		return Response::json(['reports'=>$reports],200);

	}

	public function getPolls(){
		$polls=Poll::UnArchived()->active()->with('polloptions')->get();
		foreach ($polls as $poll) {

				//Checking if the user ID and poll ID combination exists in the votes table
				$check_user_count = PollVote::whereRaw('user_id = '.Auth::user()->id.' AND poll_id = '. $request['poll_id'] )->get();
				//Getting the count,whether its 0 or 1
				$user_answer_count = count($check_user_count);
				//If the count is greater than one then that user already voted
				 
				if($user_answer_count >= 1)
				{

					$poll = Poll::findOrFail($id);
					$pollResults =  PollVote::where('poll_id', '=' , ''.$poll_id.'')->get();
					$results =  PollOption::where('poll_id', '=' , $poll_id)->get();
				

					$answer_count = count($pollResults);
					$response["total_results"] = $answer_count;
					$response["results"] = $pollResults;

					$response["error"] = true;
		        	$response["message"] ="Sorry you cant answer a poll more than once";

					return Response::json($response, 200);
				}
		}
		return Response::json(['polls'=>$polls],200);
	}

	public function getNews(){
		$news = Article::orderBy('id','DESC')->get();
		return Response::json(['news'=>$news],200);
	}

	public function postPollVote(Request $request){
		
		//Checking if the user ID and poll ID combination exists in the votes table
		// $check_user_count = PollVote::whereRaw('user_id = '.Auth::user()->id.' AND poll_id = '. $request['poll_id'] )->get();
		// //Getting the count,whether its 0 or 1
		// $user_answer_count = count($check_user_count);
		// //If the count is greater than one then that user already voted
	
		// if($user_answer_count >= 1)
		// {
		// 	\Session::flash('error_message', 'Sorry you cant answer a poll more than once');
		// 	return redirect()->back();
		// }

		$pollVote= new PollVote;
		$pollVote->user_id = Auth::user()->id;
		$pollVote->poll_id = $request['poll_id'];
		$pollVote->poll_option_id = $request['poll_option'];
		/**if using place picker plugin use code below**/
		$pollVote->lat_id =  $request['latitude'];
		$pollVote->long_id = $request['longitude'];
		//$pollVote->location_name = $request['location_name'];
		//if using location picker plugin use commented code
		// $pollVote->lat_id =  $_COOKIE['latitude']; 
		// $pollVote->long_id =  $_COOKIE['longitude'];
		$pollVote->save();

		return 'Success';

	}


	



	/**
	*
	*
	**/

	
	
	
}