<?php namespace App\Http\Controllers;

use App\Councillor;
use App\Product;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Requests\CouncillorRequest;
use Illuminate\Http\Request;
use Image;
use File;
use App\Ward;

class CouncillorsController extends Controller {

	/**
	*
	*Protect our post request
	*
	*
	*
	*/

	public function __construct()
	{
		
	}


	/**
	*
	*Show all councillors
	*
	*@return Response
	*
	*/
	public function index(){

		$councillors= Councillor::all();
		$wards = array();
		
		foreach(Ward::all() as $ward){
			$wards[$ward->id] = $ward->name;
		}

		return view('councillors.index',compact('councillors','wards'));
	}

	/**
	*
	*Shows form for creating a single form
	*
	*@return Response
	*
	*/

	public function create(){

	return view('councillors.index');

	}



	public function store(CouncillorRequest $request)
	{


		//inserting product values
		$councillor= new Councillor();
		$councillor->create($request->all());

		//Using $request->file('filename') gets me the file after it has been validated
		
		$image= $request->file('image');
		if(empty($image)){
				
				


		}else{

				$random_name = str_random();
				$filename =$image->getClientOriginalName();
				$file_string = strtolower($random_name);
				$path = public_path('images/councillors/'.$file_string.'-'.$filename);
				Image::make($image->getRealPath())->fit(300,200)->save($path);
				$councillor->image = 'images/councillors/'.$file_string.'-'.$filename;

				$councillor->save();	
		}

		$image_landscape= $request->file('image_landscape');
		if(empty($image_landscape)){
				
				


		}else{

				$random_name = str_random();
				$filename =$image_landscape->getClientOriginalName();
				$file_string = strtolower($random_name);
				$path = public_path('images/councillors/'.$file_string.'-'.$filename);
				Image::make($image_landscape->getRealPath())->fit(565,350)->save($path);
				$councillor->image_landscape = 'images/councillors/'.$file_string.'-'.$filename;
				$councillor->save();	
		}

		

		\Session::flash('message','Councillor Created');
		return redirect('admin/councillors');
	}


	/**
	*
	*Show single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){

		$councillor = Councillor::findOrFail($id);


		return view('admin/councillors',compact('category')); //$contact->title
	}

	/**
	*
	*Delete single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function destroy(Request $request){
		$id = $request->input('id');
		$councillor= Councillor::findOrFail($id);
		\Session::flash('message','Councillor Deleted');
		if ($councillor){
			$councillor->delete();
		}

		return redirect('admin/councillors');

	}



	public function edit($id)
	{
		$councillors= Councillor::all();
		$edit_councillor = Councillor::findOrFail($id);
		$edit= true;
		$wards = array();
		
		foreach(Ward::all() as $ward){
			$wards[$ward->id] = $ward->name;
		}
		return view('councillors.index',compact('edit_councillor','councillors','edit','wards'));

	}

	public function update($id, CouncillorRequest $request)
	{

		$councillor = Councillor::findOrFail($id);
		$councillor->update($request->all());

		//Using $request->file('filename') gets me the file after it has been validated
		
		$image= $request->file('image');
		if(empty($image)){
				
				


		}else{

				$random_name = str_random();
				$filename =$image->getClientOriginalName();
				$file_string = strtolower($random_name);
				$path = public_path('images/councillors/'.$file_string.'-'.$filename);
				Image::make($image->getRealPath())->fit(300,200)->save($path);
				$councillor->image = 'images/councillors/'.$file_string.'-'.$filename;

				$councillor->save();	
		}

		$image_landscape= $request->file('image_landscape');
		if(empty($image_landscape)){
				
				


		}else{

				$random_name = str_random();
				$filename =$image_landscape->getClientOriginalName();
				$file_string = strtolower($random_name);
				$path = public_path('images/councillors/'.$file_string.'-'.$filename);
				Image::make($image_landscape->getRealPath())->fit(565,350)->save($path);
				$councillor->image_landscape = 'images/councillors/'.$file_string.'-'.$filename;
				$councillor->save();	
		}


		


		\Session::flash('message','Councillor Updated');
		return redirect('admin/councillors');

	}

	public function updateRating(Request $request)
	{

		$councillor = Councillor::findOrFail($request['councillor_id']);
		$councillor->rating = $request['rating'];
		$councillor->save();
		\Session::flash('message','Councillor Updated');
		return redirect('admin/councillors');

	}



}
