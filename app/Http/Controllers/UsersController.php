<?php namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
	
class UsersController extends Controller{
		
		
		public function index()
		{
			$users = User::all();
			return view('users.index',compact('users'));
		}
		

		public function destroy(Request $request)
		{
			$id = $request->input('id');
			$user= User::findOrFail($id);
			if ($user){
				$user->delete();
				\Session::flash('message','Product Deleted');
			}

			return redirect('admin/users');

		}
		// public function newAccount(){
			
		// 	return('users.newaccount');
		// }
		
		// public function create(){
		// 	return('users.newaccount');
			
		// }
		
		// public function store(UserRequest $request)
		// {	
		// 		//inserting user values			
		// 		$user= new User();
		// 		$user->firstname = $request['firstname'];
		// 		$user->lastname = $request['lastname'];
		// 		$user->email = $request['email'];
		// 		$user->password =  Hash::make($request['password']);
		// 		$user->telephone =  $request['telephone'];
			
			
		// 		$user->save();	
		// 		//Using Request::file the file that didnt pass through the request file for validation
		// 		//Using $request->file('filename') gets me the file after it has been validated
		// 		//
				
		// 		$user->save();
		// 		\Session::flash('message','Thank you creating a new acount. Please sign in');
		// 		return redirect('users/signin');	
		// }
	
		// public function getSignIn(){
		// 	return view('auth/login');
			
		// }
		
		// public function postSignin(Request $request){
		// 	if(Auth::attempt(array('email' => $request['email'],'password' => $request['password'] ))){
		// 		\Session::flash('message','Thank you for signing in');
		// 		return('/');
		// 	}
		// 	\Session::flash('message','Your username/Pasword combo was incorrect');
		// 	return redirect('users/signin');
		// }
		
		// public function getSignout(){
		// 	Auth::logout();
		// 	\Session::flash('message','You have been signed out');
		// 	return redirect('users/signin');
			
		//}
	public function update($id, UserRequest $request)
	{
		$user = User::findOrFail($id);
		$user->update($request->all());
		return redirect('admin/articles');

	}
	
	
} 