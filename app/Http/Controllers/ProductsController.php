<?php namespace App\Http\Controllers;

use App\Product;
use App\Http\Controllers\Controller;
use App\Category;
use Carbon\Carbon;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Image;
use File;
use Cart;

class ProductsController extends Controller {

	/**
	*
	*Protect our post request
	*
	*
	*
	*/
	public function __construct(){
			//$this->middleware('auth');
			$this->middleware('auth', ['only' => 'getCart']);
		}

	/**
	*
	*Show all products
	*
	*@return Response
	*
	*/
	public function index(){
		
		
		$categories = array();
		
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}
		$products= Product::all();
		return view('products.index',compact('products','categories'));
	}

	/**
	*
	*Shows form for creating a single form
	*
	*@return Response
	*
	*/

	public function create(){
	$categories = array();
		
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}
		$products= Product::all();
		return view('products.index',compact('products','categories'));
	
	}
	
	public function store(ProductRequest $request)
	{	
		//inserting product values			
		$product= new Product();
		$product->category_id = $request['category_id'];
		$product->title =  $request['title'];
		$product->description =  $request['description'];
		$product->price =  $request['price'];
		
		//Using Request::file the file that didnt pass through the request file for validation
		//Using $request->file('filename') gets me the file after it has been validated
		//
		
		$image= $request->file('image');
		$filename =$image->getClientOriginalName();
		$path = public_path('images/products/'.$filename);
		$relative_path = 'images/products/'.$filename;
		Image::make($image->getRealPath())->resize(253,382)->save($path);
		$product->image = 'images/products/'.$filename;
		
		
		$product->save();
		\Session::flash('message','Product Created');
		return redirect('admin/products');	
	}
	
	
	/**
	*
	*Show single product
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){
	
		$product = Product::findOrFail($id);
		
		
		return view('admin/products',compact('product')); //$contact->title
	}
	
	/**
	*
	*Delete single product
	*
	*param interger $id
	*
	*@return Response
	*
	*/
	
	public function destroy(Request $request){
		$id = $request->input('id');
		$product= Product::findOrFail($id);
		File::delete('public/'.$product->image);
		if ($product){
			$product->delete();
			\Session::flash('message','Product Deleted');
		}
		
		return redirect('admin/products');
		
	}
	
	/**
	*This function defines whether a product is in stock or out of
	**/
	public function update(Request $request){
		$id = $request->input('id');
		$product= Product::findOrFail($id);
		if ($product){
			$product->availability = $request['availability'];
			$product->save();
			
			\Session::flash('message','Product Updated');
			return redirect('admin/products');			
		}
		
		\Session::flash('message','Invalid Product');
		return redirect('admin/products');
	}
	
	
	/**public function edit($id)
	{
				
		$contact = Contact::findOrFail($id);
		
		return view('contacts.edit',compact('contact'));
	
	}**/
	
	// public function update($id, productRequest $request)
	// {
				
	// 	$product = Product::findOrFail($id);
		
	// 	$product->update($request->all());
		
	// 	return redirect('products');
	
	// }
	
	//Cart functions
	
	public function addTocart(Request $request){
		
		$product=Product::find($request->input('id'));
		$quantity= $request->input('quantity');
		Cart::add(array('id' => $product->id, 'name' => $product->title, 'qty' => $quantity, 'price' => $product->price,'image' => $product->image));
		return redirect('store/cart');
	}
	
	public function getCart(){
	
		 return view('store/cart')->with('products', Cart::content());
	}	
	
	public function getRemoveitem($identifier){
	
		$item = Cart::item($identifier);
		$item->remove();
		return redirect('store/cart');
		
	}
	
}
