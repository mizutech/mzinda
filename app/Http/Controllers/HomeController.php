<?php namespace App\Http\Controllers;
use App\Http\Middleware\Authenticate;
use App\Article;
use App\Councillor;
use App\CouncillorMessage;
use App\HomeContent;
use App\MessageResponse;
use App\Poll;
use App\PollVote;
use App\Ward;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PollVoteRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\UserVoteRequest;
use App\Category;
use App\FeaturedPoll;
use Carbon\Carbon;
use View;
use Illuminate\Http\Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex(){
		

		if(Auth::user()){
			$polls=Poll::UnArchived()->with('polloptions')->get();	
			$councillors =  Councillor::all();	
			$wards= Ward::all();
			return view('pages.dashboard',compact('polls','councillors', 'wards'));
		}
		else{

			$home_contents = HomeContent::all();
			return view('pages.index',compact('home_contents'));
		}
	}

	public function getBlog(){
		$this->middleware('auth');
		$blogs = Article::paginate(6);
		$active = 6;
		return view('pages.blog',compact('blogs', 'active'));
	}

	public function getArticle($id){
		$blog = Article::findOrFail($id);
		$active = 6;
		return view('pages.blogdetails',compact('blog', 'active'));
	}

}
