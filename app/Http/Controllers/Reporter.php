<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporter extends Model {

	//
	
	protected $connection = 'sms';
	protected $table = 'reporter';
	public $timestamps = false;
	protected $fillable= ['level_id','service_id','service_account','reporter_date','reporter_first','reporter_last'];

	

}
