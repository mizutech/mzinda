<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Mail;
use Auth;
use App\User;
use App\Poll;
use App\Councillor;
use App\Ward;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;



class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

 
	protected $redirectTo = 'auth/dashboard';
	protected $redirectAfterLogout ='/auth/login';
	protected $loginPath = '/auth/login';
	
	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		// $this->user = $user;
		// $this->middleware('guest', ['except' => 'getLogout']);
	}

	public function getLogin()
	{
		if(Auth::user()){
			
			return redirect()->back();
			
		}

		

		return view('auth.login');	
		
			
	}

	public function getDashboard(){
		if(Auth::user()){
			$user=Auth::user();
			$polls = Poll::UnArchived()->active()->with('polloptions')->get();
			$councillors =  Councillor::all();	
			$wards= Ward::all();
	
			return view('pages.dashboard',compact('user','polls','councillors','wards'));
		}else{
			return redirect('auth/login');
		}
	}

	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$this->auth->login($this->registrar->create($request->all()));
		
		$firstname = $request->get('firstname');
		$submitted_email = $request->get('email');

		//Send user verification email
		UserVerification::generate(Auth::user());
        UserVerification::send(Auth::user(), 'Verify your Mzinda Account');

		return redirect('auth/first-run');

		//return redirect($this->redirectPath());
	}

	/**
	*Newly registered members get redirected by this function
	*The pages with first-run links/routes will have tutorials
	*
	*/
	public function getFirstRun(){

		if(Auth::user()){
			$user=Auth::user();
			$polls = Poll::paginate(10);
			$councillors =  Councillor::all();	
			$wards= Ward::all();
			$new_user = true;
	
			return view('pages.dashboard',compact('user','polls','councillors','wards','new_user'));
		}else{
			return redirect('auth/login');
		}

	}

	public function setFirstRun()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : 'auth/first-run';
	}


	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/auth/dashboard';
	}


	public function postLoginHome(UserRequest $request)
	{


		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			return redirect()->intended($this->redirectPath());
		}

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
	}

	public function getVerification(Request $request, $token)
	{
		$r_email = $request['email'];
		$unverified_user = User::where('email','=',$r_email)->get()->first();

		if($unverified_user->verified == 0 && $unverified_user->verification_token == $token)
		{
			$unverified_user->verified = 1;
			$unverified_user->save();
			\Session::flash('message','Verification Successful,Please login to continue');
			return redirect('/auth/login');
		}
		elseif($unverified_user->verification_token != $token)
		{
			return 'There was an error. Your verification code did not match ours. Please request a new verification code at this link<a href="#"></a>';

		}elseif($unverified_user->verified != 0)
		{
			\Session::flash('message','You are already verifie,Please login to continue');
			return redirect('/auth/login');
		}
		
	}




}
