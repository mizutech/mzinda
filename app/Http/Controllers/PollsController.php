<?php namespace App\Http\Controllers;

use App\Poll;
use App\PollOption;
use App\Category;
use App\Collection;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\PollRequest;
use Illuminate\Http\Request;
use Image;
use File;
use Cart;

class PollsController extends Controller {

	/**
	*
	*Protect our post request
	*
	*
	*
	*/
	public function __construct(){
			//$this->middleware('auth');
			$this->middleware('auth', ['only' => 'all']);
		}

	/**
	*
	*Show all polls
	*
	*@return Response
	*
	*/
	public function index(){
		
		
		$categories = array();
		
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}

		$polls= Poll::paginate(9);
		return view('polls.index',compact('polls','categories'));
	}

	/**
	*
	*Shows form for creating a single form
	*
	*@return Response
	*
	*/

	public function create(){
	$categories = array();
		
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}
		$polls= Poll::paginate(9);
		return view('polls.index',compact('polls','categories'));
	
	}
	
	public function store(PollRequest $request)
	{	

		//inserting poll values			
		$poll= new Poll();
		$poll->category_id = $request['category_id'];
		$poll->admin_id = 1;
		$poll->title =  $request['title'];
		$poll->description =  $request['description'];
		$poll->start_date = $request['start_date'];
		$poll->end_date = $request['end_date'];
		$poll->comments_active = 1;
		// $poll->members_only =  $request['members_only'];		
		$poll->active = $request['active'];
		
		//Using $request->file('filename') gets me the file after it has been validated

		
		$image= $request->file('image');
		if(!empty($image))
		{
			$filename =$image->getClientOriginalName();
			$path = public_path('images/polls/'.$filename);
			$relative_path = 'images/polls/'.$filename;
			Image::make($image->getRealPath())->resize(253,382)->save($path);
			$poll->image = 'images/polls/'.$filename;
		}
		
		$poll->save();

		$new_poll_id = $poll->id;
		$persist_array = Array(); //Array to be used to save the options
		$submitted_options = $request['poll_options']; //Array of options from the form submitted by the user
		//dd($submitted_options);
		//Set and intialize the order value to zero
		$order_count = 0;
		foreach($submitted_options as $option){
			//each time we loop over add 1 to order count
			$order_count++;

			$poll_option= new PollOption();//Instatiate the poll options
			$poll_option->poll_id = $new_poll_id;
			$poll_option->title = $option;
			$poll_option->order = $order_count;

			//Using for each loop iteration is very expensive but it will have to do for now, am out of time
			$poll_option->save();
			//Adding the row to the persist array so we can later save it to the db
			//array_push($persist_array ,$poll_option->poll_id,$poll_option->title,$poll_option->order);
		}
		
		//$poll_option->save($persist_array);



		\Session::flash('message','Poll Created');
		return redirect('admin/polls');	


	}
	
	
	/**
	*
	*Show single poll
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){
	
		$poll = Poll::findOrFail($id);
		
		
		return view('admin/polls',compact('poll')); //$contact->title
	}
	
	/**
	*
	*Delete single poll
	*
	*param interger $id
	*
	*@return Response
	*
	*/
	
	public function destroy(Request $request){
		$id = $request->input('id');
		$poll= Poll::findOrFail($id);
		File::delete('public/'.$poll->image);
		if ($poll){
			$poll->delete();
			\Session::flash('message','poll Deleted');
		}
		
		return redirect('admin/polls');
		
	}
	
	
	
	
	public function edit($id)
	{
		$polls =	Poll::paginate(9);	
		foreach(Category::all() as $category){
			$categories[$category->id] = $category->name;
		}
		$edit_poll = Poll::findOrFail($id);
		$poll_options = PollOption::where('poll_id','=',$id)->limit(4)->get();

		$edit= true;
		return view('polls.index',compact('polls','edit_poll','edit','categories','poll_options'));
	
	}
	/**
	*This function defines whether a poll is in stock or out of
	**/
	public function updateActive(Request $request){
			$id = $request['id'];
			$poll= Poll::findOrFail($id);
			$poll->active = $request['active'];
			$active = [$poll->active];
			$poll->update($active);
			\Session::flash('message','Poll Activity Updated');
			return redirect('admin/polls');			
	}

	public function updateArchived(Request $request){
			$id = $request['id'];
			$poll= Poll::findOrFail($id);
			$poll->archived = $request['archived'];
			$poll->save();
			\Session::flash('message','Poll Archive State Updated');
			return redirect('admin/polls');			
	}
	

	public function update($id, PollRequest $request)
	{
				
		$poll = Poll::findOrFail($id);

		$poll->category_id = $request['category_id'];
		$poll->admin_id = 1;
		$poll->title =  $request['title'];
		$poll->description =  $request['description'];
		$poll->start_date = $request['start_date'];
		$poll->end_date = $request['end_date'];
		$poll->comments_active = 1;
		// $poll->members_only =  $request['members_only'];		
		$poll->active = $request['active'];
		
		//Using $request->file('filename') gets me the file after it has been validated

		
		$image= $request->file('image');
		if(!empty($image))
		{
			$filename =$image->getClientOriginalName();
			$path = public_path('images/polls/'.$filename);
			$relative_path = 'images/polls/'.$filename;
			Image::make($image->getRealPath())->resize(253,382)->save($path);
			$poll->image = 'images/polls/'.$filename;
		}


		$prod_update=[$poll->category_id, $poll->admin_id, $poll->title, $poll->desc_excerpt, $poll->description,
		$poll->price, $poll->image];
		$poll->update($prod_update);

		//start processing poll options
		$new_poll_id = $poll->id;
		$persist_array = Array(); //Array to be used to save the options
		$submitted_options = $request['poll_options']; //Array of options from the form submitted by the user
		//return $submitted_options;
		//dd($submitted_options);
		//Set and intialize the order value to zero
		$order_count = 0;
		foreach($submitted_options as $option){
			//each time we loop over add 1 to order count
			$order_count++;

			$poll_option= PollOption::findOrFail($option['id']);//Instatiate the poll option
			$row = array(
				'poll_id' => $new_poll_id,
				'title' => $option['title'],
				'order' => $order_count
			);
			

			//Using for each loop iteration is very expensive but it will have to do for now, am out of time
			$poll_option->update($row);
			//Adding the row to the persist array so we can later save it to the db
			//array_push($persist_array ,$poll_option->poll_id,$poll_option->title,$poll_option->order);
		}

		\Session::flash('message','Poll Updated');		
		return redirect('admin/polls');
	
	}
	
	
}
