<?php namespace App\Http\Controllers;

use App\SliderImage;
use App\Http\Requests\SliderImagesRequest;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Http\Request;
use File;

class SliderImagesController extends Controller {

	public function __construct()
	{
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function index(){

 		$slider_images= SliderImage::all();
 		return view('slider.index',compact('slider_images'));
 	}

 	/**
 	*
 	*Show single Image
 	*
 	*param interger $id
 	*
 	*@return Response
 	*
 	*/

 	public function show(SliderImage $slider_images){
 return view('slider.show',compact('slider_images')); //$Image->title
 	}

 	public function create(){

 	return view('slider.create');
 	}


 	public function store(SliderImagesRequest $request)
 	{
		$slider_image = $request->file('slider_image');

				if (!empty($slider_image))
				{
		
					$filename =$slider_image->getClientOriginalName();
					$path = public_path('images/slider/'.$filename);

					$new_img=	Image::make($slider_image->getRealPath())->fit(1920,924)->save($path);
					$image_row = SliderImage::findOrFail($id);
					$image_row->name = 'images/slider/'.$filename;
					$image_row->top_text = $request['top_text'];
					$image_row->sub_text = $request['sub_text'];
					$image_row->main_text = $request['main_text'];
					$image_row->button_text = $request['button_text'];
					$image_row->link = $request['link'];
					$image_row->active = $request['active'];
					$image_row->text_active = $request['text_active'];
					

					$image_row->save();
				}
				else
				{
					$image_row = SliderImage::findOrFail($id);
					$image_row->top_text = $request['top_text'];
					$image_row->sub_text = $request['sub_text'];
					$image_row->main_text = $request['main_text'];
					$image_row->button_text = $request['button_text'];
					$image_row->link = $request['link'];
					$image_row->active = $request['active'];
					$image_row->text_active = $request['text_active'];

					$image_row->save();
				}

		\Session::flash('message','Slider Created');
 		return redirect('admin/slider');

 	}

 	public function edit($id)
 	{

 		$slider_image = SliderImage::findOrFail($id);

 		return view('slider.edit',compact('slider_image'));

 	}

 	public function update($id, SliderImagesRequest $request)
 	{


				$slider_image = $request->file('slider_image');

				if (!empty($slider_image))
				{
		
					$filename =$slider_image->getClientOriginalName();
					$path = public_path('images/slider/'.$filename);

					$new_img=	Image::make($slider_image->getRealPath())->fit(1920,924)->save($path);

					$image_row = SliderImage::findOrFail($id);
					$image_row->name = 'images/slider/'.$filename;
					$image_row->top_text = $request['top_text'];
					$image_row->sub_text = $request['sub_text'];
					$image_row->main_text = $request['main_text'];
					$image_row->button_text = $request['button_text'];
					$image_row->link = $request['link'];
					$image_row->active = $request['active'];
					$image_row->text_active = $request['text_active'];
					

					$image_row->update($request->all());
				}
				else
				{
					$image_row = SliderImage::findOrFail($id);
					$image_row->top_text = $request['top_text'];
					$image_row->sub_text = $request['sub_text'];
					$image_row->main_text = $request['main_text'];
					$image_row->button_text = $request['button_text'];
					$image_row->link = $request['link'];
					$image_row->active = $request['active'];
					$image_row->text_active = $request['text_active'];

					$image_row->update();
				}


	

		\Session::flash('message','Slider Updated');
 		return redirect('admin/slider');

 	}

	public function destroy(Request $request){
		$id = $request->input('id');
		$product= SliderImage::findOrFail($id);
		File::delete('public/'.$product->image);
		if ($product){
			$product->delete();
			\Session::flash('message','Slider Deleted');
		}

		return redirect('admin/products');

	}

}
