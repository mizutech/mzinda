<?php namespace App\Http\Controllers;

use App\HomeContent;
use App\Http\Requests\HomeContentsRequest;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Http\Request;
use File;

class HomeContentsController extends Controller {

	public function __construct()
	{
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function index(){

 		$home_contents= HomeContent::all();
 		return view('home_contents.index',compact('home_contents'));
 	}

 	/**
 	*
 	*Show single Image
 	*
 	*param interger $id
 	*
 	*@return Response
 	*
 	*/

 	public function show(HomeContent $home_contents){
 		return view('home_contents.show',compact('home_contents')); //$Image->title
 	}

 	public function create(){

 	return view('home_contents.create');
 	}


 	public function store(HomeContentsRequest $request)
 	{
		$image = $request->file('image');

				if (!empty($image))
				{
					$image_row->create($request->all());
					$filename =$image->getClientOriginalName();
					$path = public_path('images/home_contents/'.$filename);
					$new_img=	Image::make($image->getRealPath())->fit(400,400)->save($path);

					$image_row = HomeContent::findOrFail($image_row->id);

					$image_row->image = 'images/home_content/'.$filename;
					$image_row->save();
				}
				else
				{
					$image_row = HomeContent::findOrFail($id);


					$image_row->create($request->all());
				}

		\Session::flash('message','Content Created');
 		return redirect('admin/home-contents');

 	}

 	public function edit($id)
 	{

 		$slider_image = HomeContent::findOrFail($id);

 		return view('home_contents.edit',compact('slider_image'));

 	}

 	public function update($id, HomeContentsRequest $request)
 	{
 				$image_row = HomeContent::findOrFail($id);
				$image_row->update($request->all());

				$image = $request->file('image');

				if (!empty($image))
				{
					

					$filename =$image->getClientOriginalName();
					$path = public_path('images/home_contents/'.$filename);
					$new_img=	Image::make($image->getRealPath())->fit(400,400)->save($path);
					
					$image_row->image = 'images/home_contents/'.$filename;
					$image_row->save();
				}


		\Session::flash('message','Content Updated');
 		return redirect('admin/home-contents');

 	}

	public function destroy(Request $request){
		$id = $request->input('id');
		$product= HomeContent::findOrFail($id);
		File::delete('public/'.$product->image);
		if ($product){
			$product->delete();
			\Session::flash('message','Content Deleted');
		}

		return redirect('admin/home-contents');

	}

}
