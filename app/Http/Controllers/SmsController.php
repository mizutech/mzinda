<?php namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\SmsSync;
use App\Reporter;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PollVoteRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\UserVoteRequest;
use App\Category;
use App\FeaturedPoll;
use Carbon\Carbon;
use Response;
use App\User;
use Hash;
use App\PollVote;
use App\Poll;
use App\PollOption;
use App\Comment;


use Illuminate\Http\Request;


class SmsController extends Controller {
	
	

	public function getReceive(Request $request){

		//Save the reporter details
		$reporter = new Reporter();
		$reporter->level_id = 3;
		$reporter->service_id = 1;
		$reporter->service_account = $request->number;
		$reporter->reporter_first = "SMS";
		$reporter->reporter_last = "Server";
		$reporter->reporter_phone = $request->number;
		$reporter->reporter_date = Carbon::now();
		$reporter->save();

		//Save the sms details
		$sms = new SmsSync();		
		$sms->reporter_id = $reporter->id;
		$sms->message_from = $request->number;
		$sms->message = $request->message;
		$sms->message_date = Carbon::now();
		$sms->message_type = 1;
		$sms->save();
		return $request->number .' '.$request->message.' '.Carbon::now();
	}

	/**
	*
	*
	**/

	public function postReceive(Request $request){

		//Save the reporter details
		$reporter = new Reporter();
		$reporter->level_id = 3;
		$reporter->service_id = 1;
		$reporter->service_account = $request->from;
		$reporter->reporter_first = "SMS";
		$reporter->reporter_last = "Server";
		$reporter->reporter_phone = $request->from;
		$reporter->reporter_date = Carbon::now();
		$reporter->save();

		//Save the sms details
		$sms = new SmsSync();		
		$sms->reporter_id = $reporter->id;
		$sms->message_from = $request->from;
		$sms->message = $request->message;
		$sms->message_date = Carbon::now();
		$sms->message_type = 1;
		$sms->save();
		$array = array('foo', 'bar');
    
    //this route should returns json response
    return Response::json(['payload'=>['success' => true, 'error' => null]], 200);
	}	

public function postReports(Request $request){
		

		//Save the reporter details
		$reporter = new Reporter();
		$reporter->level_id = 3;
		$reporter->service_id = 1;
		$reporter->service_account = "0884578949";
		$reporter->reporter_first = "Mobile";
		$reporter->reporter_last = "App";
		$reporter->reporter_phone = "0884578949";
		$reporter->reporter_date = Carbon::now();
		$reporter->save();

		//Save the sms details
		$sms = new SmsSync();		
		$sms->reporter_id = $reporter->id;
		$sms->message_from = $request->sender;
		$sms->message = $request->report;
		$sms->message_date = Carbon::now();
		$sms->message_type = 1;
		$sms->save();
		//this route should returns json response
                return Response::json(['payload'=>['success' => true, 'error' => null]], 200);

	}


	//get User
	public function postUserLogin(Request $request){
		$response = array();
 		$email = $request->email;
 		$password = $request->password;

 		

		//$user_exists = User::whereRaw("email = '".$request->email."' AND password = '".$password."'")->get();
		$user_exists = User::where("email","=",$request->email)->get()->last();
		//If a user with the same email already exists in the db, just call his data
		if(count($user_exists) == 0){

			
			// User with same email already existed in the db
            $response["error"] = true;
            $response["message"] = "No user found";
            $response["password"] = $password." => ".Hash::make($request->password);

				
		}else{


			if (Hash::check($password, $user_exists->password)) {

    			$response["error"] = false;
            	$response["user"] = $user_exists;

			}else{

				$response["error"] = true;
            	$response["message"] = "Email and Password combination wrong";

			}
           

		}
				
				

		return Response::json($response, 200);

	}

public function postPollVote(Request $request){

				$user_id = $request->user_id;
		
		$selected_answer = $request->poll_option_id;
		$latitude =  $request['latitude'];
		$longitude = $request['longitude'];
$result =  PollOption::findOrFail($selected_answer);
		$poll_id = $result->poll_id;
			

//Checking if the user ID and poll ID combination exists in the votes table
		$check_user_count = PollVote::whereRaw('user_id = '.$user_id.' AND poll_id = '. $poll_id )->get();
$user_answer_count = count($check_user_count);
		//If the count is greater than one then that user already voted
		 
		if($user_answer_count >= 1)
		{

			$poll = Poll::findOrFail($poll_id);
			$pollResults =  PollVote::where('poll_id', '=' , ''.$poll_id.'')->get();
			$results =  PollOption::where('poll_id', '=' , $poll_id)->get();
		

			$answer_count = count($pollResults);
			$response["total_results"] = $answer_count;
			$response["results"] = $pollResults;

			$response["error"] = true;
        	$response["message"] ="Sorry you cant answer a poll more than once";

			return Response::json($response, 200);
		}

		
		$pollVote= new PollVote;
		$pollVote->user_id = $user_id;
		$pollVote->poll_id = $poll_id;
		$pollVote->poll_option_id = $selected_answer;
		$pollVote->lat_id =  $latitude;
		$pollVote->long_id = $longitude;

		if($pollVote->save()){

			$response["error"] = false;
        	$response["user"] = $pollVote;

			return Response::json($response, 200);

		}
		

		//$answer = true;
		//$polls=Poll::with('polloptions')->get();

		

	}
public function postComments(Request $request){
			
			$report_id = $request->report_id;
			$comment_email = $request->sender_email;
			$comment = $request->comment;
			$author = $request->sender_email;
			$comment_ip = $request->sender_ip;

			$comments = array(
				'incident_id' => $report_id,
				'comment_email' => $comment_email,
				'comment_description' => $comment,
				'comment_author' => $author,				
				'comment_ip' => $comment_ip
			);
			
			$new_comment = Comment::create($comments);

			return $new_comment;
		}

	
	
}