<?php namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use File;
use Image;


class ArticlesController extends Controller {

	/**
	*
	*Show all articles
	*
	*@return Response
	*
	*/
	public function index(){
		
		$articles= Article::latest('published_at')->published()->get();
		return view('articles.index',compact('articles'));
	}

	/**
	*
	*Show single article
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){
	
		$article = Article::findOrFail($id);
		
		
		return view('articles.show',compact('article')); //$article->title
	}
	
	public function create(){
	
	return view('articles.create');
	}
	
	
	public function store(ArticleRequest $request)
	{
				
		$article = Article::create($request->all());
		$main_image = $request->file('main_image');  
		//Making sure the images array is not empty
		//$image_checksum = array_filter($request->file('images'));


		if(!empty($main_image)){
				
				

					$real_name =$main_image->getClientOriginalName();

					
					$random_name = str_random();
					$img_string = strtolower($random_name);
					$filename = strtolower($real_name);

					$path = public_path('images/articles/'.$img_string.$filename);

					$new_img = Image::make($main_image->getRealPath())->fit(480,320)->save($path);

					$img_path = 'images/articles/'.$img_string.$filename;
					
					$article->image = 'images/articles/'.$img_string.$filename;

					$article->save();

		}
		return redirect('admin/articles');
	
	}
	
	public function edit($id)
	{
		$articles= Article::latest('published_at')->published()->get();		
		$edit_article = Article::findOrFail($id);
		$edit=true;
		return view('articles.index',compact('articles','edit_article','edit'));
	
	}
	
	public function update($id, ArticleRequest $request)
	{
				
		$article = Article::findOrFail($id);
		
		$article->update($request->except('main_image'));

		$main_image = $request->file('main_image');  
		if(!empty($main_image)){
				
				

					$real_name =$main_image->getClientOriginalName();

					
					$random_name = str_random();
					$img_string = strtolower($random_name);
					$filename = strtolower($real_name);

					$path = public_path('images/articles/'.$img_string.$filename);

					$new_img = Image::make($main_image->getRealPath())->fit(480,320)->save($path);

					$img_path = 'images/articles/'.$img_string.$filename;
					
					$article->image = 'images/articles/'.$img_string.$filename;

					$article->save();

		}
		
		return redirect('admin/articles');
	
	}

	public function destroy(Request $request){
			$id = $request->input('id');
			$article= Article::findOrFail($id);
			File::delete('public/'.$article->image);
			if ($article){
				$article->delete();
				\Session::flash('message','Article Deleted');
			}

			return redirect('admin/articles');

	}
}
