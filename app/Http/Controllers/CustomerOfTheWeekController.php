<?php namespace App\Http\Controllers;

use App\CustomerOfTheWeek;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Requests\CustomerOfTheWeekRequest;
use Illuminate\Http\Request;
use Image;
use File;


class CustomerOfTheWeekController extends Controller {
	public function __construct()
	{
		
	}

	/**
	*
	*Show all articles
	*
	*@return Response
	*
	*/
	public function index(){

		$customers= CustomerOfTheWeek::latest('published_at')->published()->get();
		return view('customers.index',compact('customers'));
	}

	/**
	*
	*Show single article
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){

		$customer = CustomerOfTheWeek::findOrFail($id);


		return view('customers.show',compact('article')); //$customer->title
	}

	public function create(){

	return view('customers.create');
	}


	public function store(CustomerOfTheWeekRequest $request)
	{

			//inserting customer values
		$customer = new CustomerOfTheWeek();
		$customer->name =  $request['name'];
		$customer->bio = $request['bio'];
		$customer->published_at = $request['published_at'];
		$customer_image = $request->file('image');

		if(!empty($customer_image))
		{
			$string = str_random(15);
			$filename = $customer_image->getClientOriginalName();
			$path = 'images/customer/'.$string.$filename;
			$new_img = Image::make($customer_image->getRealPath())->save($path);
			$customer->image = 'images/customer/'.$string.$filename;
		}
		else{

			$customer->image = 'images/customer/faq.jpg';
			
		}
		

		$customer->save();

		\Session::flash('message','Customer of the week created');
		return redirect('admin/customers');

	}

	public function edit($id)
	{
		$customers = CustomerOfTheWeek::latest('published_at')->published()->get();
		$edit_customer = CustomerOfTheWeek::findOrFail($id);
		$edit=true;
		return view('customers.index',compact('customers','edit_customer','edit','tags'));

	}

	public function update($id, CustomerOfTheWeekRequest $request)
	{
		$customer = CustomerOfTheWeek::findOrFail($id);
		$customer->name =  $request['name'];
		$customer->bio = $request['bio'];
		$customer->published_at = $request['published_at'];
		$customer_image = $request->file('image');

		if(!empty($customer_image))
		{
			$string = str_random(15);
			$filename = $customer_image->getClientOriginalName();
			$path = 'images/customer/'.$string.$filename;
			$new_img = Image::make($customer_image->getRealPath())->save($path);
			$customer->image = 'images/customer/'.$string.$filename;
		}
		

		$customer->update();
		

		return redirect('admin/customers');

	}


	public function updateActive(Request $request){
			$id = $request['id'];
			$customer= CustomerOfTheWeek::findOrFail($id);
			$customer->active = $request['active'];
			$active = [$customer->active];
			$customer->update($active);
			\Session::flash('message','Customer Updated');
			return redirect('admin/customers');
	}


	
	public function destroy(Request $request){
			$id = $request->input('id');
			$customer= CustomerOfTheWeek::findOrFail($id);
			File::delete('public/'.$customer->image);
			if ($customer){
				$customer->delete();
				\Session::flash('message','Article Deleted');
			}

			return redirect('admin/customers');

	}

}
