<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Input;
use Hash;
use Validator;
use Auth;
use Session;
use App\MzindaUser;


class AdminController extends BaseController
{

    public function index()
    {
        if(Auth::user()->role=='Administrator') {
            $admins = DB::table('users')->whereIn('role', ['role', 'Administrator', 'Accountant', 'Manager'])->get();
            return view('administrators.index', compact('admins'));
        }
        else{
            return redirect('/admin/categories');
        }

    }

    public function createAdmin(){
        if(Auth::user()->role=='Administrator') {
            return view('administrators.create_admin');

        }
        else{
            return redirect('/admin/categories');
        }

    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'role' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect('/administrator/create_admin')
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->role = Input::get('role');
        $user->save();

        $ushahidi_user = new MzindaUser();
        $ushahidi_user->name = 'Administrator';        
        $ushahidi_user->email = Input::get('email');
        $ushahidi_user->username = Input::get('firstname');
        $ushahidi_user->password = '13632cd012d5848af5b66e9e96f7dcc889023c27c2bb8900fd';
        $ushahidi_user->logins = 0;
        $ushahidi_user->notify = 0;
        $ushahidi_user->confirmed = 1;
        $ushahidi_user->public_profile = 0;
        $ushahidi_user->approved = 1;
        $ushahidi_user->needinfo = 1;
        $ushahidi_user->save();

        \Session::flash('success-msg', 'Successfully Added');

        return redirect('/administrator/create_admin');


    }

    public function show()
    {
        if(Auth::user()->role=='Administrator') {
            $admins = DB::table('users')->whereIn('role', ['role', 'Administrator', 'Accountant', 'Manager'])->get();
            return view('administrators.index', ['admins' => $admins]);
        }
        else{
            return redirect('/admin/categories');


        }

    }

    public function edit($id)
    {
        if(Auth::user()->role=='Administrator') {
        $admins = DB::table('users')->where('id', '=', $id)->first();
        return view('administrators.edit_admin', ['admin' => $admins]);
        }
        else{
            return redirect('//admin/categories');


        }
    }

    public function update($id)
    {

        $validator = Validator::make(Input::all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'role' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect('administrator/edit_admin/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $admin_firstname = Input::get('firstname');
        $admin_lastname = Input::get('lastname');
        $admin_email = Input::get('email');

        $admin_password = Input::get('password');
        $hashed = Hash::make($admin_password);

        $admin_count = DB::table('users')->where('email', '=', $admin_email)->where('id', '!=', $id)->get();

        if (sizeof($admin_count) > 0) {

            $validator = Validator::make(Input::all(), [
                'email' => 'required|email|unique:users'
            ]);

            if ($validator->fails()) {
                return redirect('/administrator/edit_admin/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        } else {


            $admin = User::find($id);
            $old_email = $admin->email;
           
            $ushahidi_user = MzindaUser::where('email','=', $old_email)->get()->first();
            if($ushahidi_user){

                $update_user = MzindaUser::find($ushahidi_user->id);    
                $update_user->email = $admin_email;
                $update_user->username = $admin_firstname;
                $update_user->save();

            }
            

            
            $admin->firstname = $admin_firstname;
            $admin->lastname = $admin_lastname;
            $admin->email = $admin_email;
            $admin->password = $hashed;
            $admin->role = Input::get('role');
            $admin->save();

            

            \Session::flash('success-msg', 'Successfully Edited');
            return redirect()->back();
        }

    }

    public function delete($id)
    {
        if(Auth::user()->role=='Administrator') {
        DB::table('users')->where('id', '=', $id)->delete();
        return redirect()->back();
        }
        else{
            return redirect('admin/categories');


        }
    }

     /**
     * [create description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogout(){
        \Auth::logout();
        Session::flush();
        return redirect('/administrator/auth/login');
    }

    public function getLogin(){

        if(Auth::check() && Auth::user()->role=='Administrator'){
            $admins = DB::table('users')->whereIn('role', ['role', 'Administrator', 'Accountant', 'Manager'])->get();
            return view('administrators.index',['admins' => $admins]);
        }

        return view('administrators.login');
    }

    public function postLogin(){
        // validator - email , password
        // auth::attempt(['email'=>$email,'password'=>$password]) response 1 / 0

        $email = Input::get('email');
        $password = Input::get('password');
        $role = Input::get('role');


        $v = Validator::make(['email'=>$email,'password'=>$password,'role'=>$role],['email'=>'required|email','password'=>'required','role'=>'required']);

        if($v->fails()){
            return redirect()->back()->withErrors($v)->withInput(Input::all());
        }else{
            if(Auth::attempt(['email'=>$email,'password'=>$password,'role'=>$role])){
                return redirect('/administrator');
            }else{
                Session::flash('error_msg','Invalid credentials');
                return redirect()->back();
            }
        }

    }
}

