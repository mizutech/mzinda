<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Image;
use File;

class CategoriesController extends Controller {

	/**
	*
	*Protect our post request
	*
	*
	*
	*/
	
	public function __construct()
	{
		$this->middleware('auth');
	}


	/**
	*
	*Show all categories
	*
	*@return Response
	*
	*/
	public function index(){
		
		$categories= Category::all();
		return view('categories.index',compact('categories'));
	}

	/**
	*
	*Shows form for creating a single form
	*
	*@return Response
	*
	*/

	public function create(){
	
	return view('categories.index');
	
	}
	


	public function store(CategoryRequest $request)
	{				
		
		
		//inserting product values			
		$category= new Category();
		$category->name = $request['name'];
		//Using Request::file the file that didnt pass through the request file for validation
		//Using $request->file('filename') gets me the file after it has been validated
		if(!empty($thumbnail)){
			$thumbnail= $request->file('thumbnail');
			$filename =$thumbnail->getClientOriginalName();
			$path = public_path('images/categories/'.$filename);
			$relative_path = 'images/categories/'.$filename;
			Image::make($thumbnail->getRealPath())->resize(250,200)->save($path);
			$category->thumbnail = 'images/categories/'.$filename;
		}
		

		$category->save();
		\Session::flash('message','Category Created');
		return redirect('admin/categories');	
	}
		
	
	/**
	*
	*Show single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){
	
		$category = Category::findOrFail($id);
		
		
		return view('admin/categories',compact('category')); //$contact->title
	}
	
	/**
	*
	*Delete single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/
	
	public function destroy(Request $request){
		$id = $request->input('id');
		$category= Category::findOrFail($id);
		\Session::flash('message','Category Deleted');
		if ($category){
			$category->delete();
		}
		
		return redirect('admin/categories');
		
	}
	
	

	public function edit($id)
	{
		$categories= Category::all();		
		$category = Category::findOrFail($id);
		$edit= true;
		return view('categories.index',compact('categories','category','edit'));
	
	}
	
	public function update($id, CategoryRequest $request)
	{
				
		$category = Category::findOrFail($id);
		$category->name = $request['name'];

		$thumbnail= $request->file('thumbnail');
		$filename =$thumbnail->getClientOriginalName();
		$path = public_path('images/categories/'.$filename);
		$relative_path = 'images/categories/'.$filename;
		Image::make($thumbnail->getRealPath())->resize(250,200)->save($path);
		$category->thumbnail = 'images/categories/'.$filename;

		$cat_update=[$category->name, $category->thumbnail];
		$category->update($cat_update);

		
		return redirect('admin/categories');
	
	}
}
