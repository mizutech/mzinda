<?php namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\Article;
use App\Councillor;
use App\CouncillorMessage;
use App\HomeContent;
use App\MessageResponse;
use App\Poll;
use App\PollVote;
use App\PollOption;
use App\Ward;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PollVoteRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\UserVoteRequest;
use App\Category;
use App\FeaturedPoll;
use Carbon\Carbon;
use View;
use Illuminate\Http\Request;
use DB;
use JavaScript;

class PagesController extends Controller {
	
	

	public function __construct(){
		
	}

	public function getIndex(){
		
		if(Auth::user()){
			$polls=Poll::UnArchived()->active()->with('polloptions')->get();	
			$councillors =  Councillor::all();	
			$wards= Ward::all();
			
			return view('pages.dashboard',compact('polls','councillors', 'wards'));
		}
		else{

			$home_contents = HomeContent::all();
			return view('pages.index',compact('home_contents'));
		}
	}
	/**Get Polls by their category**/
	public function getCategory($id){
		
		$polls=Poll::where('category_id','=', $id)->get();
		
		return view('pages.polls',compact('polls','id'));
	}


	public function getPolls(){
		
		$polls=Poll::UnArchived()->active()->with('polloptions')->get();
		$answer = false;
		return view('pages.polls',compact('polls','true'));
	}

	public function postPollVote(UserVoteRequest $request){
		
		//Checking if the user ID and poll ID combination exists in the votes table
		$check_user_count = PollVote::whereRaw('user_id = '.Auth::user()->id.' AND poll_id = '. $request['poll_id'] )->get();
		//Getting the count,whether its 0 or 1
		$user_answer_count = count($check_user_count);
		//If the count is greater than one then that user already voted
	
		if($user_answer_count >= 1)
		{
			\Session::flash('error_message', 'Sorry you cant answer a poll more than once');
			return redirect()->back();
		}

		$pollVote= new PollVote;
		$pollVote->user_id = Auth::user()->id;
		$pollVote->poll_id = $request['poll_id'];
		$pollVote->poll_option_id = $request['poll_option'];
		/**if using place picker plugin use code below**/
		$pollVote->lat_id =  $request['latitude'];
		$pollVote->long_id = $request['longitude'];
		//$pollVote->location_name = $request['location_name'];
		//if using location picker plugin use commented code
		// $pollVote->lat_id =  $_COOKIE['latitude']; 
		// $pollVote->long_id =  $_COOKIE['longitude'];
		$pollVote->save();

		$answer = true;
		$polls=Poll::with('polloptions')->get();

		\Session::flash('message','You have successfully voted for this poll');
		
		
		return redirect('/poll-answered');

	}

	public function getPollAnswered(){
		
		$polls=Poll::with('polloptions')->get();
		$answer = true;
		return view('pages.polls',compact('polls','answer'));
	}

	public function getViewPollResults($id){


		$poll = Poll::findOrFail($id);
		$pollResults =  PollVote::where('poll_id', '=' , ''.$id.'')->get();
		$results =  PollOption::where('poll_id', '=' , $id)->get();
		

		$answer_count = count($pollResults);
		return view('pages.results',compact('poll','pollResults','answer_count', 'results'));
	}

	public function getPollArchive(){
		$old_polls = Poll::Archived()->active()->get();
		
		return view('pages.pollarchive',compact('old_polls'));
	}


	public function getCitizenReports(){
		
		$polls=Poll::with('polloptions')->get();
		return view('pages.citizen_reports',compact('polls'));
	}
	
	public function getCouncillors(){
		
		$councillors = Councillor::with('ward')->get();		
		return view('pages.councillors',compact('councillors'));
	}

	public function getSearchCouncillor(Request $request){
		
		$councillor = Councillor::findOrFail($request->councillor_id);
		$engagements = CouncillorMessage::where('councillor_id','=', $request->councillor_id)->orderBy('id','DESC')->with('message_responses')->get();
		$engagements_2 = CouncillorMessage::where('councillor_id','=', $request->councillor_id)->orderBy('id','DESC')->with('message_responses')->get()->toArray();
		if(Auth::user())
		{$user_id = Auth::user()->id;}
		return view('pages.view_councillor',compact('councillor','engagements','engagements_2','user_id'));

	}

	public function postSearchCouncillors(Request $request){
		$keyword = $request['keyword'];
		$councillors =  Councillor::where('firstname', 'LIKE', '%'.$keyword.'%')->get();
		$result_2 =  Councillor::where('lastname', 'LIKE', '%'.$keyword.'%')->get();

		return view('pages.councillors', compact('councillors'));

	}

	public function getViewCouncillor($id){

		$councillor = Councillor::findOrFail($id);
		$engagements = CouncillorMessage::where('councillor_id','=', $id)->orderBy('id','DESC')->with('message_responses')->get();
		$engagements_2 = CouncillorMessage::where('councillor_id','=', $id)->orderBy('id','DESC')->with('message_responses')->get()->toArray();
		if(Auth::user())
		{$user_id = Auth::user()->id;}
		return view('pages.view_councillor',compact('councillor','engagements','engagements_2','user_id'));
	}


	public function postCouncillorMessage(MessageRequest $request){

		$c_msg= new CouncillorMessage();
		$c_msg->user_id = Auth::user()->id;
		$c_msg->councillor_id = $request['councillor_id'];
		$c_msg->message = $request['message'];

		$c_msg->save();
		\Session::flash('councillor_notitfication', 'Message posted successfully, thank you for contributing');
		return redirect('view-councillor/'.$request['councillor_id'].'#message-section');
	}

	public function postMessageResponse(MessageRequest $request){

		$r_msg= new MessageResponse();
		$r_msg->user_id = Auth::user()->id;
		$r_msg->message_id = $request['message_id'];
		$r_msg->message = $request['message'];

		$r_msg->save();
		\Session::flash('councillor_notitfication', 'Message posted successfully, thank you for contributing');
		return redirect()->back();
	}

	public function getDeleteEngagement($id){
		$msg = CouncillorMessage::findOrFail($id);
		$msg->delete();
		
		if(Auth::user())
		{$user_id = Auth::user()->id;}

		\Session::flash('councillor_notitfication', 'Message successfully deleted');
		return redirect()->back();
	}

	public function getDeleteResponse($id){
		$msg = MessageResponse::findOrFail($id);
		$msg->delete();
		
		if(Auth::user())
		{$user_id = Auth::user()->id;}

		\Session::flash('councillor_notitfication', 'Message successfully deleted');
		return redirect()->back();
	}

	public function getWards(){
		
		$wards = Ward::all();		
		return view('pages.wards',compact('wards'));
	}

	public function postSearchWard(Request $request){
		
		$wards =  Ward::where('id', '=', $request->ward_id)->get();
		return view('pages.wards', compact('wards'));

	}

	public function getBlog(){
		$this->middleware('auth');
		$blogs = Article::orderBy('id','DESC')->paginate(6);
		$active = 6;
		return view('pages.blog',compact('blogs', 'active'));
	}

	public function getArticle($id){
		$blog = Article::findOrFail($id);
		$active = 6;
		return view('pages.blogdetails',compact('blog', 'active'));
	}

	/***
	*The view function should be changed to show() in the future and should be used as shown on the next line
	*public function show($store){
		return view('store.view')->with('poll',Poll::findOrFail($store));
	}
	*The code above does not require explicitly declaring the as it is auto-generated by Route:resource
	***/
	//View is called when returning a single item to be shown

	


	public function view($id){
		return view('store.view')->with('poll',Poll::findOrFail($id));
	}
	
	public function category($cat_id){
		$polls = Poll::where('category_id','=', $cat_id)->paginate(2);
		$category = Category::findOrFail($cat_id);	
		return view('store.category',compact('polls','category') );
	}
	
	

	
	
}