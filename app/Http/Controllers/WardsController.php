<?php namespace App\Http\Controllers;

use App\Ward;
use App\Product;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Requests\WardRequest;
use Illuminate\Http\Request;
use Image;
use File;
use App\District;

class WardsController extends Controller {

	/**
	*
	*Protect our post request
	*
	*
	*
	*/

	public function __construct()
	{
		
	}


	/**
	*
	*Show all wards
	*
	*@return Response
	*
	*/
	public function index(){

		$wards= Ward::all();
		$districts = array();
		
		foreach(District::all() as $district){
			$districts[$district->id] = $district->name;
		}

		return view('wards.index',compact('wards','districts'));
	}

	/**
	*
	*Shows form for creating a single form
	*
	*@return Response
	*
	*/

	public function create(){

	return view('wards.index');

	}



	public function store(WardRequest $request)
	{


		//inserting product values
		$ward = new Ward();
		$ward->create($request->all());

		//Using $request->file('filename') gets me the file after it has been validated
		
		
		\Session::flash('message','Ward Created');
		return redirect('admin/wards');
	}


	/**
	*
	*Show single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function show($id){

		$ward = Ward::findOrFail($id);


		return view('admin/wards',compact('category')); //$contact->title
	}

	/**
	*
	*Delete single category
	*
	*param interger $id
	*
	*@return Response
	*
	*/

	public function destroy(Request $request){
		$id = $request->input('id');
		$ward= Ward::findOrFail($id);
		\Session::flash('message','Ward Deleted');
		if ($ward){
			$ward->delete();
		}

		return redirect('admin/wards');

	}



	public function edit($id)
	{
		$wards= Ward::all();
		$edit_ward = Ward::findOrFail($id);
		$edit= true;
		$districts = array();
		
		
		foreach(District::all() as $district){
			$districts[$district->id] = $district->name;
		}
		return view('wards.index',compact('edit_ward','wards','edit','districts'));

	}

	public function update($id, WardRequest $request)
	{

		$ward = Ward::findOrFail($id);
		$ward->update($request->all());

		//Using $request->file('filename') gets me the file after it has been validated
		
		$image= $request->file('image');
		if(empty($image)){
				
				


		}else{

				
				$filename =$image->getClientOriginalName();
				$path = public_path('images/wards/'.$filename);
				$relative_path = 'images/wards/'.$filename;
				Image::make($image->getRealPath())->fit(300,200)->save($path);
				$ward->image = 'images/wards/'.$filename;

				$ward->save();	
		}
		


		\Session::flash('message','Ward Updated');
		return redirect('admin/wards');

	}



}
