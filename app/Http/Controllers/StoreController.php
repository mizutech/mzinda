<?php namespace App\Http\Controllers;

use Auth;
use App\ReceptionContent;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductImage;
use App\ProductSize;
use App\Ptag;
use App\Event;
use App\EventImage;
use App\Category;
use App\Collection;
use App\CompanyDetail;
use App\CustomerOfTheWeek;
use App\FeaturedProduct;
use App\MailingList;
use App\Article;
use App\AirtelMoney;
use App\SliderImage;
use Carbon\Carbon;
use View;
use Cart;
use Mail;
use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\NewPreOrderRequest;
use App\Http\Requests\MailingListRequest;
// use DB;

class StoreController extends Controller {

	public function __construct(){
		
	}
	public function getIndex(){

		
		$slider_images= SliderImage::all();
		return view('store.index',compact('slider_images'));
	}

	public function index(){

		
		$slider_images= SliderImage::all();
		$categories = Category::all();
		$products = Product::limit(6)->orderBy('id','asc')->get();

		return view('store.index',compact('slider_images', 'categories' ,'products'));
	}

	/***
	*The view function should be changed to show() in the future and should be used as shown on the next line
	*public function show($store){
		return view('store.view')->with('product',Product::findOrFail($store));
	}
	*The code above does not require explicitly declaring the as it is auto-generated by Route:resource
	***/
	//View is called when returning a single item to be shown
	public function view($id){
		return view('store.view')->with('product',Product::findOrFail($id));
	}

	public function getCategory($id){
		$products = Product::where('category_id','=', $id)->paginate(10);
		$category = Category::findOrFail($id);
		return view('store.products',compact('products','category') );
	}

	public function getCollection($coll_id){
		$products = Product::where('collection_id','=', $coll_id)->paginate(10);
		$category = Collection::findOrFail($coll_id);
		$articles= Article::all();
		return view('store.products',compact('products','category','articles') );
	}
	public function postSearch(Request $request){
		$keyword = $request->input('keyword');
		$products =  Product::where('title', 'LIKE', '%'.$keyword.'%')->get();
		return view('store.search', compact('products'));
		// ->with('products', Product::where('title', 'LIKE', '%'.$keyword.'%')->get())
		// ->with('keyword',$keyword);

	}

	public function getLaWomann(){

		$products =Product::whereRaw('category_id = 3 and visible = 1')->orderBy('item_code', 'asc')->paginate(10);
		$articles= Article::all();
		return view('store.products',compact('products','articles'));
	}

	public function getProducts(){
		$products =Product::all();
		return view('store.products',compact('products'));
	}

	public function getLaMann(){

		$products =Product::whereRaw('category_id = 4 and visible = 1')->paginate(10);
		$articles= Article::all();
		return view('store.products',compact('products','articles'));
	}

	public function getLaBoy(){
		
	}

	/**The method is duplicated with different spellings because some links had typos with one s and stuff **/ 
	public function getAccesories(){

		$products =Product::whereRaw('category_id = 5 and visible = 1')->paginate(10);
		$articles= Article::all();
		return view('store.products',compact('products','articles'));
	}

	/**The method is duplicated with different spellings because some links had typos with one s and stuff **/ 
	public function getAccessories(){

		$products =Product::whereRaw('category_id = 5 and visible = 1')->paginate(10);
		$articles= Article::all();
		return view('store.products',compact('products','articles'));
	}

	public function getNewPreOrder($id){

		$product =Product::findOrFail($id);
		return view('store.order_now',compact('product'));
	}

	public function postNewPreOrder(NewPreOrderRequest $request){

		$email = $request->email;

		$sent = Mail::send('emails.pre_order',
        array(
            
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' =>  $request->email,
            'color' =>  $request->color,
            'size' =>  $request->size,
            'city' =>  $request->country, //We are posting the city field to the country column 
            'product_title' =>  $request->product_title,
            'product_code' =>  $request->product_code,

        ),function($message)use ($email) 
        {
	        $message->from($email);

	        $message->to('info@horizondesignsafrica.com', 'New User-Order')->subject('Pre Order');
	    });

	    /**Sending order to user**/
	    $user_sent = Mail::send('emails.new_pre_order',
        array(
            
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' =>  $request->email,
            'color' =>  $request->color,
            'size' =>  $request->size,
            'city' =>  $request->country, //We are posting the city field to the country column 
            'product_title' =>  $request->product_title,
            'product_code' =>  $request->product_code,

        ),function($message)use ($email) 
        {
	        $message->from('info@horizondesignsafrica.com');

	        $message->to($email, 'New User-Order')->subject('Pre Order');
	    });

		\Session::flash('message','You have successfully made a pre order, you will get an email response soon');
		return redirect()->back();
	}
	
	public function getAbout(){
		return view('store.about');
	}

	public function getAboutUs(){
		$about_us= CompanyDetail::where('keyword', '=', 'about-us')->get()->first();
		return view('store.aboutus',compact('about_us'));
	}
	public function getCheckout(){
		return view('store.checkout');
	}

	public function getShopNow(){
		$reception_contents = ReceptionContent::all();
		return view('store.shopnow',compact('reception_contents'));
	}

	public function getChoose(){
		$reception_contents = ReceptionContent::all();
		return view('store.shopnow',compact('reception_contents'));
	}

	public function getFaq(){
		$customers = CustomerOfTheWeek::all();
		return view('store.faq',compact('customers'));
	}
	/**
	 *Gets a single product row
	 * @return Array $product
	 */

	public function getProductDetails($id)
	{	
		
		$product= Product::findOrFail($id);
		// $product_image = ProductImage::findOrFail($id);
		$product_image= ProductImage::where('product_id', '=' , $id)->first();
	 	// $main = ProductImage::whereRaw('product_id = '. $id .' and pov = "front"')->get();
	 	// $main_img = DB::table('product_images')->whereRaw('product_id = '. $id .' and pov = "front"')->first();
		return view('store.productdetails',compact('product','product_image'));
	}
	
	/**
	*Gets all products belonging to a certain tag
	* @return Array $products
	**/
	public function getPtags($id){
		$products= Ptag::findOrFail($id)->products;
		return view('store.search',compact('products'));
	}


	public function getPayment(Request $request)
	{
		return view('store.payment');
	}

	/**
	*Produce 
	* @return Array $payment_details
	**/
	public function postPayment(Request $request)
	{
		
		return redirect('store/payment');
	}

	public function getBlog(){	
		$articles= Article::latest('published_at')->published()->paginate(2);
		return view('store.blog',compact('articles'));
	}
	public function getArticle($id){
		$articles= Article::paginate(2);
		$article = Article::findOrFail($id);
		return view('store.blogdetails',compact('article','articles'));
	}

	public function getSubscribe()
	{
		return view('store.subscribe');
	}

	public function postSubscribe(MailingListRequest $request)
	{
		$mail =	MailingList::create($request->all());
		\Session::flash('message','You are officially subscribed to our mailing list');
		
		return redirect('store/subscribe');
	}

	public function getSuccess(Request $request){

		return view('store.success');

	}	

	public function getContact(){
		return view('store.contact');
	}

	public function getCustomer(){
		$customer = CustomerOfTheWeek::latest('published_at')->published()->first();
		$customers = CustomerOfTheWeek::latest('published_at')->published()->get();
		return view('store.customer',compact('customer', 'customers'));
	}

	public function getOurCustomer($id){
		$customer = CustomerOfTheWeek::findOrFail($id);
		$customers = CustomerOfTheWeek::latest('published_at')->published()->get();
		return view('store.pre_customer',compact('customer', 'customers'));
	}


	public function postSContact(ContactFormRequest $request){

		Mail::send('emails.contactmail',
        array(
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'message' => $request['message'],
        ),function($message)
	    {
	        $message->from('info@horizondesignsafrica.com');
	        $message->to('info@horizondesignsafrica.com', 'Admin')->subject('Feedback from Contact Form on Website');
	    });
		\Session::flash('message','Message Sent');
		return redirect('store/contact');
	}

	public function postAirtel(Request $request)
	{
		AirtelMoney::create($request->all());
		\Session::flash('message','You have paid using airtel money.Please call to receive');
		return redirect('/store/success');
	}



	public function getCartLogout()
	{
		Auth::logout();

		return redirect('store/cart');
	}
	
}
