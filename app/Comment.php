<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	//
	
	protected $connection = 'sms';
	protected $table = 'comment';
	public $timestamps = false;
	protected $guarded= ['id'];

	public function report(){
		return $this->belongsTo('App\Report');
	}

}
