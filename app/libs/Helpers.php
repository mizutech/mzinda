<?php
/**
 *  Given a file, i.e. /css/base.css, replaces it with a string containing the
 *  file's mtime, i.e. /css/base.1221534296.css.
 *  
 *  @param $file  The file to be loaded.  Must be an absolute path (i.e.
 *                starting with slash).
 */
function auto_version($file)
{
  if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
    return $file;

  $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
  return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
}

function asset_timed($path, $secure=null){
    $file = public_path($path);
    if(file_exists($file)){
        return asset($path, $secure) . '?' . filemtime($file);
    }else{
        throw new \Exception('The file "'.$path.'" cannot be found in the public folder');
    }
}

function version($path){
    $file = public_path($path);
    if(file_exists($file)){
        $parts = explode( '.', $path);
        $extension = array_pop($parts);
        array_push($parts, filemtime($file), $extension);
        $path = implode('.', $parts);
        return $path;
    }else{
        throw new \Exception('The file "'.$path.'" cannot be found in the public folder');
    }
}
?>