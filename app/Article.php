<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model {

	//
	
	protected $guarded = array('id');

	protected $dates = ['published_at'];
	
	
	public function scopePublished($query){
		
			$query->where('published_at', '<=', Carbon::now());
		
	}
	
	public function scopeUnPublished($query){
		
			$query->where('published_at', '>=', Carbon::now());
		
	}
	
	public function setPublishedAtAttribute($date){
		
		$this->attributes['published_at']= Carbon::createFromFormat('Y-m-d',$date);
	}
	
	/**
	 * A article is owned by one user 
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */		
	public function User(){
		
		return $this->belongsTo('App\User');
	}


}