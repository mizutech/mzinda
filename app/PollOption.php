<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PollOption extends Model {

	//
	protected $fillable = ['poll_id','order','title'];

	public function poll(){
		return $this->belongsTo('App\Poll');
	}

	public function poll_votes(){
		return $this->hasMany('App\PollVote');
	}
	

}
