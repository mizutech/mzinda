 <script src="{{asset('assets/js/tether.js')}}"></script>
        <script src="{{asset('assets/js/shepherd.js')}}"></script>

        <?php $new_user = false; ?>
    @if($new_user == true)

     <script type="text/javascript">
            var tour = new Shepherd.Tour({
                  defaults: {
                    classes: 'shepherd-theme-arrows',
                    scrollTo: true
                  }
                });

                tour.addStep('myStep', {
                  title: 'Welcome!',
                  text: 'Hello! Welcome to the Mzinda Platform. This tour will help you get started',
                  attachTo: '',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [

                    {
                      text: 'Exit',
                      classes: 'shepherd-button-primary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'Next',
                      action: tour.next,
                      classes: 'shepherd-button-primary',
                      
                    }
                  ]
                });

                tour.addStep('step2', {
                    
                  title: 'Dashboard',
                  text: 'This is the first screen you see when you login, you can answer the latest polls and search for councillors, wards',
                  attachTo: '#nav-dashboard bottom',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [
                    {
                      text: 'Back',
                      action: tour.back,
                      classes: 'shepherd-button-example-primary'
                    }, {
                      text: 'Exit',
                      classes: 'shepherd-button-secondary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'Next',
                      action: tour.next,
                      classes: 'shepherd-button-primary',
                      
                    }
                  ]
                });
                tour.addStep('step3', {
                    
                  title: 'Citizen Reports',
                  text: 'Citizen reports shows you the incoming messages from citizens. Messages are sent by sms,email and webforms',
                  attachTo: '#citizen-reports bottom',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [
                    {
                      text: 'Back',
                      action: tour.back,
                      classes: 'shepherd-button-example-primary'
                    }, {
                      text: 'Exit',
                      classes: 'shepherd-button-secondary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'Next',
                      action: tour.next,
                      classes: 'shepherd-button-primary',
                      
                    }
                  ]
                });
                tour.addStep('step4', {
                    
                  title: 'Polls',
                  text: 'View new and old poll results, you can also vote on the polls found here',
                  attachTo: '#polls bottom',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [
                    {
                      text: 'Back',
                      action: tour.back,
                      classes: 'shepherd-button-example-primary'
                    }, {
                      text: 'Exit',
                      classes: 'shepherd-button-secondary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'Next',
                      action: tour.next,
                      classes: 'shepherd-button-primary',
                      
                    }
                  ]
                });
                tour.addStep('step5', {
                    
                  title: 'Engage your councillor',
                  text: 'Post directly to your councillors, hear their and other peoples feedback, on current issues',
                  attachTo: '#councillors bottom',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [
                    {
                      text: 'Back',
                      action: tour.back,
                      classes: 'shepherd-button-example-primary'
                    }, {
                      text: 'Exit',
                      classes: 'shepherd-button-secondary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'Next',
                      action: tour.next,
                      classes: 'shepherd-button-primary',
                      
                    }
                  ]
                });
                tour.addStep('step6', {
                    
                  title: 'Thank you',
                  text: 'Thank you for signing up. Let`s keep the conversation going. Click complete to finish the tour.',
                  attachTo: '',
                  classes: 'shepherd shepherd-open shepherd-theme-arrows shepherd-transparent-text',
                  buttons: [
                    {
                      text: 'Back',
                      action: tour.back,
                      classes: 'shepherd-button-example-primary'
                    }, 
                    {
                      text: 'Exit',
                      classes: 'shepherd-button-secondary',
                      action: function() {
                        return tour.hide();
                      }
                    },
                    {
                      text: 'complete',
                      action: tour.complete,
                      classes: 'shepherd-button-secondary'
                      
                    }
                  ]
                });
            


            tour.start();
    </script>

    @else

    @endif

    
