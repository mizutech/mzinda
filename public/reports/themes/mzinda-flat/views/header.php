<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title><?php echo $page_title.$site_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="<?php echo Kohana::config('core.site_protocol'); ?>://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700,100' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:300,700,900,500' rel='stylesheet' type='text/css'>
  <link href="<?php echo url::site();?>themes/mzinda-flat/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo url::site();?>themes/mzinda-flat/bootstrap/css/theme.css" rel="stylesheet" type="text/css">

	<!--Icon Fonts-->
        <link rel="stylesheet" media="screen" href="<?php echo url::site();?>themes/mzinda-flat/fonts/font-awesome/font-awesome.min.css" />

 
        <!-- Extras -->
        <link rel="stylesheet" href="<?php echo url::site();?>themes/mzinda-flat/flat_ui/css/flat-ui.css" />
        <link rel="stylesheet" href="<?php echo url::site();?>themes/mzinda-flat/style.css" />
	<?php echo $header_block; ?>
		<?php
	// Action::header_scripts - Additional Inline Scripts from Plugins
	Event::run('ushahidi_action.header_scripts');
	?>
</head>


<?php
  // Add a class to the body tag according to the page URI
  // we're on the home page
  if (count($uri_segments) == 0)
  {
    $body_class = "page-main";
  }
  // 1st tier pages
  elseif (count($uri_segments) == 1)
  {
    $body_class = "page-".$uri_segments[0];
  }
  // 2nd tier pages... ie "/reports/submit"
  elseif (count($uri_segments) >= 2)
  {
    $body_class = "page-".$uri_segments[0]."-".$uri_segments[1];
  }

?>

<body id="page " class="<?php echo $body_class; ?>">



 
  <!-- / searchbox -->
<div class="logo-menu" id="nav-to-top">
    <nav class="navbar navbar-default " role="navigation" data-spy="affix" data-offset-top="50">

      <div class="">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-3">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo" href="../<?php $_SERVER['DOCUMENT_ROOT']?>"><img class="logo-mzinda img-responsive" src="<?php echo url::site();?>themes/mzinda-flat/images/your_logo.png"/></a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav main-nav col-md-8 pull-right">
  
                            
                            <li ><a href="../<?php $_SERVER['DOCUMENT_ROOT']?>"><i class="fa fa-home"></i> Dashboard</a></li>
                            

                            <!--Shows more options if logged in and vice versa-->
                           
                            <li class="active"><a  href="<?php $_SERVER['DOCUMENT_ROOT']?>/reports"><i class="fa fa-comments"></i> Citizen Reports</a></li>
                            <li><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/polls"><i class="fa fa-pencil-square-o"></i> Polls</a></li>
                            
                            <li><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/councillors"><i class="fa fa-envelope"></i> Engage Councillor</a></li>
<li><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/blog"><i class="fa fa-envelope"></i>News</a></li>

                            <!--Shows Logout if logged in and vice versa-->
                          
                            <li><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/auth/logout"><i class="fa fa-user"></i>Logout</a></li>
                                                    
                            
                        </ul>
        </div>
      </div>
    </nav>
  </div>

	<!-- wrapper -->
	<div class="wrapper floatholder">

<!--Log-change:Removed header and top bar-->


         <!-- / header item for plugins -->
        <?php
            // Action::header_item - Additional items to be added by plugins
	        Event::run('ushahidi_action.header_item');
        ?>

        <?php if(isset($site_message) AND $site_message != '') { ?>
			<div class="green-box">
				<h3><?php echo $site_message; ?></h3>
			</div>
		<?php } ?>
    <!--container-->
<div class="container">
  
		<!-- main body -->
		<div id="middle">
			<div class="background layoutleft">

				<!-- mainmenu -->
				<div id="mainmenu" class="clearingfix">
					<ul class="clearingfix">
						<?php nav::main_tabs($this_page); ?>
					</ul>

				</div>
				<!-- / mainmenu -->
