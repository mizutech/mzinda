			</div>
		</div>
		<!-- / main body -->

	</div>
	<!-- / wrapper -->

	<!-- footer -->
	<div id="footer" class="clearingfix">

		<div id="underfooter"></div>

		<!-- footer content -->
		<div class="wrapper floatholder">

			<!-- footer credits -->
			
			<!-- / footer credits -->

			<!-- footer menu -->
			<div class="footermenu">
				<ul class="clearingfix">
					<li>
						<a class="item1" href="<?php echo url::site(); ?>">
							<?php echo Kohana::lang('ui_main.home'); ?>
						</a>
					</li>

					<?php if (Kohana::config('settings.allow_reports')): ?>
					<li>
						<a href="<?php echo url::site()."reports/submit"; ?>">
							<?php echo Kohana::lang('ui_main.submit'); ?>
						</a>
					</li>
					<?php endif; ?>

					<?php if (Kohana::config('settings.allow_alerts')): ?>
					<li>
						<a href="<?php echo url::site()."alerts"; ?>">
							<?php echo Kohana::lang('ui_main.alerts'); ?>
						</a>
					</li>
					<?php endif; ?>

					<?php if (Kohana::config('settings.site_contact_page')): ?>
					<li>
						<a href="<?php echo url::site()."contact"; ?>">
							<?php echo Kohana::lang('ui_main.contact'); ?>
						</a>
					</li>
					<?php endif; ?>

					<?php
					// Action::nav_main_bottom - Add items to the bottom links
					Event::run('ushahidi_action.nav_main_bottom');
					?>

				</ul>
				<?php if ($site_copyright_statement != ''): ?>
	      		<p><?php echo $site_copyright_statement; ?></p>
		      	<?php endif; ?>
			</div>
			<!-- / footer menu -->


		</div>
		<!-- / footer content -->

	</div>

	<div class="footer">
<!-- Conatct Section -->
    <section id="contact">
    <div class="col-sm-12 text-center">
    <div class="row">
    <h1 class="title contact-header">Contact us</h1>

    <div class="col-md-6 wow fadeInRight">
        <div class="contact-info">
            <p><i class="fa fa-map-marker"></i>Blantyre ,Malawi</p>
             <p><i class="fa fa-envelope"></i> info@mzinda.org</p>
        </div>
            
    </div>
    <div class="col-md-4 wow fadeInRight">
    <div class="social-links">
    <a class="social" href="#" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
    <a class="social" href="#" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
    <a class="social" href="#" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>
    <a class="social" href="#" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
    </div>
       

    </div>
    </div>
    </section>

<!-- Conatct Section End-->

    <div id="copyright">
    <div class="container">
    <div class="col-md-10"><p>© Mzinda 2015</p></div>
    <div class="col-md-2">
        <span class="to-top pull-right"><a href="#nav-to-top"><i class="fa fa-angle-up fa-2x"></i></a></span>
        </div>
    </div>
    </div>
<!-- Copyright Section End-->
</div>
<!--Footer ends-->



	<!-- / footer -->

	<?php
	echo $footer_block;
	// Action::main_footer - Add items before the </body> tag
	Event::run('ushahidi_action.main_footer');
	?>
</body>
</html>
