<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title><?php echo $page_title.$site_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="<?php echo Kohana::config('core.site_protocol'); ?>://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet" type="text/css">
	<link href="<?php echo url::site();?>themes/mzinda/css/mzinda/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo url::site();?>themes/mzinda/css/mzinda/main.css" rel="stylesheet" type="text/css">

	<!--Icon Fonts-->
        <link rel="stylesheet" media="screen" href="<?php echo url::site();?>themes/mzinda/fonts/font-awesome/font-awesome.min.css" />

 
        <!-- Extras -->
        <link rel="stylesheet" href="<?php echo url::site();?>themes/mzinda/css/extra/skel.css" />
        <link rel="stylesheet" href="<?php echo url::site();?>themes/mzinda/css/extra/style.css" />
        <link rel="stylesheet" href="<?php echo url::site();?>themes/mzinda/css/extra/style-desktop.css" />
	<?php echo $header_block; ?>
		<?php
	// Action::header_scripts - Additional Inline Scripts from Plugins
	Event::run('ushahidi_action.header_scripts');
	?>
</head>


<?php
  // Add a class to the body tag according to the page URI
  // we're on the home page
  if (count($uri_segments) == 0)
  {
    $body_class = "page-main";
  }
  // 1st tier pages
  elseif (count($uri_segments) == 1)
  {
    $body_class = "page-".$uri_segments[0];
  }
  // 2nd tier pages... ie "/reports/submit"
  elseif (count($uri_segments) >= 2)
  {
    $body_class = "page-".$uri_segments[0]."-".$uri_segments[1];
  }

?>

<body id="page " class="<?php echo $body_class; ?>">



 
  <!-- / searchbox -->
<div class="logo-menu" id="nav-to-top">
    <nav class="navbar navbar-default " role="navigation" data-spy="affix" data-offset-top="50">
    <!-- <div class="container">
                            <div class="row-special">
                                <div class="col-sm-12 overflow">
                                   <div class="social-icons pull-right">
                                        <ul class="nav nav-pills">
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div> 
                                </div>
                             </div>
                        </div> -->
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-3">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo" href=""><img width="200px" height="56px" src="<?php echo url::site().'themes/mzinda/images/mzinda-logo.png'?>"/></a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav main-nav col-md-8 pull-right">

                            
                             
                            
                            <li class="active"><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/mzinda/public"><i class="fa fa-home"></i> Home</a></li>
                            

                            <!--Shows more options if logged in and vice versa-->
                           
                            <li><a href="{{url('/citizen-reports')}}"><i class="fa fa-comments"></i> Citizen Reports</a></li>
                            <li><a href="{{url('/polls')}}"><i class="fa fa-pencil-square-o"></i> Polls</a></li>
                            
                            <li><a href="{{url('/councillors')}}"><i class="fa fa-envelope"></i> Engage Your Councillor</a></li>

                            
                           
                            <!--Shows Logout if logged in and vice versa-->
                          
                            <li><a href="{{url('auth/logout')}}"><i class="fa fa-user"></i> Logout</a></li>
                           
                            <li><a href="{{url('/blog')}}"><i class="fa fa-info"></i> News</a></li>
                             
                            
                        </ul>
        </div>
      </div>
    </nav>
  </div>

	<!-- wrapper -->
	<div class="wrapper floatholder">

		<!-- header -->
		<div id="header">

			<!-- logo -->
			<div id="logo">
				 <!-- top bar-->
					  <div id="top-bar">
					    <!-- searchbox -->
							<div id="searchbox">

								<!-- languages -->
								<?php echo $languages;?>
								<!-- / languages -->

								<!-- searchform -->
								<?php echo $search; ?>
								<!-- / searchform -->

						    </div>
					  </div>
			</div>
			
			<!-- / logo -->

			<!-- submit incident -->
			<?php echo $submit_btn; ?>
			<!-- / submit incident -->

			<?php
				// Action::main_sidebar - Add Items to the Entry Page Sidebar
				Event::run('ushahidi_action.main_sidebar');
			?>

		</div>
		<!-- / header -->
         <!-- / header item for plugins -->
        <?php
            // Action::header_item - Additional items to be added by plugins
	        Event::run('ushahidi_action.header_item');
        ?>

        <?php if(isset($site_message) AND $site_message != '') { ?>
			<div class="green-box">
				<h3><?php echo $site_message; ?></h3>
			</div>
		<?php } ?>

		<!-- main body -->
		<div id="middle">
			<div class="background layoutleft">

				<!-- mainmenu -->
				<div id="mainmenu" class="clearingfix">
					<ul class="clearingfix">
						<?php nav::main_tabs($this_page); ?>
					</ul>

				</div>
				<!-- / mainmenu -->
