<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturedProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('featured_products',function($table){
			$table->increments('id');
			$table->unsignedInteger('product_id');
			$table->string('position');
			$table->integer('width');
			$table->integer('height');
			$table->string('style')->nullable;
			
			$table->foreign('product_id')->references('id')->on('products');	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('featured_products');
	}

}
