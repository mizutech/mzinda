<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders',function($table){
				//
				$table->increments('id');
				$table->unsignedInteger('product_id');
				$table->unsignedInteger('user_id');
				$table->timestamp('time');
				$table->integer('quantity');
				$table->tinyint('payment_made');
				$table->string('');

				$table->foreign('product_id')->references('id')->on('products');
				$table->foreign('user_id')->references('id')->on('users');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('orders');
	}

}
