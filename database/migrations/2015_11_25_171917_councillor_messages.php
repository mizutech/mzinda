<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CouncillorMessages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('councillor_messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('councillor_id')->unsigned();
			$table->string('message');
			$table->timestamps();

			$table->foreign('councillor_id')
				  ->references('id')
				  ->on('councillors');


			$table->foreign('user_id')
				  ->references('id')
				  ->on('users');		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('councillor_messages');
	}

}
