<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Councillors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('councillors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('district_id')->unsigned();
			$table->integer('ward_id')->unsigned();
			$table->string('firstname');
			$table->string('lastname');
			$table->string('image')->nullable();
			$table->integer('age');			
			$table->timestamps();

			$table->foreign('district_id')
				  ->references('id')
				  ->on('districts');

			$table->foreign('ward_id')
				  ->references('id')
				  ->on('wards');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('councillors');
	}

}
