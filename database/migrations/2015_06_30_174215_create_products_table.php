<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('products',function($table){
			$table->increments('id');
			$table->unsignedInteger('category_id');
			$table->unsignedInteger('collection_id');
			$table->unsignedInteger('admin_id');
			$table->string('title');
			$table->text('description');
			$table->text('desc_excerpt');
			$table->decimal('price', 6, 2);
			$table->boolean('availability')->default(1);
			$table->string('image');
			$table->timestamps();	
			
			$table->foreign('category_id')->references('id')->on('categories');
			$table->foreign('collection_id')->references('id')->on('collections');
			$table->foreign('admin_id')->references('id')->on('users');		
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('products');
	}

}
