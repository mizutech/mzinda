<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('polls', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('category_id');//
			$table->unsignedInteger('admin_id');//not done
			$table->string('title');//
			$table->string('image');//
			$table->text('description');//
			$table->text('desc_excerpt');//
			$table->integer('comments_active')->default(1);//
			$table->boolean('members_only')->default(1);
			$table->boolean('active')->default(1);//
			$table->timestamp('start_date');
			$table->timestamp('end_date');
			$table->timestamps();	
			
			$table->foreign('category_id')->references('id')->on('categories');
			$table->foreign('admin_id')->references('id')->on('users');		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('polls');
	}

}
