<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishlistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('wishlist',function($table){
				$table->increments('id');
				$table->unsignedInteger('product_id');
				$table->integer('size');
				$table->timestamps();

				$table->foreign('product_id')->references('id')->on('products');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('wishlist');
	}

}
