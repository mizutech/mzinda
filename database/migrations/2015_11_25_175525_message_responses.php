<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageResponses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('message_responses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('message_id')->unsigned();
			$table->string('message');
			$table->timestamps();

			$table->foreign('message_id')
				  ->references('id')
				  ->on('councillor_messages');


			$table->foreign('user_id')
				  ->references('id')
				  ->on('users');		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('message_responses');
	}

}
