<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('wards', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('district_id')->unsigned();
			$table->string('name');
			$table->string('image');
			$table->integer('size')->nullable();
			$table->integer('population');
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->timestamps();

			$table->foreign('district_id')
				  ->references('id')
				  ->on('districts');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('wards');
	}

}
