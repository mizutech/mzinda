<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('billing_details',function(Blueprint $table){

			$table->string('u_id');
			$table->string('payment_method');
			$table->string('billing_address');
			$table->string('card_number')->nullable();

			$table->foreign('u_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('billing_details');	
	}

}
