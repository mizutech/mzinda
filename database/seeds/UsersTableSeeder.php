<?php
	
	class UsersTableSeeder extends Seeder{
		
		public function run(){
		$user = new User();
		$user->firstname= 'Jon'; 
		$user->lastname = 'Doe' ;
		$user->email= 'jon@doe.com'; 
		$user->password= Hash::make('password' );
		$user->telephone= '55778899' ;
		$user->admin= '1' ;
		$user->save();
		
		}
		
	}