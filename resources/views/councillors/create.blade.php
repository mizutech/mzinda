
                        <h3>Create new Councillor</h3>
                      @if($errors->has())
                      <div id="form-errors">  
                        <p>The following errors have occured</p> 
                        <ul>
                          @foreach($errors->all() as $error)
                            <li class="callout callout-danger">{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->    
                      @endif
                
                
                {!! Form::open(['url'=>'admin/councillors','files'=> true, 'class'=>'login-form']) !!}
         
               <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('Specify the category in the box below',null,['class'=>'form-control']) !!}
                        {!! Form::select('ward_id',$wards,['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>

               

                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('firstname','First Name')!!}
                        {!! Form::text('firstname', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('lastname','Last Name')!!}
                        {!! Form::text('lastname', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                 <div class="col-sm-12">
                    <div class="form-group"> 
                        {!! Form::label('bio','Biography')!!}
                        {!! Form::textarea('bio',null,['class'=>'form-control ckeditor' ,'rows'=>'2', 'id'=>'form-field-1'])!!}
                      </div>
                  </div>

                  <div class="col-sm-6">
                   <div class=" form-group">
                       {!! Form::label('image',' Image')!!}
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail"><img src="{{url('images/default-upload.png')}}" alt=""/>
                          </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <div>
                             <span class="btn btn-primary btn-file">
                                 <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                 <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                     
                                  {!! Form::file('image')!!}
                              </span>
                               <a href="#" class="btn fileupload-exists btn-primary" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                       </div>
                   </div>
                   </div>

                 <div class="col-sm-6">
                  <div class=" form-group">
                       {!! Form::label('image_landscape','Landscape Image')!!}
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail"><img src="{{url('images/default-upload.png')}}" alt=""/>
                          </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <div>
                             <span class="btn btn-primary btn-file">
                                 <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                 <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                     
                                  {!! Form::file('image_landscape')!!}
                              </span>
                               <a href="#" class="btn fileupload-exists btn-primary" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                       </div>
                   </div>
                  </div>
              
               
                  <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('age','Age:') !!}
                         {!! Form::text('age',null,['class' => 'required form-input']) !!}
                         </div>
                    </div>



                 <div class="box-footer">
                    {!! Form::submit('Save Councillor', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}               
                  