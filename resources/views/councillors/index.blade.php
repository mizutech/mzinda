@extends('adminApp')

@section('header')
<h1>
     Councillor Content
    <small>Manage the polls</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')
<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Councillor List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
      <div class="box-body">
										<table class="table table-striped table-hover" id="sample-table-2">
											<thead>
												<tr>
													<th class="center">#</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Image</th>
													<th>landscape Image</th>
													
													<th>Rating</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												 @foreach($councillors as $councillor)
												<tr>
													<td class="center"> {{$councillor->id}}</td>
													<td class="hidden-xs">{{$councillor->firstname}}</td>
													<td class="hidden-xs">{{$councillor->lastname}}</td>
													<td class="hidden-xs"><img width="100"src="{{url($councillor->image)}}"/></td>
													<td class="hidden-xs"><img width="200" src="{{url($councillor->image_landscape)}}"/></td>

													<td>
													<h3>{{$councillor->rating}}</h3>
													{!! Form::open(['url'=>'admin/update-rating']) !!}														
														 
						                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
						                                    <input type="hidden" name="councillor_id" value="{{$councillor->id}}"/>
						                                    <select class="btn btn-info f-area-score"  name="rating" onchange="this.form.submit()">
						                                    @for($i=1;  $i<=100; $i++)
						                                        <option value="{{$i}}" selected="selected">{{$i}}</option>
						                                    @endfor
						                                    </select>                    
						                                  {!! Form::close() !!}
													</td>

													<td class="center">
													<div class="visible-md visible-lg hidden-sm hidden-xs">
														
						                                  <a href="{{url('admin/councillors/'.$councillor->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a>
						                                
														     {!! Form::open(['url'=>'admin/councillors/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['CouncillorsController@delete', $councillor->id]]) !!}
																	  {!! Form::hidden('id', $councillor->id) !!}
	 																{!! Form::submit('Delete',array('class' => 'btn btn-danger')) !!}
																	  {!! Form::close() !!}
													</div>
													
													</td>

													</tr>					
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<!-- end: BASIC TABLE PANEL -->
      </div>
    </div>
                        <!-- end: TABLE WITH IMAGES PANEL -->


                    
 
      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Councillor Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('councillors.edit')
                @else
                 @include('councillors.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->
		
			
			
		
		</div>
 
</div><!--end admin-->
	 

@stop