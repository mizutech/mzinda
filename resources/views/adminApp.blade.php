<!DOCTYPE html>
<html lang="en" dir="ltr" class="no-js">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex">
      <meta name="robots" content="nofollow" >
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">
      <link rel="shortcut icon" href="{{url('favicon.ico?v=2')}}" />
      <title>Mzinda's Control Panel</title>
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="{{asset('admin/bootstrap/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
<!--==Form elements ==-->
      <link rel="stylesheet" href="{{ asset('admin/plugins/select2/select2.min.css') }}">

      <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
      <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
            page. However, you can choose any other skin. Make sure you
            apply the skin class to the body tag so the changes take effect.
      -->
      <link rel="stylesheet" href="{{asset('admin/dist/css/skins/skin-black-light.min.css')}}">

      <!--Default css styles for some plugins are found in this file-->
      <link rel="stylesheet" href="{{ asset('admin/plugins/plugins.css') }}"> 
      <!--Css for bootstrap file uploads plugin-->
      <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
      <link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
     <!--==Form elements ==-->

      @yield('css')
  </head>
    
   <body class="hold-transition skin-black-light sidebar-mini">


      <div class="wrapper">
          <!-- Main Header -->
          <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
              <!-- mini logo for sidebar mini 50x50 pixels -->
              <span class="logo-mini"><b>M</b>A</span>
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg"><b>Mzinda's</b> Admin</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
              </a>
              <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Messages: style can be found in dropdown.less-->
                 

                
                  <!-- User Account Menu -->
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <!-- The user image in the navbar-->
                      <img src="{{url('admin/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                      <!-- hidden-xs hides the username on small devices so only the image appears. -->
                      <span class="hidden-xs">{{Auth::user()->firstname.' '.Auth::user()->lastname}}</span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- The user image in the menu -->
                      <li class="user-header">
                        <img src="{{url('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                        <p>
                          {{Auth::user()->firstname.' '.Auth::user()->lastname}}
                          <small>Member since Nov. 2015</small>
                        </p>
                      </li>
                      <!-- Menu Body -->
                      <li class="user-body">
                        <div class="col-xs-4 text-center">
                          <a href="#">Followers</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">Friends</a>
                        </div>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="{{url('administrator/auth/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <!-- Control Sidebar Toggle Button -->
                  <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
        
          <!-- Left side column. contains the logo and sidebar -->
          <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

              <!-- Sidebar user panel (optional) -->
              <div class="user-panel">
                <div class="pull-left image">
                  <img src="{{url('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                  <p>{{Auth::user()->firstname.' '.Auth::user()->lastname}}</p>
                  <!-- Status -->
                  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
              </div>

              <!-- search form (Optional) -->
              <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
              <!-- /.search form -->

              <!-- Sidebar Menu -->
              <ul class="sidebar-menu">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                 <li>
                            <a href="{{url('administrator')}}"><i class="fa fa-home"></i> <span class="title"> Administrators</span> </a>
                         </li>

                         <li>
                            <a href="{{url('reports/admin')}}"><i class="fa fa-users"></i> <span class="title"> Ushahidi-Mzinda Admin Panel</br><!--(NB:You will re-login </br>with same details)--></span> </a>
                         </li>

                     	  <li>
                            <a href="{{url('admin/home-contents')}}"><i class="fa fa-folder-open"></i> <span class="title"> Home Contents</span> </a>
                         </li>
                        <!-- <li>
                            <a href="{{url('admin/slider')}}"><i class="fa fa-image"></i> <span class="title"> Slider</span> </a>
                         </li>-->
                         <li>
                            <a href="{{url('admin/categories')}}"><i class="fa fa-sitemap"></i> <span class="title">Poll Categories</span> </a>
                         </li>
                         <li>
                            <a href="{{url('admin/polls')}}"><i class="fa fa-folder-open"></i> <span class="title">Polls</span> </a>
                         </li>

                         <li>
                            <a href="{{url('admin/wards')}}"><i class="fa fa-sitemap"></i> <span class="title">Wards</span> </a>
                         </li>
                         <li>
                            <a href="{{url('admin/councillors')}}"><i class="fa fa-th-large"></i> <span class="title"> Councillors</span> </a>
                         </li>
                          
                           <li>
                            <a href="{{url('admin/articles')}}"><i class="fa fa-pencil"></i> <span class="title">Articles</span> </a>
                         </li>
                         
                         
                         
                      
                        
                        <li>
                          <a href="{{url('admin/users')}}"><i class="fa fa-users"></i> <span class="title">Site Users</span> </a>
                        </li>
              </ul><!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
          </aside>
           
      
             
       <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              @yield('header')
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
              @if (Session::has('message')) <div class="callout callout-success">{{ Session::get('message') }}</div> @endif
               @if (Session::has('error')) <div class="callout callout-danger">{{ Session::get('error') }}</div> @endif  

               @yield('content')
                <!-- Your Page Content Here -->
                </div>
            </section><!-- /.content -->



          </div>
      <!-- /.content-wrapper -->

          
        
          <!-- Main Footer -->
          <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
              Digital Innovations
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2015 <a href="#">Mzinda</a>.</strong> All rights reserved.
          </footer>
          <!-- end: FOOTER -->
      </div>
      <!-- Wrapper End -->


      <!-- ============================ JavaScript Files====================== -->
      <!-- ============================ Admin JavaScript Files====================== -->
      <!--[if lt IE 9]>
      <script src="{{ asset('admin/plugins/respond.min.js') }}"></script>
         <!-- REQUIRED JS SCRIPTS -->

      <!-- jQuery 2.1.4 -->
      <script src="{{asset('admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
      <!-- Bootstrap 3.3.5 -->
      <script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{asset('admin/dist/js/app.min.js')}}"></script>

       <!-- AdminLTE App -->
      <!-- <script src="{{asset('admin/plugins/ckeditor_windows/ckeditor.js')}}"></script>-->

      <!-- CK Editor -->
      <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
      <!--==Form Scripts ==-->
      <!--Javascript for bootstrap file uploads plugin-->
      <script src="{{asset('admin/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
      <script src="{{asset('admin/plugins/select2/select2.full.min.js')}}"></script>
      <link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}">
      <!--==end :Form Scripts ==-->

      <!-- end: MAIN JAVASCRIPTS -->
      <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
       @yield('scripts')
       <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->


       <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
       
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        // Replace the <textarea id="editor2"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor2');
        // Replace the <textarea id="editor3"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor3');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>
      <!-- ============================End Admin JavaScript Files====================== -->
   </body>
</html>
