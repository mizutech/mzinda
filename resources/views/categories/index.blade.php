@extends('adminApp')

@section('header')
<h1>
     Category Content
    <small>Manage the polls</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')
<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Categorys List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
      <div class="box-body">
										<table class="table table-striped table-hover" id="sample-table-2">
											<thead>
												<tr>
													<th class="center">#</th>
													<th>Name</th>
													<th>Thumbnail</th>
													<th class="hidden-xs">Active</th>
													
													<th></th>
												</tr>
											</thead>
											<tbody>
												 @foreach($categories as $category)
												<tr>
													<td class="center"> {{$category->id}}</td>
													<td class="hidden-xs">{{$category->name}}</td>
													<td class="hidden-xs"><img src="{{url($category->thumbnail)}}"/></td>

													<td class="center">
													<div class="visible-md visible-lg hidden-sm hidden-xs">
														
						                                  <a href="{{url('admin/categories/'.$category->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a>
						                                
														     {!! Form::open(['url'=>'admin/categories/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['CategoriesController@delete', $category->id]]) !!}
																	  {!! Form::hidden('id', $category->id) !!}
	 																{!! Form::submit('Delete',array('class' => 'btn btn-danger')) !!}
																	  {!! Form::close() !!}
													</div>
													<div class="visible-xs visible-sm hidden-md hidden-lg">
														<div class="btn-group">
															<a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
																<i class="fa fa-cog"></i> <span class="caret"></span>
															</a>
															<ul role="menu" class="dropdown-menu pull-right dropdown-dark">
																<li>
																	<a role="menuitem" tabindex="-1" href="#">
																		<i class="fa fa-edit"></i> Edit
																	</a>
																</li>
																<li>
																	<a role="menuitem" tabindex="-1" href="#">
																		<i class="fa fa-share"></i> Share
																	</a>
																</li>
												</div>
															</div>
														</td>
													</tr>					
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<!-- end: BASIC TABLE PANEL -->
      </div>
    </div>
                        <!-- end: TABLE WITH IMAGES PANEL -->


                    
 
      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Category Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('categories.edit')
                @else
                 @include('categories.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->
		
			
			
		
		</div>
 
</div><!--end admin-->
	 

@stop