<section id="register" class="register">				
	    <div class="container">
	      <div class="row">
	        
	        <div class="col-lg-12">
	          <div class="login-box login-page">
	            <h3>Edit a Category: {{$category->name}} </h3>
			          @if($errors->has())
			          <div id="form-errors">  
			            <p>The following errors have occured</p> 
			            <ul>
			              @foreach($errors->all() as $error)
			               <li>{{ $error }}</li>
			              @endforeach
			              </ul>
			             </div><!--end form errors-->    
			          @endif

			    {!! Form::model($category, ['method' => 'PATCH','files'=>true,'action' => ['CategoriesController@update', $category->id]]) !!}
					   <div class="form-group">
					      <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
					    {!! Form::label('name',null,['class'=>'form-control']) !!}
					    {!! Form::text('name',null,['class'=>'form-control']) !!}
							 </div> 
						 </div>	 
					 <div class="form-group">
			                    <div class="col-sm-9">
			                        {!! Form::label('image','Choose a thumbnail to represent the category')!!}
			                        {!! Form::file('thumbnail',null,['class'=>'form-price form-control'])!!}
			                        </div>
			         </div>
			  
				 <div class="form-bottom">
			              <span class="pull-left">

			              </span>
			    {!! Form::submit('Save Category', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
			 	  </div>
			            
			            
			     {!! Form::close() !!}
	         
	        </div>
	      </div>
	    </div>
	  </section>