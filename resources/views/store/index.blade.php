@extends('app')

@section('slider')
<section>
            <div class="revolution-container">
                <div class="revolution">
                    <ul class="list-unstyled">  <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="img/preview/slider/bg_1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="160"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="300"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                               <!--  <img src="img/preview/slider/1.png" alt=""> -->
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="224"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                             <!--    <img src="img/preview/slider/1-2.png" alt=""> -->
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="335"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                               <!--  <img src="img/preview/slider/1-1.png" alt=""> -->
                            </div>
                         <!--   <div class="tp-caption customin customout hidden-xs"
                                 data-x="20"
                                 data-y="430"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1000"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <a href="#" class="btn-home">Shop All</a>
                            </div>-->
                            <div class="tp-caption customin customout hidden-xs"
                                 data-x="145"
                                 data-y="430"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <a href="#" class="btn-home">Read more</a>
                            </div>
                        </li>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="img/preview/slider/bg_2.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="204"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <!-- <img src="img/preview/slider/1-2.png" alt=""> -->
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="315"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                               <!--  <img src="img/preview/slider/1-1.png" alt=""> -->
                            </div>
                           <!-- <div class="tp-caption customin customout hidden-xs"
                                 data-x="20"
                                 data-y="410"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1000"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 3">
                                <a href="#" class="btn-home">Shop All</a>
                            </div>-->
                            <div class="tp-caption customin customout hidden-xs"
                                 data-x="145"
                                 data-y="410"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                                <a href="#" class="btn-home">Read more</a>
                            </div>


                        </li>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="img/preview/slider/bg_4.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="160"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="300"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                                <!-- <img src="img/preview/slider/1.png" alt=""> -->
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="224"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                                <!-- <img src="img/preview/slider/1-2.png" alt=""> -->
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="335"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                               <!--  <img src="img/preview/slider/1-1.png" alt=""> -->
                            </div>
                            <div class="tp-caption customin customout hidden-xs"
                                 data-x="20"
                                 data-y="430"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1000"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <a href="#" class="btn-home">Shop All</a>
                            </div>
                            <div class="tp-caption customin customout hidden-xs"
                                 data-x="145"
                                 data-y="430"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <a href="#" class="btn-home">Read more</a>
                            </div>
                        </li>

                    </ul>
                    <div class="revolutiontimer"></div>
                </div>
            </div>
        </section>

@stop

@section('content')
<!--  Latest News-->
   <section>
     
            <div class="color-scheme-2">
                <div class="container">
                <h1 class="wow fadeInRight animated" data-wow-duration="1s">LATEST <span>NEWS</span>  </h1>
                    <div class="row">
                       
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <article class="banner">
                                <a href="#">
                                    <img src="img/product/category_3.jpg" class="img-responsive" alt="">
                                </a> 
                            </article>
                        </div>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                            <article class="banner">
                                <a href="#">
                                    <img src="img/product/category_3.jpg" class="img-responsive" alt="">
                                </a> 
                            </article>
                        </div>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                            <article class="banner">
                                <a href="#">
                                    <img src="img/product/category_3.jpg" class="img-responsive" alt="">
                                </a> 
                            </article>
                        </div>
                    </div>
                </div>  
            </div>
        </section>
  <!--  Latest News Section End--> 
 
<!--  Straight from the runway -->
<section>
            <div class="block color-scheme-2">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">STRAIGHT FROM THE <span>RUNWAY</span></h1>
                        <div class="toolbar-for-light" id="nav-bestseller">
                            <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div id="owl-bestseller"  class="owl-carousel">
                    @foreach($products as $product)
                        <div class="text-center">
                            <article class="product light">
                                <figure class="figure-hover-overlay">                                                                        
                                    <a href="#"  class="figure-href"></a>
                                    <div class="product-new">new</div>
                                    <div class="product-sale">11% <br> off</div>
                                    <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                    <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                    {!! Html::image($product->image,$product->name,array('class'=>'img-responsive'))!!}
                                </figure>
                                <div class="product-caption">
                                    <div class="block-name">
                                        <a href="#" class="product-name">{{$product->title}}</a>
                                        <p class="product-price"><span>{{$product->old_price}}</span> ${{$product->price}}</p>

                                    </div>

                                    {!! Form::open(array('url'=>'store/addtocart'))!!}
                                    {!! Form::hidden('quantity','1')!!}
                                    {!! Form::hidden('id',$product->id)!!}
                                    <div class="product-cart">
                                    <button type="submit" class="lilly-cart-btn"><i class="fa fa-shopping-cart"></i> </button></div>
                                    {!!Form::close()!!} 
                                    

                                    <div class="product-rating">
                                        <div class="stars">
                                            <span class="star"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                        </div>
                                        <a href="" class="review">8 Reviews</a>
                                    </div>
                                    <p class="description">{{$product->description}}</p>
                                </div>

                            </article>
                        </div>
                    @endforeach    
                       




                    </div>
                </div>
            </div>
        </section>
  <!--  Straight from the runway End --> 

 <!--  Lilly's Special Selection start --> 
 <section id="sale-section">
            <div class="block color-scheme-white-90">
                <div class="container">
                    <div class="header-for-light  hidden-xs hidden-sm">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">LILLY'S <span>SPECIAL SELECTION</span></h1>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="title-block light wow fadeInLeft">

                                <h2>Summer sale collection</h2>
                                <h1>Lilly Designs </h1>
                                <p>Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla auctor fringilla. </p>
                                <div class="text-center">
                                    <div class="toolbar-for-light" id="nav-summer-sale">
                                        <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                                        <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div id="owl-summer-sale"  class="owl-carousel">
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <div class="product-sale">11% <br>off</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                            <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                            <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                            <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <div class="product-sale">9% <br>off</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                            <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>

                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <div class="product-sale">12% <br>off</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                            <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                            </div>  
                        </div>
                    </div>

                </div>
            </div>
        </section>
 <!--  Lilly's Special Selection End --> 

<!--Bestsellers,new blog posts,others-->

        <section>
            <div class="block ">
                <div class="container">
                    <div class="row">
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-thumbs-up"></i> Bestseller
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-user"></i>WORDS FROM A DESIGNER
                            </div> 
                            <p>
                                <span class="dropcap"></span>"Everywhere where fashion is, I want to be there, and I see myself making it. I’m not doubting on that one. I dream big and I’m dreaming in colour. I invite you to join me in dreaming big ,see yourself where you want to be and starting taking the steps towards that place. Its only impossible if you don't try." 
                            </p>
                            <blockquote>
                              
                            </blockquote>
                        </article>
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-comments"></i> Latest Post From Lilly's Corner
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4  col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="img/blog/blog-1.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">My work is a recreation of my feelings.</a>

                                        </div>
                                        <p class="description">I recently spoke to CNN about my entry into the fashion industry and how i feels about my work. Take a look and..</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="img/blog/blog-2.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">NOMINATED FOR THE BULGARIAN FASHION AWARDS</a>

                                        </div>
                                        <p class="description">Words cannot describe my excitement at being nomi...</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="img/blog/blog-3.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">THANK YOU ALL</a>

                                        </div>
                                        <p class="description">I would like to take this chance to thank everybody...</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </section>
<!--End Bestsellers-->

  <!--New Collections-->
  <section class="block-chess-banners">
            <div class="block">
                <div class="collection-container">
                    <div class="header-for-dark">
                        <h1 style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="wow fadeInRight animated animated" data-wow-duration="2s">New <span>Collections</span></h1>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInLeft animated" data-wow-duration="2s">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="#">{!!Html::image('images/collections/collection1.jpg','Wedding Collection',array('class'=>'img-responsive'))!!}</a>

                                    </div>
                                    <div class="col-md-8">
                                        <div class="chess-caption-right">
                                            <a href="store/category/4" class="col-name">Wedding Collection - Elite</a>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-3">
                            <article style="visibility: visible; animation-duration: 2s;" class="block-chess wow fadeInRight animated" data-wow-duration="2s">
                                <a href="#">{!!Html::image('images/collections/collection2.jpg','Wedding Collection',array('class'=>'img-responsive'))!!}</a>
                            </article>
                        </div>
                    </div> 
                    <div class="row">

                        <div class="col-md-3">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInLeft animated" data-wow-duration="2s">
                                <a href="#">{!!Html::image('images/collections/gcc_logo.png','Wedding Collection',array('class'=>'img-responsive'))!!}</a>
                            </article>
                        </div>
                        <div class="col-md-9">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInRight animated" data-wow-duration="2s">
                                <div class="row">

                                    <div class="col-md-8">
                                        <div class="chess-caption-left">
                                            <a href="store/category/1" class="col-name">The Gentleman's Class Collection</a>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="#">{!!Html::image('images/collections/gcc_collection.jpg','Wedding Collection',array('class'=>'img-responsive'))!!}</a> 
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  <!--End New Collections-->
  
  <!--  New Collection Start -->
  
         
         <!--Category list starts here-->
          <div class="col-lg-4 col-sm-4">  
           <ul class="category-list">
             @foreach($cat_nav as $cat)
            <li> {!! Html::link('/store/category/'.$cat->id, $cat->name) !!} </li>
              @endforeach
          </div>
           <!--End Category list-->
          
      
                  </div>
    </div>
  </section>
  <!--  New Collection End -->


@stop