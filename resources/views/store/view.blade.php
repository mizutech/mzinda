@extends('app')

@section('content')
 <!--  About Section Start-->
  
  <section class="products grid-view">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <h2 class="main-title">Products List View</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-sm-3 sidebar"> 
          
          <!-- Side Bar-->
          <div class="category widget">
            <h3>Category</h3>
            <ul>
              <li><a href="#"> Casual Wear </a> <span>10</span></li>
              <li><a href="#"> Jeans</a> <span>22</span></li>
              <li><a href="#"> Tops</a> <span>20</span></li>
              <li><a href="#"> Jackets</a> <span>18</span></li>
              <li><a href="#">Night Wear</a> <span>26</span></li>
              <li><a href="#">Party Wear</a> <span>28</span></li>
            </ul>
          </div>
          <div class="featured widget">
            <h3>Featured Items</h3>
            <ul>
              <li class="fp-item"> <img src="images/fp3.jpg" alt="">
                <h4>Product Name</h4>
                <p>Maecenas laoreet lectus est, eget ultricies eros. </p>
                <a href="#">Details</a> </li>
              <li class="fp-item"> <img src="images/fp1.jpg" alt="">
                <h4>Product Name</h4>
                <p>Maecenas laoreet lectus est, eget ultricies eros. </p>
                <a href="#">Details</a> </li>
              <li class="fp-item"> <img src="images/fp2.jpg" alt="">
                <h4>Product Name</h4>
                <p>Maecenas laoreet lectus est, eget ultricies eros. </p>
                <a href="#">Details</a> </li>
            </ul>
          </div>
          
        </div>
        <div class="col-lg-9 col-sm-9">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="pro-details-slider">
                  <ul class="prod-slider">
                    <li> {!! Html::image($product->image,$product->title)!!}</li>
                    <li><img src="images/pd-g2.jpg" alt="" /></li>
                    <li><img src="images/pd-g3.jpg" alt="" /></li>
                    <li><img src="images/pd-g4.jpg" alt="" /></li>
                  </ul>
                  <div id="bx-pager"> <a data-slide-index="0" href=""><img src="images/pdt-g1.jpg" alt="" /></a> <a data-slide-index="1" href=""><img src="images/pdt-g2.jpg" alt="" /></a> <a data-slide-index="2" href=""><img src="images/pdt-g3.jpg" alt="" /></a> <a data-slide-index="3" href=""><img src="images/pdt-g4.jpg" alt="" /></a> </div>
                </div>
              </div>
              <div class="col-md-7 col-sm-7 ">
                <div class="pro-details-sec">
                  <div class="pro-head"> <span><a href="#"> ‹  Back to Fashion </a></span> <span class="pull-right"><a href="#prev">Prev</a> / <a href="#next">Next</a> <a href="#back"><i class="fa fa-angle-left"></i></a> <a href="#next"><i class="fa fa-angle-right"></i></a></span> </div>
                  <h3>{{$product->title}}</h3>
                  <span class="stock"><i class="fa fa-check-circle"></i> Available in stock </span>
                  <ul class="hot-deal-options">
                    <li class="rating-stars"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"> </i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></li>
                    <li class="reviews"><a href="#">(5) reviews</a></li>
                  </ul>
                  <div class="pdetails">
                    <p> {{$product->description}}</p>
                    <!--<ul class="points">
                      <li> Constructed in washed cotton jersey</li>
                      <li> Fixie american apparel flexitarian pitchfork neutra</li>
                      <li> Brooklyn high life plaid cardigan</li>
                      <li> Regular fit</li>
                      <li> Authentic sartorial wes anderson scenester</li>
                    </ul>-->
                  </div>
                  <div class="quantity">
                    <div class="numeric-stepper">
                      <button type="submit" name="ns_button_1" value="1" class="plus">+</button>
                      <input type="text" name="ns_textbox" size="2" class="input-text qty text" />
                      <button type="submit" name="ns_button_2" value="-1" class="minus">-</button>
                    </div>
                    {!! Form::open(array('url'=>'store/addtocart'))!!}
                    {!! Form::hidden('quantity','1')!!}
                    {!! Form::hidden('id',$product->id)!!}
                    <button type="submit" class="addtocart">
                      <i class="fa fa-shopping-cart"></i> 
                      Add to Cart
                      </button>
                    {!!Form::close()!!} 
                  </div>
                  <div class="product_meta"> <span class="sku_wrapper"> SKU: <span class="sku">SD-091</span>.</span> <span class="posted_in">Category: <a rel="tag" href="#">Instrumental</a>.</span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tabs-details">
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#description" role="tab" data-toggle="tab">Description</a></li>
              <li><a href="#adinfo" role="tab" data-toggle="tab">Additional Information</a></li>
              <li><a href="#reviews" role="tab" data-toggle="tab">Reviews</a></li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="description"> <strong>Description</strong>
                <p>{{$product->description}}</p>
              </div>
              <div class="tab-pane" id="adinfo"> <strong>Additional Information</strong>
                <p> {{$product->description}}</p>
              </div>
              <div class="tab-pane" id="reviews"> <strong>Reviews</strong>
                <p>{{$product->description}}</p>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--  About Collection End -->
@stop