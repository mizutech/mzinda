@extends('app')

@section('content')
<div class="second-page-container">
                <div class="container">
                    <div class="row">

                        <div class="col-md-9">
                            <div class="block-breadcrumb">
                                <ul class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Pages</a></li>
                                    <li class="active">Products list</li>
                                </ul>
                            </div>

                            <div class="header-for-light">
                                <h1 class="wow fadeInRight animated" data-wow-duration="1s">List<span>of Products</span> </h1>

                            </div>
                            <div class="block-products-modes color-scheme-2">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="product-view-mode">
                                            <a href="products-grid.html"><i class="fa fa-th-large"></i></a>
                                            <a href="products-list.html"  class="active"><i class="fa fa-th-list"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-1">
                                                <label class="pull-right">Sort by</label>
                                            </div>
                                            <div class="col-md-5">
                                                <select name="sort-type" class="form-control">
                                                    <option value="position:asc">--</option>
                                                    <option value="price:asc"  selected="selected">Price: Lowest first</option>
                                                    <option value="price:desc">Price: Highest first</option>
                                                    <option value="name:asc">Product Name: A to Z</option>
                                                    <option value="name:desc">Product Name: Z to A</option>
                                                    <option value="quantity:desc">In stock</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select name="products-per-page" class="form-control">
                                                    <option value="10" selected="selected">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="100">100</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @foreach($products as $product)

                            <article class="product list">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                                        <figure class="figure-hover-overlay text-center">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                            <div class="product-sale">7% <br> off</div>
                                            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            {!! Html::image($product->image,'products',array('class' => 'img-responsive' ))!!}
                                        </figure>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">{{$product->title}}</a>
                                                 <span class="stock"><i class="fa fa-check-circle"></i>
                                                  @if($product->availability = 1)
                                                  {{'Stock is available'}}@endif</span>
                                                  <ul class="hot-deal-options">
                                                <p class="product-price">${{$product->price}}</p>

                                            </div>

                                            <div class="product-rating">
                                                <div class="stars">
                                                    <span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span>
                                                </div>
                                                <a href="" class="review">8 Reviews</a>
                                            </div>
                                            <p class="description">
                                              {{$product->description}}
                                            </p>
                                            
                                         {!! Form::open(array('url'=>'store/addtocart'))!!}
                                    {!! Form::hidden('quantity','1')!!}
                                    {!! Form::hidden('id',$product->id)!!}
                                    <div class="product-cart">
                                    <button type="submit" class="lilly-cart-btn"><i class="fa fa-shopping-cart"></i> </button></div>
                                    {!!Form::close()!!} 
                                            </div>
                                       
                                    </div>
                                </div>
                            </article>

                            @endforeach
                            
                            <div class="block-pagination">
                                <ul class="pagination">
                                    <li><a href="#">&laquo;</a></li>
                                    <li><a href="#">1</a></li>
                                    <li class="active"><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">&raquo;</a></li>
                                </ul>
                            </div>  

                        </div>
                        <aside class="col-md-3">
                            <div class="main-category-block ">
                                <div class="main-category-title">
                                    <i class="fa fa-list"></i> Category

                                </div>
                            </div>
                            <div class="widget-block">
                                <ul class="list-unstyled ul-side-category">
                                    <li><a href=""><i class="fa fa-angle-right"></i> Man / 2</a>
                                        <ul class="sub-category">
                                            <li><a href="">Dress / 2</a>
                                                <ul class="sub-category">
                                                    <li><a href="#">Dress 1</a></li>
                                                    <li><a href="#">Dress 2</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="">Shirt / 2</a>
                                                <ul class="sub-category">
                                                    <li><a href="#">Shirt 1</a></li>
                                                    <li><a href="#">Shirt 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href=""><i class="fa fa-angle-right"></i> Woman / 2</a>
                                        <ul class="sub-category">
                                            <li><a href="">Dress / 2 </a>
                                                <ul class="sub-category">
                                                    <li><a href="#">Dress 1</a></li>
                                                    <li><a href="#">Dress 2</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="">Shirt / 2</a>
                                                <ul class="sub-category">
                                                    <li><a href="#">Shirt 1</a></li>
                                                    <li><a href="#">Shirt 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-angle-right"></i> Tablet / 0</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-angle-right"></i> Laptop / 0</a>
                                    </li>

                                </ul>

                            </div>
                            <div class="product light last-sale">
                                <figure class="figure-hover-overlay">                                                                        
                                    <a href="#"  class="figure-href"></a>
                                    <div class="product-sale">Save <br> 7%</div>
                                    <div class="product-sale-time"><p class="time"></p></div>
                                    <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                                    <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                    <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                    <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                </figure>
                                <div class="product-caption">
                                    <div class="block-name">
                                        <a href="#" class="product-name">Product name</a>
                                        <p class="product-price"><span>$330</span> $320.99</p>

                                    </div>
                                    <div class="product-cart">
                                        <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                    </div>
                                </div>

                            </div>
                            <div class="widget-title">
                                <i class="fa fa-money"></i> Price range

                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" id="price-from" class="form-control" value="0">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" id="price-to" class="form-control" value="500">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="widget-title">
                                <i class="fa fa-dashboard"></i> Colors

                            </div>
                            <div class="widget-block">
                                <ul class="colors clearfix list-unstyled">
                                    <li><a href="" rel="003d71"></a></li>
                                    <li><a href="" rel="c42c39"></a></li>
                                    <li><a href="" rel="f4bc08"></a></li>
                                    <li><a href="" rel="02882c"></a></li>
                                    <li><a href="" rel="000000"></a></li>
                                    <li><a href="" rel="caccce"></a></li>
                                    <li><a href="" rel="ffffff"></a></li>
                                    <li><a href="" rel="f9e7b6"></a></li>
                                    <li><a href="" rel="ef8a07"></a></li>
                                    <li><a href="" rel="5a433f"></a></li>
                                </ul>
                            </div>
                            <div class="widget-title">
                                <i class="fa fa-thumbs-up"></i> Bestseller
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review hidden-md">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review hidden-md">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>

                        </aside>

                    </div>
                </div>  
            </div>



<section class="products grid-view">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <h2 class="main-title">Products</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-sm-3 sidebar"> 
          
          <!-- Side Bar-->
          <div class="category widget">
            <h3>Category</h3>
            <ul>
               @foreach($cat_nav as $cat)
            <li> {!! Html::link('/store/category/'.$cat->id, $cat->name) !!} </li>
              @endforeach
            </ul>
          </div>
          <div class="featured widget">
            <h3>Featured Items</h3>
            <ul>
             
              <li class="fp-item">
              <img src="images/fp3.jpg" alt="">
              <h4>Product Name</h4>
              <p>Maecenas laoreet lectus est, eget ultricies eros. </p>
              <a href="#">Details</a>
              </li>
              
        
            </ul>
          </div>
          
          
          <div class="sidebanner widget">
          <img src="images/banner.jpg" alt="">
          
          </div>
          
        </div>
        <div class="col-lg-9 col-sm-9 gallery">
          <div class="container-fluid">
            <div class="row">
             
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop