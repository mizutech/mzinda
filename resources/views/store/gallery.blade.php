@extends('app')

@section('content')
<section class="img-gallery gallery">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="main-title">Gallery</h2>
        </div>
      </div>
      <div class="row">
        <div class="gallery">
        <div class="cp-gallery clearfix">
        
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m2.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m3.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m4.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m5.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m6.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m7.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w3 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w3 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-w2 item-h3 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          <figure class="item item-h2 "> <a href="#">{!! Html::image('images/gallery/m/m1.jpg')!!}</a> </figure>
          
          
        
        </div>
      </div>
      </div>
    </div>
  </section>
@stop