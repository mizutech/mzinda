<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mzinda Wanga</title>
        @if(isset($blog->excerpt))
        <?php $desc = $blog->excerpt; ?>
        @else 
        <?php $desc = "Mzinda is a citizen engagement platform where citizens engange with elected counsellors, local city council, ESCOM and Waterboard on service delivery. It is a place where debate on topical issues is stimulated and citizens can send in service delivery reports. Mzinda is a project embedded within the MobiSAM project at Rhodes University in South Africa."; ?>
        @endif
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no">
        <meta name="description" content="{{$desc}}">
        @if(isset($blog->title)) 
        <?php $page_title = $blog->title; ?>
        @else
        <?php $page_title = 'Mzinda Wanga' ?>
        @endif

        <meta property="og:title" content="{{$page_title}}">
        <meta property="og:site_name" content="Mzinda Wanga"/>
        <meta property="og:type" content="website">
        <meta property="fb:app_id" content="">
        @if(isset($blog->image)) 
        <?php $page_image = "http://www.mzinda.com/".$blog->image; ?>
        @else
        <?php $page_image = 'http://mzinda.com/images/about/graph.png' ?>
        @endif
        <meta property="og:image:url" content="{{$page_image}}">
        <meta property="og:image" content="{{$page_image}}">
        <meta property="og:url" content="{{$this_url}}" />
        <meta property="og:description" content="{{$desc}}">
        <meta name="author" content="mHub">
        <meta property="fb:admins" content="">
        <meta name="robots" content="*">
        <meta property="keywords" content="Democracy, Citizen Engagements, Malawi, Elections, Voice, Speaking out, ">
        <link rel="icon" href="{{url('images/favicon.ico?v=1')}}" type="image/x-icon">
        <link rel="shortcut icon" href="{{url('images/favicon.ico?v=1')}}" type="image/x-icon">
        <meta name="viewport" content="initial-scale=1.0, width=device-width">
        <link rel="shortcut icon" href="{{url('images/favicon.ico?v=1')}}">
        <!-- Bootstrap core CSS -->
        <link href="{{asset('bootstrap/css/theme.css')}}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{asset('style.css')}}" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
            <!--[if lt IE 8]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js" type="text/javascript"></script>
        <![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700,900,500" rel="stylesheet"> 
        <link href='https://fonts.googleapis.com/css?family=Raleway:300,700,900,500' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.7/typicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/pushy.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/masonry.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/odometer-theme-default.css')}}">
        <!--Tthecss file for all the flat attribution-->
        <link rel="stylesheet" href="{{asset('flat_ui/css/flat-ui.css')}}" >
        <!--NEW USER TOUR JS DEPENDENCY -->
        <link rel="stylesheet" href="{{asset('assets/css/shepherd-theme-arrows.css')}}" >
        <link rel="stylesheet" href="{{asset('assets/css/introjs.css')}}" >
        <link rel="stylesheet" href="" >
        
        
        @yield('extra_css')
   
        <script>
        window.odometerOptions = {
          selector: '.odometer',
          format: '(,ddd)', // Change how digit groups are formatted, and how many digits are shown after the decimal point
          duration: 13000, // Change how long the javascript expects the CSS animation to take
          theme: 'default'
        };
        </script>
        <script type="text/javascript">
            (function(){"use strict";var c=[],f={},a,e,d,b;if(!window.jQuery){a=function(g){c.push(g)};f.ready=function(g){a(g)};e=window.jQuery=window.$=function(g){if(typeof g=="function"){a(g)}return f};window.checkJQ=function(){if(!d()){b=setTimeout(checkJQ,100)}};b=setTimeout(checkJQ,100);d=function(){if(window.jQuery!==e){clearTimeout(b);var g=c.shift();while(g){jQuery(g);g=c.shift()}b=f=a=e=d=window.checkJQ=null;return true}return false}}})();
        </script>



      </head>
        @if(Auth::user())
            <?php $space = "body-padding" ?>
        @else
            <?php $space = "none-body" ?>
        @endif

<body class="{{$space}}" data-spy="scroll" data-offset="20" data-target="#navbar" id="app">
    <!-- Nav Menu Section -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-81659966-1', 'auto');
  ga('send', 'pageview');

</script>
    <div class="wrapper">
      <!-- Pushy Menu -->
     

      <!-- Site Overlay -->
      <div class="site-overlay"></div>

       <div  class="logo-menu shepherd-active" >
                     <nav  class="navbar navbar-default navbar-fixed-top affix-top" role="navigation" data-spy="affix" data-offset-top="50">
                        <div class="">
                            <div class="">
                                <div  class="navbar-header col-md-3">
                                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                      </button>
                                    <a  href="{{url('home')}}" class="navbar-brand logo">

                                        <img  class="logo-mzinda img-responsive" src="{{url('images/your_logo.png')}}" alt="" >
                                    </a>
                                </div>
                                 

                                <div class="collapse navbar-collapse" id="navbar">
                                    <ul class="nav navbar-nav main-nav col-md-8 pull-right">
                                          @if(Auth::user())
                                             <li id="nav-dashboard" class="{{( Request::segment('2') == 'dashboard' ? 'active' : false )}}"><a href="{{url('auth/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                                             @else
                                            <li class="{{( Request::segment('1') == 'home' ? 'active' : false )}}"><a href="{{url('home')}}"><i class="fa fa-home"></i> Home</a></li>
                                            @endif

                                            <!--Shows more options if logged in and vice versa-->
                                            @if(Auth::user())
                                            <li id="citizen-reports"><a href="<?php $_SERVER['DOCUMENT_ROOT']?>/reports/reports"><i class="fa fa-comments"></i> Citizen Reports</a></li>
                                            <li id="polls" ><a class="{{( Request::segment('1') == 'polls' ? 'active' : false )}}" href="{{url('/polls')}}"><i class="fa fa-pencil-square-o"></i> Polls</a></li>
                                            
                                            <li id="councillors"><a class="{{( Request::segment('1') == 'councillors' ? 'active' : false )}}" href="{{url('/councillors')}}"><i class="fa fa-envelope"></i> Engage Councillor</a></li>

<li><a class="{{( Request::segment('2') == 'blog' ? 'active' : false )}}" href="{{url('home/blog')}}"><i class="fa fa-newspaper-o"></i>News</a></li>  
                                            @endif
                                           
                                            <!--Shows Logout if logged in and vice versa-->
                                            @if(Auth::user())
                                            <li><a href="{{url('auth/logout')}}"><i class="fa fa-user"></i>Logout</a></li>
                                            @else
                                            <li><a href="{{url('home#contact')}}"><i class="fa fa-info"></i>About Us</a></li>
                                            <li><a class="{{( Request::segment('2') == 'blog' ? 'active' : false )}}" href="{{url('home/blog')}}"><i class="fa fa-newspaper-o"></i>News</a></li>                            
                                            <li><a href="{{url('home#news')}}"><i class="fa fa-user"></i>Active Citizen</a></li>
                                            <li><a href="{{url('auth/login')}}"><i class="fa fa-user"></i>Login</a></li>
                                            @endif
                                    </ul>
                                </div>
                             </div>

                            </div>
                          </nav>
        </div>  

@if(Session::has('error_message'))
<div class="container">
    <div class="row normalize-row">
        <div class="alert alert-danger alert-dismissable">
            {{ Session::get('error_message') }} 
        </div> 
    </div>
</div> 
@endif

@yield('content')


        <footer class="">
            <div class="container">
            <div class="row">
                <div class="col-sm-12" >

                        <div class="col-md-2 col-md-offset-1 col-sm-6 col-xs-12 ">
                                <a href="http://mhubmw.com" target="_blank"> <img src="{{url('images/mhub.png')}}" class="img-responsive" /> </a>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 ">
                                <a href="http://mobisam.net" target="_blank"> <img src="{{url('images/mlogo.png')}}" class="img-responsive" /> </a>
                        </div>

                        

                        <div class="col-md-2 col-sm-6 col-xs-12">
                                   <a href="http://cfjmalawi.org" target="_blank"><img src="{{url('images/cfj-small.png')}}" style="margin:0 auto;" class="img-responsive" /> </a>
                                </div>


                        <div class="col-md-2 col-sm-6 col-xs-12">
                                <a href="http://osisa.co.za" target="_blank"> <img src="{{url('images/osisa.png')}}" class="img-responsive" /> </a>
                        </div>
                        
                         <div class="col-md-2 col-sm-6 col-xs-12">
                                <a href="http://storyworkshopmw.com" target="_blank"> <img src="{{url('images/sclogo.png')}}" class="img-responsive" /> </a>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="footer-contact">
                            <p><i class="fa fa-phone"></i>+265999201886</p>
                            <p><i class="fa fa-envelope"></i>info@mzinda.com </p>
                            <p>© 2016 Mzinda. </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="text-right social">
                        <a href="https://web.facebook.com/mzindawanga/" _target="blank"><i class="typcn typcn-social-facebook-circular"></i></a>

                        <a href="https://twitter.com/mzindawanga" _target="blank"><i class="typcn typcn-social-twitter-circular"></i></a></p>

                        @if(Auth::user())

                        <a id="startusertour" href="#" class="pull-right btn btn-info"><i class="fa fa-user"></i>View our -Getting Started- tutorial </a>
                        @endif
                    </div>
                </div>
            </div>
        </footer>
<!--Footer ends-->
     <!-- jQuery Load -->
<!--Footer begins-->
   </div>
        <script type="text/javascript">


            
        </script>

    <!-- Placed at the end of the document so the pages load faster -->
 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        @yield('chart_script')
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyCaH3mMey-ieiFRCeXNjEczI3NGT4dn_C0&sensor=false&libraries=places'></script>
        @yield('extra_scripts')
        <script src="{{asset('assets/js/script.js')}}"></script>

        
        @yield('comment_script')
        <!--NEW USER TOUR JS DEPENDENCY -->
        <script src="{{asset('assets/js/intro.js')}}"></script>

            <script type="text/javascript">
                  
                    var introguide = introJs();
                    var startbtn   = $('#startusertour');
                     

                    introguide.setOptions({
                        steps: [
                            {
                              element: '#middle',
                              intro: 'Hello! Welcome to the Mzinda Platform. This tour will help you get started',
                              position: 'bottom'
                            },
                            {
                              element: '#nav-dashboard',
                              intro: 'This is the first screen you see when you login, you can answer the latest polls and search for councillors, wards.',
                              position: 'bottom'
                            },
                            {
                              element: '#citizen-reports',
                              intro: 'Citizen reports shows you the incoming messages from citizens. Messages are sent by sms,email and webforms',
                              position: 'right'
                            },
                            {
                              element: '#polls',
                              intro: 'View new and old poll results, you can also vote on the polls found here',
                              position: 'bottom'
                            },
                            {
                              element: '#councillors',
                              intro: 'Post directly to your councillors, hear their and other peoples feedback, on current issues',
                              position: 'bottom'
                            }, 
                            {
                              element: '#middle',
                              intro: 'If you need to go through the tour again, you can click the link at bottom of the page.',
                              position: 'left'
                            },                      
                            {
                              element: '#middle',
                              intro: 'Thank you for signing up. Let`s keep the conversation going. Click done to finish the tour.',
                              position: 'bottom'
                            },
                        ]
                    });

                    

                    /**   
                    * Initiating the tour by clicking a link/button
                    * Hide button when tour ends.
                    **/          
                    
                    startbtn.on('click', function(e){
                        e.preventDefault();
                        introguide.start();
                        $(this).hide();
                    });
                    /**   
                    * oncomplete re-display the button
                    * hide by default since we don't need this functionality.
                    **/
                    introguide.oncomplete(function() {
                        if(startbtn.css('display') == 'none') {
                        startbtn.show();
                        }
                    });
                    introguide.onexit(function() {
                        if(startbtn.css('display') == 'none') {
                        startbtn.show();
                          }
                    }); 
                    
            </script>

        <?php //$new_user = true; ?>
        <!--Start the tutorial only when we are on a firt run-->
        @if(isset($new_user)&& $new_user ==true)

            <script type="text/javascript">
            introguide.start();
            </script>
        @endif


        <script src="{{asset('assets/js/js.cookie.js')}}"></script>

        <script>   
        //Setting the cookies that will keep poll map location values                                                                                                 
        $(document).ready(function(){
            //Start by deleting the old latitude cookie and setting the new latitude
            $("#us20-lat").change(function(){
                var latitude = $('#us20-lat').val();
                // alert(latitude); //for debugging
                Cookies.set('latitude', latitude);  
            });   

            //Start by deleting the old longitude cookie and setting the new latitude
            $("#us20-lon").change(function(){
                var longitude = $('#us20-lon').val();
                // alert(longitude); //for debugging
                Cookies.set('longitude', longitude);  
            });  

            $("#save-location").click(function(){
                var latitude = $('#us20-lat').val();
                var longitude = $('#us20-lon').val();
                // alert(longitude); //for debugging
                // alert(latitude); //for debugging
                Cookies.set('latitude', latitude); 
                Cookies.set('longitude', longitude); 
                alert('Location saved'); //for debugging 
            });

            $("#pick-location").click(function(){
                var picked_lat = $('#picked_lat').val();
                var picked_lon = $('#picked_lon').val();
                 $('#us20-lat').val(picked_lat);
                 $('#us20-lon').val(picked_lon);
                alert(picked_lon); //for debugging 
            });                                                                                                                                                                                                                                 
        });                                                                                                                                                                                                                                      
        </script>

        
         <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('https://netdna.bootstrapcdn.com/twitter-bootstrap/2.0.4/js/bootstrap-scrollspy.js')}}"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="{{asset('assets/js/ie10-viewport-bug-workaround.js')}}"></script>
       <!--  <script src="{{asset('assets/js/masonry.js')}}"></script>-->
        <script src="{{asset('assets/js/pushy.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        
        <!--<script src="{{asset('assets/js/locationpicker.jquery.js')}}"></script> -->    
        <script src="{{asset('assets/js/scripts.js')}}"></script>

    @yield('vue-section')
    
    @yield('social')
    
    </body>
    </html>
    