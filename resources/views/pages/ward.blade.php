@extends('app')

@section('content')
<div class="content-base">
              <div class="container">
                  <div class="row no-padding">
@if(Session::has('ward_notitfication'))
<div class="container">
    <div class="row normalize-row">
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('ward_notitfication') }} 
        </div> 
    </div>
</div> 
@endif
                      <div class="8u">
                            
                             <!-- Content -->
                      <article class="col-md-12 engagement-box">
                      <div class="col-md-5">
                         <a href="#" class="image featured"><img class="blog-img" src="{{url($ward->image)}}" alt="" /></a>
                          
                      </div>

                    <div class="col-md-6">
                          <ul>
                            <h4 class="ward-name">Name:<span>{{$ward->firstname.' '.$ward->lastname}}</span></h4>
                            <h4 class="ward-name">Ward:<span>{{ $ward->ward->name }}</span></h4>
                            <h4 class="ward-name">Bio:<span>{{$ward->bio}}</span></h4>
                          </ul>
                      
                     </div>

                </article>
              </div>
            </div>
        </div> 


  <div class="container engagement-comments">


    <div class="row no-padding">
          <!-- Comments -->
              <div id="message-section" class="comments-area space-top-3x">

                <h2 class="ward-name">Post Message Here</h2>
                      <form role="form" name="contactform" id="contactform" method="post" action="{{url('ward-message')}}">

                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                      <input type="hidden" value="{{$ward->id}}" name="ward_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                      
                                          <!-- Field 4 -->
                       <div class="textarea-message form-group">
                          <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                       </div>

                        <div class="textarea-message form-group">
                          <input type="submit" class="btn btn-lg btn-orange" value="Send" />
                       </div>
                       </form>
              </div>
    </div>
     <div class="row no-padding"> 
    @if(Session::has('ward_notitfication'))

        <div class="row ">
            <div class="alert alert-success alert-dismissable">
                {{ Session::get('ward_notitfication') }} 
            </div> 

    @endif
          <div id="message-section" class="comments-area space-top-3x">
                <h3 class="ward-name comments-count">Previous Engagements<span></span></h3>

            @foreach ($engagements as $engagement)
                <!-- Parent Comment -->
                <div class="comment">
                  <div class="inner">
                    <div class="comment-body">
                      <p>{{$engagement->message}}</p>
                      <div class="comment-meta">
                        <div class="column">
                          <span class="comment-autor">{{$engagement->user->firstname.' '.$engagement->user->lastname}}</span>
                          <span class="comment-date">{{ date('F d, Y', strtotime($engagement->created_at)) }}</span>
                        </div>
                        <div class="column comment-reply">
                          <a href="#response2-{{$engagement->user->firstname}}" data-index="<?= $engagement->id; ?>" class="comment_header comment-reply-link" rel="nofollow">Reply</a>

                          <?php //If the user owns this comment, he should be able to delete it ?>
                           @if(isset($user_id))
                              @if($engagement->user_id==$user_id)
                              <a href="{{url('/delete-engagement', $engagement->id)}}" class="comment-reply-link" rel="nofollow">Delete</a>
                              @endif
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>

                    <!--Text reply box appears when you click reply-->
                    <div id="<?= $engagement->id; ?>" class="comment_box col-md-6" >
                        <form  role="form" name="contactform" id="message response2-{{$engagement->user->firstname}} user-box-{{$engagement->user->firstname}}" method="post" action="{{url('message-response')}}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                    <input type="hidden" value="{{$engagement->id}}" name="message_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                                    
                                                        <!-- Field 4 -->
                                     <div class="textarea-message form-group">
                                        <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                                     </div>

                                      <div class="textarea-message form-group">
                                        <input type="submit" value="Send" />
                                     </div>
                        </form>
                    </div>
               <?php   $message_responses = array_filter($engagement->message_responses->toArray()) ?>
              @if(!empty($message_responses))
                @foreach ($engagement->message_responses as $response)
                 <!-- Comment Reply -->
                  <div class="comment depth-2">
                    <div class="inner">
                      <div class="comment-body">
                        <p>{{$response->message}}</p>
                        <div class="comment-meta">
                          <div class="column">
                            <span class="comment-autor">{{$response->user->firstname.' '.$response->user->lastname}}</span>
                            <span class="comment-date">{{ date('F d, Y', strtotime($response->created_at)) }}</span>
                          </div>
                          <div class="column comment-reply">
                            <a href="#response2-{{$response->user->firstname}}"data-index="<?= $response->id; ?>" class="response_comment_header comment-reply-links" rel="nofollow" >Reply</a>
                            @if(isset($user_id))
                              @if($response->user_id==$user_id)
                              <a href="{{url('/delete-response', $response->id)}}" class="comment-reply-link" rel="nofollow">Delete</a>
                              @endif
                          @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- .comment.depth-2 -->

                   <!--Text reply box appears when you click reply-->
                    <div id="<?= $response->id; ?>" class="response_comment_box col-md-6" >
                        <form  role="form" name="contactform" id="message response2-{{$engagement->user->firstname}} user-box-{{$engagement->user->firstname}}" method="post" action="{{url('message-response')}}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                    <input type="hidden" value="{{$engagement->id}}" name="message_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                                    
                                                        <!-- Field 4 -->
                                     <div class="textarea-message form-group">
                                        <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                                     </div>

                                      <div class="textarea-message form-group">
                                        <input type="submit" value="Send" />
                                     </div>
                        </form>
                    </div>

                
                  @endforeach


              @endif

                  
               </div><!-- .comment -->
            @endforeach
              
                
              

        </div><!-- .comments-area -->
    </div>
  </div><!--Container-->

</div>
@stop

@section('comment_script')
<script>
$(document).ready(function(e) {
  $('.comment_box').hide();
  $('.comment_header').on('click', function(e) {
      $('#' + $(this).data('index')).toggle();
  });

  //for responses to  a Comment
  $('.response_comment_box').hide();
  $('.response_comment_header').on('click', function(e) {
      $('#' + $(this).data('index')).toggle();
  });

});


</script>

@stop




@section('vue-section')
<script src="{{asset('assets/js/vue.js')}}"></script>
    <script type="text/javascript">
        new Vue({

        el: '',

        data:{


         textbox1 : '',
         textbox2 : '',
        
        },

        methods:{

            showTextBox1: function(){
                this.textbox1 = 'activated';
            },

            showTextBox2: function(){
                this.textbox2 = 'activated';
            }
        }

    });  

</script>
@stop