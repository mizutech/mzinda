@extends('app')

@section('content')
<div class="container">
    <div class="row">
	@foreach($old_polls as $poll)
	<div class="col-md-6">    
	<div class="tile">
		 <a href="{{url('/category',$poll->category->id)}}" class="category"><i class="cat-icons {{$poll->category->icon}}"></i>&nbsp{{$poll->category->name}}</a>
	     <p>{{$poll->title}}</p>
		<div class="row">
		                                                                                    
		    <div class="col-md-6">    
		            <a class="btn btn-info btn-large " href="{{url('viewresults',$poll->id)}}">Check out the Results</a>
		    </div>


		</div>
	</div>
	</div>
@endforeach
	</div>
</div>
@stop

