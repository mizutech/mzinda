@extends('app')
@section('extra_css')
<!-- Select2 -->
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/select2.min.css')}}">
@stop

@section('content')
<!-- Hero Area Section -->
<div class="content-base">
<header id="home null-bg-home">
    <div class="container-fluid-2">
                <!-- change the image in style.css to the class header .container-fluid [approximately row 50] -->
               


            <div class="container">
                        <div class="row">
      
                                @if($answer == true)
                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                
                                 
                                        <div class="tile">
                                            
                                            <span class="poll-title ">Thank you for voting!</span>

                                             <div class="form-group form-full">
                                                See what other people have said below  
                                            </div>
                                            <div class="form-bottom ">
                                                                    

                                            <a href="{{url('/polls')}}">See More</a>
                                        </div>

                                            
                                  
                                </div>


                                @else
                                <div class="col-md-6  col-sm-6 col-xs-12 ">
                                    <div class="col-xs-12">
                                        @if(!empty($polls))               
                                                                        @foreach($polls as $poll)
                                                                        
                                                                       
                                                                    
                                                                            <div class="tile">
                                                                                
                                                                                <a href="{{url('/category',$poll->category->id)}}" class="category"><i class="cat-icons {{$poll->category->icon}}"></i>&nbsp{{$poll->category->name}}</a>
                                                                                <p>{{$poll->title}}</p>
                                                                                                   

                                                                                    {!! Form::open(['url'=>'poll-vote','files'=> true, 'class'=>'poll-form']) !!}

                                                                                <div class="row">
                                                                                    
                                                                                    <div class="col-md-12 form-full">
                                                                                        @foreach($poll->polloptions as $pollOption)
                                                                                                    {!! Form::hidden('poll_id',$poll->id) !!}
                                                                                                    {!! Form::radio('poll_option', $pollOption->id, 
                                                                                                    ['id'=> 'optionsRadios1',  'data-toggle'=>'radio']) !!} <span class="poll-option">{{$pollOption->title}}</span></br>
                                                                                         @endforeach
                                                                                      
                                                                                    </div>

                                                                                    
                                                                                </div>

                                                                                <div class="row">
                                                                                    
                                                                                    <div class="col-md-6">    
                                                                                            <button class="btn  btn-orange btn-large " >Answer</button>
                                                                                            <a class="btn btn-info btn-large " href="{{url('viewresults',$poll->id)}}">Results</a>
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                                            <div class="textarea-message form-group">
                                                                                                             <?php
                                                                                                             /**Setting the unique id for the map which will dynamically generated, the id is created in the js function below and passed into the map jquery function**/
                                                                                                             $phpvalue = "lat".$poll->id
                                                                                                              ?>  

                                                                                                            

                                                                                


                                                                                                                
                                                                                            </div>
                                                                                            
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                         

                                                                        {!! Form::close() !!}
                                                                    
                                                                            @endforeach
                                    </div>
                                                                            @else
                                                                                <div class="tile">
                                                                                    <h6>No Polls available for this category</h6>
                                                                                </div>

                                                @endif
                                          @endif                                  
                                        <!-- Hero Area Section End-->
                                </div>
                                 <div class="col-md-6 col-xs-12 ">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
                                                        <div class="search-section">

                                                        

                                                              <div class="row" data-example>
                                                                <h3 class="map-instruction">Type your District or click on map to auto-locate or pick your location</h3>
                                         <div class="">
                                             <div class="form-group">
                                               <input class="placepicker form-control" name="location_name" data-map-container-id="collapseOne" data-latitude-input="#latitude"
                                                data-longitude-input="#longitude" placeholder="Enter your location"/>

                                               <input type="hidden" id="latitude" name="latitude" value="-11.4216438"/>
                                               <input type="hidden" id="longitude" name="longitude" value="33.9953792"/>
                                             </div>

                                                                     <div id="collapseOne" class="collapse">
                                                                       <div class="placepicker-map thumbnail"></div>
                                                                     </div>
                                                                     <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                                                 </div>
                                                               </div>
                                                                                   
                                                              
                                                             {!! Form::close() !!}  
                                                                   
                                                                
                                            
                                                            </div>
                                                            
                                                        </div> 
                                                       
                                                            
                                  </div>

                                  <div class="col-md-6 col-xs-12 pull-right">
                                    <hr>
                                      <div class="" style="display:block !important;">
                                        
                                      <div class="col-md-6">
                                        <a href="{{url('/poll-archive')}}" class=""> 
                                        <img src="{{url('images/icons/folder-blue.png')}}" width="140" class="img-responsive" style=""> 
                                        <h3 class="folder-name" ><span>Poll Archives</span></h3>
                                        </a>
                                      </div>

                                      <div class="col-md-6">
                                        <a href="{{url('/poll-archive')}}" class=""> 
                                        <img src="{{url('images/icons/folder-blue.png')}}" width="140" class="img-responsive" style=""> 
                                        <h3 class="folder-name" ><span>Most Voted</span></h3>
                                        </a>
                                      </div>


                                    
                                        
                                      </div>

                                  </div>
                </div>
                                                            
            </div> 
                                                      
    </div>
</header>
</div>


<!-- Service Section -->




@stop
@section('extra_scripts')
<script src="{{asset_timed('assets/js/jquery.placepicker.js')}}"></script>
<script>

      $(document).ready(function() {

        // Basic usage
        $(".placepicker").placepicker();

        // Advanced usage
        $("#advanced-placepicker").each(function() {
          var target = this;
          var $collapse = $(this).parents('.form-group').next('.collapse');
          var $map = $collapse.find('.another-map-class');

          var placepicker = $(this).placepicker({
            map: $map.get(0),
            placeChanged: function(place) {
              console.log("place changed: ", place.formatted_address, this.getLocation());
            }
          }).data('placepicker');
        });

      }); // END document.ready

</script>
<script src="{{asset('admin/plugins/select2/select2.full.min.js')}}"></script>
<script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        });
</script>
@stop
