@extends('app')

@section('content')
<div class="content-base">
<header id="home">
            <div class="container-fluid-2">
                <!-- change the image in style.css to the class header .container-fluid [approximately row 50] -->
               


                    <div class="container">
                        <div class="row">
                <?php 
                
                foreach($councillors as $councillor):?>

                
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
                        <div class="councillor">
                             <div class="product-img">
                                        <img height="200px" src="{{ url($councillor->image) }}" class="img-responsive" alt="" width="100%">
                                        
                                    </div>
                            <div class="councillor-tile">
                                    <div class="product-details">
                                        <h4 class="truncate" > <a href="{{ url('view-councillor',$councillor->id) }}">{{$councillor->firstname.' '.$councillor->lastname}} </a>
                                       </h4>
                                       
                                        <h5>{{ $councillor->ward->name }}</h5>                               
                                      
                                    </div>

                                    <div class="product-overlay">
                                            <div class="add-to-cart">
                                                <a href="{{ url('view-councillor',$councillor->id) }}" class="btn btn-orange">
                                                <i class="fa fa-arrow-right"></i> Engage</a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                 <?php endforeach ?> 
            </div>
</div>
</div>
</header>
</div>
@stop