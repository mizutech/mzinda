@extends('app')

@section('content')



<!-- Hero Area Section -->
<header id="home">
            <div class="container-fluid">
                <!-- change the image in style.css to the class header .container-fluid [approximately row 50] -->
                <div class="space-bottom">
                   


                    <div class="container">
                        <div class="row">

                        @if (count($errors) > 0)

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                         @endif

                            <div class="col-md-8 col-xs-12">
 

                                <div class="col-md-12 mzinda-learn padding-130">
                                    <div class="intro">

                                    <div id="main" class="col-md-12">
                                  
                                            <div class="">
                                                <div class="col-md-4">
                                                <a href="{{url('/auth/login')}}">
                                                <img src="{{url('images/services/go1.png')}}" class="img-responsive" alt="">
                                                </a>
                                                <h3 class="small-title">Start</h3>
                                                <p class="contribute-content">Get started by <a href="{{url('auth/login')}}">signing up</a> on this site</p>
                                                </div>
                                            </div>
                                  
                                            <div class="col-md-4">
                                                <div class="service-item">
                                                <a href="{{url('/auth/login')}}">
                                                <img src="{{url('images/services/go2.png')}}" class="img-responsive" alt="">
                                                </a>
                                                <h3 class="small-title">Have your say</h3>
                                                <p class="contribute-content">Let your views and thoughts on important issues be expressed.</p>
                                                </div>
                                            </div>
                                      
                                            <div class="col-md-4">
                                                <div class="service-item">
                                                <a href="{{url('/auth/login')}}">
                                                <img src="{{url('images/services/go3.png')}}" class="img-responsive" alt="">
                                                </a>
                                                <h3 class="small-title">Influence</h3>
                                                <p class="contribute-content">The results determine the effectiveness of service delivery</p>
                                                </div>
                                            </div>
                                    
                                    </div>
                                        
    
                                    </div>

                              </div>
                            
                            </div>

                            <div class="col-md-4 col-xs-12 ">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  mzinda-login padding-130 pull-right">

                                <form  role="form" method="POST" action="{{ url('/auth/login-home') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <div class="login-form">
                                            <div class="form-group">
                                              <input type="text" class="form-control login-field" name="email" value="" placeholder="Enter your name" id="login-name" />
                                              <label class="login-field-icon fui-user" for="login-name"></label>
                                            </div>

                                            <div class="form-group">
                                              <input type="password" class="form-control login-field" name="password" value="" placeholder="Password" id="login-pass" />
                                              <label class="login-field-icon fui-lock" for="login-pass"></label>
                                            </div>

                                            <button class="btn btn-primary btn-lg btn-block btn-mzinda" href="#">Log in</button>
                                            <a class="login-link" href="{{('password/email')}}">Lost your password?</a>
                                          </div>
                                          <div class="signup-section">
                                                <a class="btn btn-primary btn-lg btn-block btn-mzinda" href="{{('auth/register')}}">Sign Up</a>
                                          </div>
                                </form>
                              </div>
                            </div>
                        </div>
                    
                    </div>

                    <div class="clear clear-the-air"></div>

                </div>

            </div>
        </header>

<?php $count=0; ?>

@foreach($home_contents as $home_content)




        
               


@if($home_content->keyword == 'about-mzinda')
        <section id="contact" class="prefooter wow fadeInUp" data-wow-delay="300ms">
                    <!-- change the image in style.css to the class .prefooter .container-fluid [approximately row 154] -->
                    <div class="container-fluid">
                        <div class="container">
                            <div class="row">                     
                                <div class="col-md-8 col-sm-12 text-center">  
                                    <h1 class="title">{!!$home_content->heading!!}</h1>
                                        <h2 class="subtitle">{!!$home_content->sub_heading!!}</h2>                      

                                    <p>{!!$home_content->content!!}</p>
                                    </div>
                                    <div class="col-md-4 col-md-4 col-sm-12 col-xs-12">
                                    <img class="img-responsive" src="images/about/graph.png" alt="">
                                    </div>
                            </div>
                        </div>
                    </div>
         </section>
@endif

@if($home_content->keyword == 'call-to-action')
        <section id="about" class="number wow fadeInUp text-center" data-wow-delay="300ms">
            <!-- change the image in style.css to the class .number .container-fluid [approximately row 102] -->
            <div class="container-fluid">
                <div class="container">
                    <div class="row">

                    <div class="col-md-12" style="padding:40px">
                        <h2 class="call-title">{!!$home_content->heading!!}</h2>
                        @if($home_content->keyword == 'call-to-action' && $home_content->button_active = 1)
                        <a class="btn btn-orange btn-danger btn-lg" href="{{url('auth/register')}}">{{$home_content->button_text}} <i class="fa fa-arrow-circle-o-right"></i> </a>
                        @endif
                    </div>

        
                    </div>
                </div>
            </div>
        </section>
@endif

@if($home_content->keyword == 'sms-reports')
        <section id="news" class="blog wow fadeInUp" data-wow-delay="300ms">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>{!!$home_content->heading!!}</h2>
                        <p>{!!$home_content->content!!}</p>
                        @if($home_content->button_active == 1)
                        <a class="btn btn-orange btn-danger btn-lg" href="<?php $_SERVER['DOCUMENT_ROOT']?>/reports/reports">{{$home_content->button_text}} <i class="fa fa-arrow-circle-o-right"></i> </a>
                        @endif
                    </div>
                    <div class="col-md-5 text-center">
                        
                             <div><img src="{{$home_content->image}}" alt="Mail" width="50%"></div>
                        
                    </div>
                </div>
            </div>
        </section>
@endif 


        
@if($home_content->keyword == 'engagements')
<section id="engagements" class="col-md-6 engagement-statistics" data-wow-delay="300ms">
            <!-- change the image in style.css to the class .number .container-fluid [approximately row 102] -->
            <div class="">
                <div class="">
                    <div class="row">
                    <div class="text-center" style="padding:40px">
                        <h2 class="call-title">{!!$home_content->heading!!}</h2>
                        <h3 class="engagements-figure">{!!$home_content->sub_heading!!}</h3>
                    </div>
                    </div>
               </div>
        </div>
</section>
@endif
@if($home_content->keyword == 'fixed')
<section id="fixed" class="col-md-6 fixed-statistics" data-wow-delay="300ms">
            <!-- change the image in style.css to the class .number .container-fluid [approximately row 102] -->
            <div class="">
                <div class="">
                    <div class="row">
                    <div class="text-center" style="padding:40px">
                        <h2 class="call-title">{!!$home_content->heading!!}</h2>
                        <h3 class="engagements-fixed">{!!$home_content->sub_heading!!}</h3>
                    </div>
                    </div>
               </div>
        </div>
</section>
@endif       
@endforeach


@stop