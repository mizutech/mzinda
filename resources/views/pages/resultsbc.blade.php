@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="poll-box">
			<span class="poll-title ">Poll Title:{{$poll->title}}</span>
			@foreach($pollResults as $pollResult)
			{{$pollResult->user->firstname.' '.$pollResult->user->lastname }} said {{$pollResult->poll_option->title}} </br>

			@endforeach

			</br>{{$answer_count}} people answered</br>

			@foreach($results as $result)	

				{{count($result->poll_votes->toArray())}} said {{$result->title}} </br>

			@endforeach
		</div>

	</div>

	
</div>
@stop