@extends('app')
@section('extra_css')

@stop
@section('content')
<div class="container">
	<div class="row">
		<div class="tile  tile-inline">
			<span class="poll-title ">Poll results for:</br>{{$poll->title}}</span>



			</br><strong>{{$answer_count}} people answered</strong></br>
			<?php $count = 0; ?>
			@foreach($results as $result)	
				<?php $count++; ?>
				<strong>{{count($result->poll_votes->toArray())}}  voter(s) said {{$result->title}}</strong> </br>
				<?php
				 
				  
				  	switch ($count) {
				  		case 1:
				  			$result_count1 = count($result->poll_votes->toArray());
				  			$result_title1 = $result->title;
				  			break;
				  		case 2:
				  			$result_count2 = count($result->poll_votes->toArray());
				  			$result_title2 = $result->title;
				  			break;
				  		case 3:
				  			$result_count3= count($result->poll_votes->toArray());
				  			$result_title3 = $result->title;
				  			break;
				  		case 4:
				  			$result_count4= count($result->poll_votes->toArray());
				  			$result_title4 = $result->title;
				  			break;
				  		default:
				  			$result_count1 = count($result->poll_votes->toArray());
				  			break;
				  	}
				  	
				
				?>
			@endforeach
		</div>
		<div class="tile tile-inline">
			<!-- DONUT CHART -->
	             <div class="col-md-12 pie-box">
	                <div class="box-header">
	                  <h3 class="box-title text-danger" style="color:#fff !important;">Results Chart</h3>
	                </div><!-- /.box-header -->
  	                <div class="box-body text-center">
  	                   <div id="donut-chart" style="width:450px; height: 300px;"></div>
  	                   
  	                </div><!-- /.box-body -->
	              </div><!-- /.box -->
                   <?php 
                   /**Chart Values
                   *
                   *Setting the values to variables that will be used for the chart
                   */
                     //First set some defaults in case the value is null
                     $chartvalue1=0;$chartvalue2=0;$chartvalue3=0;$chartvalue4=0;
                     if(isset($result_count1)){ $chartvalue1= $result_count1; }; 
                     if(isset($result_count2)){ $chartvalue2=  $result_count2; }; 
                     if(isset($result_count3)){ $chartvalue3 = $result_count3; };  
                     if(isset($result_count4)){ $chartvalue4= $result_count4; }; 
                     ?>
                     <?php
                     /**Chart Titles
                     *
                     *Setting the title to variables that will be used for the chart
                     */
                     //First set some defaults in case the value is null
                     $chart_title1= "No Value";$chart_title2= "No Value";$chart_title3="N/A";$chart_title4= "N/A";
                     ?>

		                  @if(isset($result_title1))
		                  <?php $chart_title1= $result_title1; ?>
		                  @endif

		                  @if(isset($result_title2))
		                  <?php $chart_title2= $result_title2; ?>
		                  @endif

		                  @if(isset($result_title3))
		                  <?php $chart_title3= $result_title3; ?>
		                  @endif

		                  @if(isset($result_title4))
		                  <?php $chart_title4= $result_title4; ?>
		                  @endif


         	</div>

  </div>
</div>
@stop

@section('chart_script')

<!-- FLOT CHARTS -->
    <script src="{{asset('admin/plugins/flot/jquery.flot.js')}}"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="{{asset('admin/plugins/flot/jquery.flot.resize.min.js')}}"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="{{asset('admin/plugins/flot/jquery.flot.pie.min.js')}}"></script>
    <!-- Page script -->
    <script>
       $(function () {
        
        /* END BAR CHART */

        /*
         * DONUT CHART
         * -----------
         */

        var donutData = [

          {label: "{{$chart_title1}}", data: <?php echo $chartvalue1; ?>, color: "#36C"},
          {label: "{{$chart_title2}}", data: <?php echo $chartvalue2; ?>, color: "#DC3912"},
          {label: "{{$chart_title3}}", data: <?php echo $chartvalue3; ?>, color: "#FF9900"},
          {label: "{{$chart_title4}}", data: <?php echo $chartvalue4; ?>, color: "#109618"}
        ];
        $.plot($("#donut-chart"), donutData, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: true
          }
        });
        /*
         * END DONUT CHART
         */

      });


      /*
       * Custom Label formatter
       * ----------------------
       */
      function labelFormatter(label, series) {
        return '<div style="font-size:15px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + "<br>"
                + Math.round(series.percent) + "%</div>";
      }
    </script>
  
@stop