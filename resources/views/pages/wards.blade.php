@extends('app')

@section('content')
<div class="container">
                <div class="row">
                @foreach($wards as $ward)
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="councillor">
                            <div class="product-img">
                                <img src="{{ url($ward->image) }}" alt="" height="276" width="265">
                                
                            </div>
                            <div class="product-details">
                                <h4> <a href="{{ url('view-ward',$ward->id) }}">{{$ward->district->name}} {{$ward->name}} </a></h4>
                                <h6> {{$ward->population}} People</h6>
                                
                                @if(isset($ward->councillor->firstname) && isset($ward->councillor->lastname) && !empty($ward->councillor->firstname)&&!empty($ward->councillor->lastname))
                                <h6> <a href="{{ url('view-councillor',$ward->id) }}">{{ $ward->councillor->firstname.' '.$ward->councillor->lastname }}</a></h6>
                                @else
                                <h6> Ward Councillor not available</h6>
                               @endif
                              
                            </div>

                            @if(isset($ward->councillor->firstname) && isset($ward->councillor->lastname) && !empty($ward->councillor->firstname)&&!empty($ward->councillor->lastname))
                            <div class="product-overlay">
                                    <div class="add-to-cart">
                                        <a href="{{ url('view-councillor',$ward->councillor->id) }}" class="button">
                                        <i class="fa fa-user"></i> Engage Ward Councillor</a>
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
                @endforeach
                </div>
</div>
@stop