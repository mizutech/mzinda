@extends('app')


@section('extra_css')

@stop

@section('content')

<div class="content-base" style="margin-top:100px"> 


				
						<div class="blog-posts">
							
							@foreach($blogs as $blog)
							<!-- Content -->
								<article class="post">
									<div class="flex-post">
										<div class="col-md-4">

											<div class="post-image">
													<a href="{{url('home/article/'.$blog->id)}}" class="image featured">
													<img class="blog-img" src="{{url($blog->image)}}" alt="" /></a>
											</div>
											

										</div>
											
										<div class="col-md-8">
											<div class="post-summary">
												<h2>{{$blog->title}}</h2>
												  <?php $day = date_parse($blog->published_at); 
		                                                  //Get the month
		                                                    $monthNum=$day['month'];
		                                                    //Change the month from a numerical value to actual month name
		                                                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
		                                                    $monthName = $dateObj->format('F'); 
		                            		?>
											<p> 
												<span class="day">{{ date('d', strtotime($blog->published_at)) }}</span>
						                        <span class="month">{{ date('F', strtotime($blog->published_at)) }}</span>
						                        <span class="day">{{ date('Y', strtotime($blog->published_at)) }}</span>
						                    </p>
											
											<p>
												{{$blog->excerpt}}
											</p>
											<a href="{{url('home/article/'.$blog->id)}}" class="btn btn-info alt">Read Post</a>
											<div class="social">
							                    <ul class="link">
							                      <li class="fb">
								                      <a href="http://www.facebook.com/sharer/sharer.php?u={{url('home/article/'.$blog->id)}}&title={{$blog->title}}" rel="nofollow" target="_blank" style="text-decoration:none;">
								                      	<i class="fa fa-facebook"></i>  
								                      </a> 
							                      </li>
							                      <li class="tw">
								                      <a href="http://twitter.com/intent/tweet?status=Interesting+article+titled+{{$blog->title}}+on+{{url('home/article/'.$blog->id)}}" rel="nofollow" target="_blank" style="text-decoration:none;">
								                      <i class="fa fa-twitter"></i>  
								                      </a> 
							                      </li>
							                      <li class="pintrest">
								                      <a href="http://pinterest.com/pin/create/bookmarklet/?media={{url($blog->image)}}&url={{url('home/article/'.$blog->id)}}&is_video=false&description={{$blog->title}}" rel="nofollow" target="_blank" style="text-decoration:none;"> 
								                      	<i class="fa fa-pinterest"></i> 
								                      </a> 
							                      </li>
							                      <li class="googleplus"><a href="https://plus.google.com/share?url={{url('home/article/'.$blog->id)}}" rel="nofollow" target="_blank" style="text-decoration:none;"> <i class="fa fa-google-plus"></i> </a> </li>
							                    </ul>
							                  </div>
											</div>

										</div>
									</div>
									
								</article>

								<div class="divider"></div>
							@endforeach

						</div>

</div>
@stop