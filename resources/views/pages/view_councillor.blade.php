@extends('app')

@section('content')
<div class="content-base">
              <div class="container">
                  <div class="row no-padding">
@if(Session::has('councillor_notitfication'))
<div class="container">
    <div class="row normalize-row">
        <div class="alert alert-success alert-dismissable">
            {{ Session::get('councillor_notitfication') }} 
        </div> 
    </div>
</div> 
@endif
      
        <div class="col-md-12">
              <!-- Widget: user widget style 1 -->
              <div class="custom-box custom-box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                <img class="custom-box-image" src="{{url($councillor->image)}}" alt="{{$councillor->firstname}}">
                  <h3 class="widget-user-username">{{$councillor->firstname.' '.$councillor->lastname}}</h3>
                  <span class="description-text">{{ $councillor->ward->name }}</span>

                                    <h1 >68% PERFORMANCE RATING</h1>


                </div>
                <div class="widget-user-image">
                  
                </div>
                <div class="custom-box-footer">
                  <div class="row">
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{ $councillor->ward->name }}</h5>
                        <span class="description-text">WARD</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{ $councillor->ward->population }}</h5>
                        <span class="description-text">WARD POPULACE</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4">
                      <div class="description-block">
                        <h5 class="description-header">1</h5>
                        <span class="description-text">TERM #</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div>
              </div><!-- /.widget-user -->
        </div><!-- /.col -->
        <div class="col-md-12">
              <!-- Box Comment -->
              <div class="custom-box custom-box-widget">

                <div class='custom-box-body'>
                  <!-- post text -->
                  <p>{{$councillor->firstname.' '.$councillor->lastname}}&nbsp{{ $councillor->bio }}</p>

                  <!-- Attachment -->
                  <div class="attachment-block clearfix">
                    <div class="attachment-pushed">
                      <h4 class="attachment-heading"><a href="http://www.mzinda.com/">Councillor Messages</a></h4>
                      <div class="attachment-text">
                        If you have any messages for this councillor, please post or follow the conversation below
                      </div><!-- /.attachment-text -->
                    </div><!-- /.attachment-pushed -->
                  </div><!-- /.attachment-block -->
                  <div class="custom-box-footer">
                @if(Session::has('councillor_notitfication'))
                        <div class="alert alert-success alert-dismissable">
                            {{ Session::get('councillor_notitfication') }} 
                        </div> 

                @endif
                  <form method="post" action="{{url('councillor-message')}}">

                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                  <input type="hidden" value="{{$councillor->id}}" name="councillor_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                    </div>
                    <input type="submit" class="btn btn-lg btn-orange" value="Send" />
                  </form>
                </div><!-- /.custom-box-footer -->
         

                </div><!-- /.custom-box-body -->
                <div class='custom-box-footer custom-box-comments'>
                @foreach ($engagements as $engagement)
                  <div class='custom-box-comment'>
                    <!-- User image -->
                    <img class='img-circle img-sm' src="{{url('images/home_contents/graph.png')}}" alt='user image' width="50">
                    <div class='comment-text'>
                      <span class="username">
                        {{$engagement->user->firstname.' '.$engagement->user->lastname}}
                        <span class='text-muted pull-right'>{{ date('F d, Y', strtotime($engagement->created_at)) }}</span>
                      </span><!-- /.username -->
                      <p>{{$engagement->message}}</p>
                      <div class="column comment-reply">
                          <a href="#response2-{{$engagement->user->firstname}}" data-index="<?= $engagement->id; ?>" class="comment_header comment-reply-link" rel="nofollow">Reply</a>

                          <?php //If the user owns this comment, he should be able to delete it ?>
                           @if(isset($user_id))
                              @if($engagement->user_id==$user_id)
                              <a href="{{url('/delete-engagement', $engagement->id)}}" class="comment-reply-link" rel="nofollow">Delete</a>
                              @endif
                          @endif
                        </div>
                    </div><!-- /.comment-text -->
                    <!--Text reply box appears when you click reply-->
                    <div id="<?= $engagement->id; ?>" class="comment_box col-md-6" >
                        <form  role="form" name="contactform" id="message response2-{{$engagement->user->firstname}} user-box-{{$engagement->user->firstname}}" method="post" action="{{url('message-response')}}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                    <input type="hidden" value="{{$engagement->id}}" name="message_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                                    
                                                        <!-- Field 4 -->
                                     <div class="textarea-message form-group">
                                        <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                                     </div>

                                      <div class="textarea-message form-group">
                                        <input type="submit" value="Send" />
                                     </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <!--REPLIES TO THE ABOVE COMMENT ARE DISPLAYED BELOW -->
                    <?php   $message_responses = array_filter($engagement->message_responses->toArray()) ?>
              @if(!empty($message_responses))
                @foreach ($engagement->message_responses as $response)
                 <!-- Comment Reply -->
                  <div class="comment depth-2">
                    <div class="inner">
                    <hr>
                    <div class="comment-meta">
                          <div class="column">
                            <span class="comment-autor">{{$response->user->firstname.' '.$response->user->lastname}}</span>
                            <span class='text-muted pull-right'>{{ date('F d, Y', strtotime($response->created_at)) }}</span>
                          </div>
                          <div class="column comment-reply">
                            <a href="#response2-{{$response->user->firstname}}"data-index="<?= $response->id; ?>" class="response_comment_header comment-reply-links" rel="nofollow" >Reply</a>
                            @if(isset($user_id))
                              @if($response->user_id==$user_id)
                              <a href="{{url('/delete-response', $response->id)}}" class="comment-reply-link" rel="nofollow">Delete</a>
                              @endif
                          @endif
                          </div>
                        </div>
                      <div class="comment-body">
                        <p>{{$response->message}}</p>
                        
                      </div>
                    </div>
                  </div><!-- .comment.depth-2 -->

                   <!--Text reply box appears when you click reply-->
                    <div id="<?= $response->id; ?>" class="response_comment_box col-md-6" >
                        <form  role="form" name="contactform" id="message response2-{{$engagement->user->firstname}} user-box-{{$engagement->user->firstname}}" method="post" action="{{url('message-response')}}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                    <input type="hidden" value="{{$engagement->id}}" name="message_id" class="input-name form-control  form-issues" placeholder="Full Name" />
                                    
                                                        <!-- Field 4 -->
                                     <div class="textarea-message form-group">
                                        <textarea name="message" class="textarea-message hight-82 form-control  form-issues" placeholder="Write your message here" rows="4"></textarea>
                                     </div>

                                      <div class="textarea-message form-group">
                                        <input type="submit" value="Send" />
                                     </div>
                        </form>
                    </div>

                
                  @endforeach


              @endif
                  </div><!-- /.custom-box-comment -->
                @endforeach
                </div><!-- /.custom-box-footer -->
                     </div><!-- /.box -->
            </div><!-- /.col -->
              
            </div>
        </div> 


  
@stop

@section('comment_script')
<script>
$(document).ready(function(e) {
  $('.comment_box').hide();
  $('.comment_header').on('click', function(e) {
      $('#' + $(this).data('index')).toggle();
  });

  //for responses to  a Comment
  $('.response_comment_box').hide();
  $('.response_comment_header').on('click', function(e) {
      $('#' + $(this).data('index')).toggle();
  });

});


</script>

@stop




@section('vue-section')
<script src="{{asset('assets/js/vue.js')}}"></script>
    <script type="text/javascript">
        new Vue({

        el: '',

        data:{


         textbox1 : '',
         textbox2 : '',
        
        },

        methods:{

            showTextBox1: function(){
                this.textbox1 = 'activated';
            },

            showTextBox2: function(){
                this.textbox2 = 'activated';
            }
        }

    });  

</script>
@stop