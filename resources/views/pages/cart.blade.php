@extends('app')

@section('content')


 <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                <table class="cart-table table wow fadeInLeft" cellspacing="0" class="shop_table cart">
                                    <thead>
                                        <tr>
                                            <th class="product-remove">&nbsp;</th>
                                            <th class="product-thumbnail">&nbsp;</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                         @foreach($products as $product)
                                        <tr class="cart_item">
                                            <td class="product-remove">
                                                <a title="Remove this item" class="remove" href="/store/removeitem">× Remove Item</a> 
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="single-product.html">{!!Html::image($product->image, $product->name, array('width'=>'145', 'height'=> '145','class' => 'shop-thumbnail'))!!}</a>
                                            </td>

                                            <td class="product-name">
                                                <a href="single-product.html">{!! $product->name !!}</a> 
                                            </td>

                                            <td class="product-price">
                                                <span class="amount">{!!$product->price!!}</span> 
                                            </td>

                                            <td class="product-quantity">
                                                <div class="quantity buttons_added">
                                                    <input type="button" class="minus" value="-">
                                                    <input type="number" size="4" class="input-text qty text" title="Qty" value="{!! $product->quantity !!}" step="1">
                                                    <input type="button" class="plus" value="+">
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="amount">Subtotal: {!! Cart::total(); !!}</span> 
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="actions" colspan="6">
                                                <div class="coupon">
                                                    <label for="coupon_code">Coupon:</label>
                                                    <input type="text" placeholder="Coupon code" value="" id="coupon_code" class="input-text" name="coupon_code">
                                                    <input type="submit" value="Apply Coupon" name="apply_coupon" class="button">
                                                    <input type="hidden" name="cmd" value="_xclick" >
                                                    <input type="hidden" name="business" value="lillyalfonso@icloud.com" >
                                                    <input type="hidden" name="item_name" value="Lilly Design Purchases" >
                                                    <input type="hidden" name="amount" value="1.00" >
                                                    <input type="hidden" name="first_name" value="{{Auth::user()->firstname}}" >
                                                    <input type="hidden" name="last_name" value="{{Auth::user()->lastname}}" >
                                                    <input type="hidden" name="email" value="{{Auth::user()->email}}" >
                                                    
                                                </div>
                                                {!!Html::link('/products', 'Continue Shopping',array('class'=>'cart-btn button'))!!}
                                                <input type="submit" value="Proceed to Checkout" name="proceed" class="cart-btn checkout-button button alt wc-forward">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
@stop