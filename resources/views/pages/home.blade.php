@extends('app')

@section('content')
<!-- Hero Area Section -->

<section id="hero-area">
<div class="col-md-12">
    <div class="row">
<div class="col-md-12">
        <!-- <h1 class="title">Corporal - Bootstrap Theme</h1> -->
        <!-- <h2 class="subtitle">Start your Business or Personal site right now!</h2> -->

    <div class="col-md-6 col-sm-6 col-xs-12 ">
                 
                 <img src="{{url('assets/img/hero/hero.jpg')}}" />
  
     </div>

        <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInRight delay-0-5">
       
            <div class="search-section">
                <span class="btn btn-common btn-lg black">Search By Councillor</span>
                <input type="name" class="form-control form_h" id="exampleInputAmount" placeholder="Type the councillor and press enter">
            </div>
            <div class="search-section">
                 <span class="btn btn-primary btn-lg gold">Search by Ward</span>
                  <input type="name" class="form-control form_h" id="exampleInputAmount" placeholder="Type your ward and press enter">
            </div>

                        <div class="search-section">
                        <span href="#" class="btn btn-primary btn-lg green">Search by Map</span>
                        <div id="map-canvas" data-lat="-13.9669200 " data-lng="33.7872500" data-zoom="7">

                        </div>
                        <div class="addresses-block">
                            <a data-lat="-13.9669200 " data-lng="33.7872500" data-string="Find us here. You will be most welcome."></a>
                            
                        </div>
                        </div> 
            </div>

    </div>
</div>
</section>

<!-- Hero Area Section End-->



<!-- Service Section -->

<section id="city">
<div class="overlay-black">
<div class="container text-center">
<div class="row">
<h1 class="title">How to contribute</h1>
<h2 class="subtitle">Let your views and thoughts be heard</h2>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="service-item">
    <img src="{{url('assets/img/services/responsive.png')}}" alt="">
    <h3>Start</h3>
    <p>Get started by signing up on this site</p>
    </div>
    </div>


    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="service-item">
    <img src="{{url('assets/img/services/bs3.png')}}" alt="">
    <h3>Have your say</h3>
    <p>Let your views and thoughts on important issues be expressed to </p>
    </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="service-item">
    <img src="{{asset('assets/img/services/free.png')}}" alt="">
    <h3>Influence</h3>
    <p>The results determine the effectiveness of service delivery</p>
    </div>
    </div>

</div>
</div>
</div>
 </section>
<!-- Service Section End -->










@foreach($polls as $poll)
{{$poll->title}}

{!! Form::open(['url'=>'pollVote','files'=> true, 'class'=>'login-form']) !!}
 <div class="form-group">
       <div class="col-sm-9">
             @foreach($poll->polloptions as $pollOption)
         		{!! Form::hidden('poll_id',$poll->id) !!}
				{!! Form::radio('poll_option', $pollOption->id) !!} {{$pollOption->title}}
			 @endforeach
        </div>
  </div>
<div class="form-bottom">
                            <span class="pull-left"></span>
{!! Form::submit('Create Poll', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
 </div>
{!! Form::close() !!}

<a href="{{url('viewresults',$poll->id)}}">View Results</a>

@endforeach

@stop