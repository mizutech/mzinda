@extends('app')


@section('extra_css')

@stop

@section('content')
<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div class="8u">
							
					
							<!-- Content -->
								<article class="box-article post tile">

								

								<div class="blog-img-section"
									<a href="#" class="image featured"><img class="" src="{{url($blog->image)}}" alt="" width="320"/></a>
									<span class="caption">"{!!$blog->image_caption!!}"</span>
								</div>
								<div class="blog-content">
								<header>
									<h2>{{$blog->title}}</h2>
										
										  <?php $day = date_parse($blog->published_at); 
                                                  //Get the month
                                                    $monthNum=$day['month'];
                                                    //Change the month from a numerical value to actual month name
                                                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                                    $monthName = $dateObj->format('F'); 
                            		?>
									<p> 
										
				                        <span class="day">{{$day['day']}}</span>
				                        <span class="month">{{$monthName}}</span>
				                         <span class="day">{{$day['year']}}</span>
				                    </p>
									</header>
									<p>
										{!!$blog->body!!}
									</p>									
								</div>	
									
								</article>
					

						</div>
						
					</div>
				</div>
			</div>
@stop

@include('pages.social')