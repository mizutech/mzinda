@extends ('app')


@section('content')
<div> Results for <span></span></div>

	<div id="search-results">
		<ul class="pro-grid pro-cat-slider row">
          @foreach($products as $product)
          <li class="col-lg-3">
            <div class="pro-box">
              <div class="pro-img">
                <div class="pro-hover"> 
                  
                  {!! Form::open(array('url'=>'store/addtocart'))!!}
                    {!! Form::hidden('quantity','1')!!}
                    {!! Form::hidden('id',$product->id)!!}
                    <button type="submit" class="cart-btn">
                      <i class="fa fa-shopping-cart"></i> 
                      Add to Cart
                      </button>
                    {!!Form::close()!!} 
                  <p> <a href="store/view/{{ $product->id }}">Add to Compare</a><br>
                    <a href="store/view/{{ $product->id }}">Add to Wishlist</a> </p>
                </div>
                {!! Html::image($product->image, $product->title)!!} </div>
              <div class="pro-rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i></div>
              <h3><a href="#">{{$product->title}}</a></h3>
              <strong>$55</strong> </div>
          </li>
          @endforeach
         </ul>
	</div>

@stop