@extends('app')
@section('content')
<section class="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <h2 class="main-title">Contact Us</h2>
          <p class="p">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-lg-8">
          <div class="contact-form">
            <form role="form" method="post" >
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" name="Name" placeholder="Name" required>
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" name="InputName" placeholder="Email" required>
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span> </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" name="InputName" placeholder="Phone" required>
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span> </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" name="InputName"  placeholder="Subject" required>
                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span> </div>
                </div>
              </div>
              <div class="col-md-6 text-area">
                <div class="form-group">
                  <div class="input-group textarea">
                    <textarea name="InputMessage" id="InputMessage" class="form-control" placeholder="Message" rows="11" required></textarea>
                  </div>
                </div>
                              <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">

                
                
                
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-4">
          <address>
          <ul>
            <li>
              <p class="first">Any queries,requests or special orders. Don't hesitate,contact us on +265 888 888 888.</p>
            </li>
            <li class="add">
              <p>Corporate Mall, Next to Road Traffic,
               Along Paul Kagame Highway, Lilongwe
              </p>
            </li>
            <li class="tell">
              <p>+265 999 201 886</p>
            </li>
            <li class="skype">
              <p>+265 999 201 886</p>
            </li>
            <li class="email">
              <p><a href="mailto:info@pinkworldmw.com">info@pinkworldmw.com</a></p>
            </li>
          </ul>
          </address>
        </div>
      </div>
      
      
      <div class="row">
      <div class="col-lg-12 gmap"><div id="map-canvas" data-lat="-13.9669200 " data-lng="33.7872500" data-zoom="7"></div></div>
      
      </div>
      
      
    </div>
  </section>

@stop