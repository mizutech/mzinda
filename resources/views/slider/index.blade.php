@extends('adminApp')
@section('css')
<link rel="stylesheet" href="{{ asset('/assets/css/slider-images.css') }}">
@stop


@section('header')
<h1>
     Slider Manager
    <small>Manage the slider on the front page</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')


           
            <?php $count = 0;?>
               <div class="col-sm-12">
                        <!-- start: TABLE WITH slider_images PANEL -->
                   @foreach($slider_images as $slider_image)
                  <?php $count++ ?>
                    <div class="box box-primary">
                       <div class="box-header with-border">
                          <h3 class="box-title">Slide {{$count}}</h3>
                          <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        
                          <div class="box-body">
                           <table class="table table-striped table-hover" id="sample-table-2">
                              <thead>

                                <tr>
                             
                                  <th>Change Details</th>
                                
                                  <th class="hidden-xs">Active</th>
                                  <th></th>
                                </tr>

                              </thead>
                              <tbody>
                               
                                <tr>
                               


                                {!! Form::model($slider_image, ['method' => 'PATCH','files'=> true,'action' => ['SliderImagesController@update', $slider_image->id]]) !!}
                                   {!! Form::hidden('id', $slider_image->id) !!}
                            


                                  <td >

                                    <div class="form-group">


                                        <div class="col-sm-8">
                                             {!! Form::label('slider_image','Upload an image')!!}
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"><img src="{{url($slider_image->name)}}" alt=""/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div>
                                              <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                 
                                                  {!! Form::file('slider_image',array('multiple'=>true))!!}
                                              </span>
                                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                  <i class="fa fa-times"></i> Remove
                                                </a>
                                              </div>
                                            </div>
                                        </div>
                                    </div>  

                                 
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      
                                          {!! Form::label('top_text','Top text')!!}
                                          {!! Form::text('top_text',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                <div class="col-sm-6">
                                  <div class="form-group">                                     
                                          {!! Form::label('main_text','Main Text')!!}
                                          {!! Form::text('main_text',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                 <div class="col-sm-6">
                                      <div class="form-group">      
                                          {!! Form::label('sub_text','Sub Text')!!}
                                          {!! Form::text('sub_text',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                  <div class="col-sm-6">
                                      <div class="form-group">      
                                          {!! Form::label('button_text','Button Text')!!}
                                          {!! Form::text('button_text',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                    <div class="col-sm-6">
                                      <div class="form-group">   
                                          {!! Form::label('link','Slide Link')!!}
                                          {!! Form::text('link',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                  <div class="col-sm-6">
                                      <div class="form-group">   
                                          {!! Form::label('text_active','Text Viewable')!!}
                                          {!! Form::select('text_active', ['1'=>'Show Text on Slider','0'=>'No Text on Slider'],$slider_image->text_active,['class'=>'form-price form-control'] ) !!}
                                        
                                      </div>
                                  </div>

                                
                                        </td>

                               


                              <td class="hidden-xs">


                                 {!! Form::select('active', ['1'=>'Active','0'=>'Inactive'],$slider_image->active ) !!}

                                 
                                  </td>
                                  <td class="center">
                                  <div class="visible-md visible-lg hidden-sm hidden-xs">

                                  {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                  <p> </p>

                                 {!! Form::close() !!}
                                    
                                            {!! Form::open(['url'=>'admin/slider/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['SliderImagesController@delete', $slider_image->id]]) !!}
                                            {!! Form::hidden('id', $slider_image->id) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                  </div>
                                  <div class="visible-xs visible-sm hidden-md hidden-lg">
                                    <div class="btn-group">
                                      <a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                      </a>
                                      <ul role="menu" class="dropdown-menu pull-right dropdown-dark">
                                        <li>
                                          <a role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-edit"></i> Edit
                                          </a>
                                        </li>
                                        {{-- <li>
                                          <a role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-share"></i> Share
                                          </a>
                                        </li> --}}
                                        <li>

                                        </li>
                                      </ul>
                                    </div>
                                  </div></td>
                                </tr>
                              
                              </tbody>
                            </table>
                          </div>
                       
                    </div>
                     @endforeach
                   
            </div>
             
                    



</div><!--end admin-->


@stop
