<section id="register" class="register">				
	    <div class="container">
	      <div class="row">
	        
	        <div class="col-lg-12">
	          <div class="login-box login-page">
	            <h3>Edit a collection: {{$collection->name}} </h3>
			          @if($errors->has())
			          <div id="form-errors">  
			            <p>The following errors have occured</p> 
			            <ul>
			              @foreach($errors->all() as $error)
			               <li>{{ $error }}</li>
			              @endforeach
			              </ul>
			             </div><!--end form errors-->    
			          @endif

			    {!! Form::model($collection, ['method' => 'PATCH','files'=>true,'action' => ['CollectionsController@update', $collection->id]]) !!}
					   <div class="form-group">
					      <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
					    {!! Form::label('name',null,['class'=>'form-control']) !!}
					    {!! Form::text('name',null,['class'=>'form-control']) !!}
							 </div> 
						 </div>	 
					 <div class="form-group">
			                    <div class="col-sm-9">
			                        {!! Form::label('image','Choose a thumbnail to represent the collection')!!}
			                        {!! Form::file('thumbnail',null,['class'=>'form-price form-control'])!!}
			                        </div>
			         </div>
			  
				 <div class="form-bottom">
			              <span class="pull-left">

			              </span>
			    {!! Form::submit('Save Collection', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
			 	  </div>
			            
			            
			     {!! Form::close() !!}
	         
	        </div>
	      </div>
	    </div>
	  </section>