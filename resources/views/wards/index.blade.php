@extends('adminApp')

@section('header')
<h1>
     Ward Content
    <small>Manage the wards</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')
<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Ward List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
      <div class="box-body">
										<table class="table table-striped table-hover" id="sample-table-2">
											<thead>
												<tr>
													<th class="center">#</th>
													<th>Name</th>
													<th>Size</th>
													<th>Population</th>
													<th>Latitude</th>
													<th>Longitude</th>
													<th>Image</th>
													
													
													<th></th>
												</tr>
											</thead>
											<tbody>
												 @foreach($wards as $ward)
												<tr>
													<td class="center"> {{$ward->district->name}}</td>
													<td class="hidden-xs">{{$ward->name}}</td>
													<td class="hidden-xs">{{$ward->size}}</td>
													<td class="hidden-xs">{{$ward->population}}</td>
													<td class="hidden-xs">{{$ward->latitude}}</td>
													<td class="hidden-xs">{{$ward->longitude}}</td>
													<td class="hidden-xs"><img width="100"src="{{url($ward->image)}}"/></td>

													<td class="center">
													<div class="visible-md visible-lg hidden-sm hidden-xs">
														
						                                  <a href="{{url('admin/wards/'.$ward->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a>
						                                
														     {!! Form::open(['url'=>'admin/wards/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['WardsController@delete', $ward->id]]) !!}
																	  {!! Form::hidden('id', $ward->id) !!}
	 																{!! Form::submit('Delete',array('class' => 'btn btn-danger')) !!}
																	  {!! Form::close() !!}
													</div>
													<div class="visible-xs visible-sm hidden-md hidden-lg">
														<div class="btn-group">
															<a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
																<i class="fa fa-cog"></i> <span class="caret"></span>
															</a>
															<ul role="menu" class="dropdown-menu pull-right dropdown-dark">
																<li>
																	<a role="menuitem" tabindex="-1" href="#">
																		<i class="fa fa-edit"></i> Edit
																	</a>
																</li>
																<li>
																	<a role="menuitem" tabindex="-1" href="#">
																		<i class="fa fa-share"></i> Share
																	</a>
																</li>
												</div>
															</div>
														</td>
													</tr>					
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<!-- end: BASIC TABLE PANEL -->
      </div>
    </div>
                        <!-- end: TABLE WITH IMAGES PANEL -->


                    
 
      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Ward Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('wards.edit')
                @else
                 @include('wards.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->
		
			
			
		
		</div>
 
</div><!--end admin-->
	 

@stop