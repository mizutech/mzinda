
                        <h3>Create new Councillor</h3>
                      @if($errors->has())
                      <div id="form-errors">  
                        <p>The following errors have occured</p> 
                        <ul>
                          @foreach($errors->all() as $error)
                            <li class="callout callout-danger">{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->    
                      @endif
                
                
                {!! Form::open(['url'=>'admin/wards','files'=> true, 'class'=>'login-form']) !!}
         
               <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('Specify the category in the box below',null,['class'=>'form-control']) !!}
                        {!! Form::select('district_id',$districts,['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>

               

                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('name','Name')!!}
                        {!! Form::text('name', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('population','Population')!!}
                        {!! Form::text('population', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-12">
                    <div class="form-group"> 
                        {!! Form::label('size','Size')!!}
                        {!! Form::text('size', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('latitude','Latitude')!!}
                        {!! Form::text('latitude', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('longitude','Longitude')!!}
                        {!! Form::text('longitude', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 


                  <div class="col-sm-12">
                   <div class=" form-group">
                       {!! Form::label('image',' Image')!!}
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail"><img src="{{url('images/default-upload.png')}}" alt=""/>
                          </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <div>
                             <span class="btn btn-primary btn-file">
                                 <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                 <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                     
                                  {!! Form::file('image')!!}
                              </span>
                               <a href="#" class="btn fileupload-exists btn-primary" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                       </div>
                   </div>
                   </div>

              



                 <div class="box-footer">
                    {!! Form::submit('Save Ward', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}               
                  