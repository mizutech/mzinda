@extends('adminApp')

@section('css')

<link rel="stylesheet" href="{{asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">

@stop

@section('header')
<h1>
     Products
    <small>Manage the products in your store</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop

@section('content')

<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Product List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="products" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Main Image</th>
                        <th>Item Code</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Visibility</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                      <tr> 
                        <td>
                        @if($product->product_images->first())
                        <img src="{{url($product->product_images->first()->front)}}" width="50px" height=auto /></td>
                        @endif
                        <td>{{$product->item_code}}</td>
                        <td>{{$product->title}}</td>
                        <td>{{$product->price}}</td>
                        <td>   {!! Form::open(['url'=>'admin/products/updateAvailability', 'class'=>'form-inline','action' => ['ProductsController@updateAvailability', $product->id]]) !!}
                               {!! Form::hidden('id', $product->id) !!}
                               {!! Form::select('availability', ['1'=>'In Stock','0'=>'Out of Stock'],$product->availability ) !!}
                               {!! Form::submit('Update') !!}
                               {!! Form::close() !!}
                        </td>
                        <td>
                              {!! Form::open(['url'=>'admin/products/updateVisible', 'class'=>'form-inline','action' => ['ProductsController@updateVisible', $product->id]]) !!}
                               {!! Form::hidden('id', $product->id) !!}
                               {!! Form::select('visible', ['1'=>'Showing','0'=>'Not Showing'], $product->visible ) !!}
                               {!! Form::submit('Update') !!}
                               {!! Form::close() !!}
                        </td>
                        <td><a href="{{url('admin/products/'.$product->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a></td>
                        <td>   {!! Form::open(['url'=>'admin/products/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['productsController@delete', $product->id]]) !!}
                                            {!! Form::hidden('id', $product->id) !!}
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                                                               
                                            {!! Form::close() !!}
                        </td>
                      </tr>
                      
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Main Image</th>
                        <th>Item Code</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Visibility</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->



      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Product Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('products.edit')
                @else
                 @include('products.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->

@stop

@section('scripts')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"> </script>
<script src="{{asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"> </script>

<script>
      $(function () {
        $("#products").DataTable();
        });
    </script>
@stop

