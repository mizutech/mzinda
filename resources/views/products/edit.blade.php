
                        <h3>Edit a Product:{{$edit_product->name}}</h3>
                      @if($errors->has())
                      <div id="form-errors">
                        <p>The following errors have occured</p>
                        <ul>
                          @foreach($errors->all() as $error)
                          <li class="callout callout-danger">{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->
                      @endif


            {!! Form::model($edit_product,['method' => 'PATCH','files'=>true,'action' => ['ProductsController@update', $edit_product->id]]) !!}
         
                    <div class="col-sm-9">
                        <div class="form-group">
                    
                        {!! Form::label('category_id','Product Category') !!}
                        {!! Form::select('category_id',$categories,null,['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>
               <div class="col-sm-9">
                        <div class="form-group">
                      
                        {!! Form::label('collection_id','Product Collections') !!}
                        {!! Form::select('collection_id',$collections,null,['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>

                <div class="col-sm-9">
                      <div class="form-group">
                        {!! Form::label('title')!!}
                        {!! Form::text('title', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-9">
                   <div class="form-group">
                        {!! Form::label('item code')!!}
                        {!! Form::text('item_code', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                <div class="col-sm-9">
                      <div class="form-group">
                        {!! Form::label('A short version of the main description below')!!}
                        {!! Form::textarea('desc_excerpt',null,['class'=>'form-control' , 'id'=>'form-field-1','cols'=>'6', 'rows'=>'6'])!!}
                      </div>
                  </div>
                  <div class="col-sm-9">
                      <div class="form-group">
                        {!! Form::label('description', 'Description')!!}
                        {!! Form::textarea('description',null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                  </div>
                  <div class="col-sm-9">
                      <div class="form-group">
                          {!! Form::label('price')!!}
                          {!! Form::text('price',null,['class'=>'form-price form-control'])!!}
                      </div>
                  </div>
                    <!--==Front Image==-->
                  <div class="form-group">
                      <div class="col-sm-12">
                         {!! Form::label("images['front']",'Choose the Front')!!}
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail"><img src="{{url($edit_product->product_images->first()->front)}}" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                          <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                          <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                      {!! Form::file("images['front']")!!}
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-info" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                        </a>
                                    </div>
                            </div>
                     </div>
                 </div>
                 <!--==End:Front Image==-->
                 <!--==Side Image==-->
                   <div class="form-group">
                     <div class="col-sm-12">
                         {!! Form::label("images['side']",'Choose the Side')!!}
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail"><img src="{{url($edit_product->product_images->first()->side)}}" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                          <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                          <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                      {!! Form::file("images['side']")!!}
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-info" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                        </a>
                                    </div>
                            </div>
                     </div>
                  </div>
                  <!--==End:Side Image==-->
                  <!--==Back Image==-->
                   <div class="form-group">
                       <div class="col-sm-12">
                        {!! Form::label("image['back']",'Choose the Front')!!}
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail"><img src="{{url($edit_product->product_images->first()->back)}}" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                          <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                          <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                      {!! Form::file("images['back']")!!}
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-info" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                        </a>
                                    </div>
                            </div>
                     </div>
                 </div>
                <!--==End:Back Image==-->
                <!--==Full Image==-->
                 <div class="form-group">
                       <div class="col-sm-12">
                        {!! Form::label("images['full']",'Choose the Back')!!}
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail"><img src="{{url($edit_product->product_images->first()->full)}}" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                          <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                          <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                      {!! Form::file("images['full']")!!}
                                        </span>
                                        <a href="#" class="btn fileupload-exists btn-info" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                        </a>
                                    </div>
                            </div>
                     </div>
                 </div>
                <!--==End:Full Image==-->
                   <div class="col-sm-9">
                        <div class="form-group">
                          {!! Form::label('ptag_list','Product Tags:')!!}
                          {!! Form::select('ptag_list[]', $ptags, null, ['class'=>'select2 form-control' ,'multiple', 'id'=>'form-field-1'])!!}
                        </div>
                  </div>
                  <div class="col-sm-9">
                        <div class="col-sm-9">
                          {!! Form::label('product_size_list','Sizes:')!!}
                          {!! Form::select('product_size_list[]', $product_size, null, ['class'=>'select2 form-control' ,'multiple', 'id'=>'form-field-1'])!!}
                        </div>
                  </div>
                   
  </div> <!--Closing box-body div from index file-->
              
                 <div class="box-footer">
                    {!! Form::submit('Save Product', ['class'=>'btn btn-primary', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}
               
                    
                  
