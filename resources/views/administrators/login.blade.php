<!DOCTYPE html>
<html>
  <head>
    <meta name="robots" content="noindex">
    <meta name="robots" content="nofollow" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mzinda Admin | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('admin/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/square/blue.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<!-- BEGIN BODY -->
<body class="hold-transition login-page">
  

   <div class="login-box">

    <div class="login-logo">
        <img src="{{url('images/your_logo.png')}}"> <a href="#"><b>Admin</b></a>
      </div><!-- /.login-logo -->
                <!-- start: LOGIN BOX -->
        <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
                   

                    <form class="form-login" action="{{url('/administrator/auth/login')}}" method="post">
                        <div class="errorHandler">
                             <div class="grid-body no-border"> <br>
                                    @if (count($errors) > 0)
                                        <div >
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="alert alert-danger">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(Session::has('error_msg'))
                                        <p class="alert alert-danger">{{ Session::get('error_msg') }}</p>
                                    @endif
                        </div>
                      

                         <input type="hidden" name="_token" value="{{csrf_token()}}"/>


                            <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>

                            <div class="form-group has-feedback">
                                
                                    <input type="password" class="form-control password" name="password" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>

                            <div class="row">
                                <div class="col-xs-8">
                                  <div class="checkbox icheck">
                                    <label>
                                      <input type="checkbox" id="remember" name="remember"> Remember Me
                                    </label>
                                  </div>
                                </div><!-- /.col -->

                                <div class="col-md-12">
                                    <label class="form-label">Role</label>
                                    <select class="form-control" name="role">

                                        <option value="Administrator" >Administrator</option>
                                        <option value="Manager" >Manager</option>
                                        <option value="Accountant" >Accountant</option>

                                    </select>
                               </div><!-- /.col -->

                                <div class="col-xs-4">
                                  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                                </div><!-- /.col -->
                            </div>

                           
                       
                    </form>
               
                </div>
                <!-- end: LOGIN BOX -->
                <!-- start: FORGOT BOX -->
                <a href="#">I forgot my password</a><br>
                <!-- end: FORGOT BOX -->


            </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

   <!-- jQuery 2.1.4 -->
    <script src="{{asset('admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('admin/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>