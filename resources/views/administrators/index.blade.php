@extends('adminApp')

@section('header')
<h1>
     Administrators
    <small>Manage those who can access the control panel</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop

@section('content')


        <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Administrator List</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="grid simple ">
                                <div class="grid-title">
                                    <h4> <span class="semi-bold">Administrators</span></h4>
                                    <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>

                                </div>

                                <div class="grid-body ">
                                   <a href="{{url('/administrator/create_admin')}}"> <button class="btn btn-primary"  style="margin-bottom:20px" id="test2">Add Administrator</button></a>
                                    <table class="table" id="example3" >
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Edit</th>
                                            <th>Delete</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach ($admins as $admin)

                                        <tr class="odd gradeX">
                                            <td>{{$admin->firstname}}</td>
                                            <td>{{$admin->email}}</td>
                                            <td>{{$admin->role}}</td>
                                            <td><a href="{{url('/administrator/edit_admin/'.$admin->id)}}"> <button type="button" class="btn btn-primary btn-sm">Edit</button></a></td>
                                            <td><a href="{{url('/administrator/delete_admin/'.$admin->id)}}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a></td>

                                        </tr>

                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div><!-- /.col -->
@stop