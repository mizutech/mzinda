@extends('adminApp')

@section('header')
<h1>
    Articles
    <small>Manage the articles</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop

@section('content')

<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Article List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                           <table class="table table-striped table-hover" id="sample-table-2">
                              <thead>

                                <tr>
                                 
                                  <th class="center">Photo</th>
                                  <th>Excerpt</th>
                                  <th class="hidden-xs">Body</th>
                                  
                                  <th class="hidden-xs">Tags</th>
                                  <th>Active</th>
                                </tr>

                              </thead>
                              <tbody>
                               @foreach($articles as $article)
                                <tr>
                              



                                  <td class="center"> {!!Html::image($article->image ,$article->title,array('width'=>'50'))  !!}</td>
                                  <td>{{$article->excerpt}}</td>
                                  <td class="hidden-xs">{{$article->body}}</td>
                     

                                  <td class="hidden-xs">
                                 {!! Form::open(['url'=>'admin/articles/updateActive', 'class'=>'form-inline','action' => ['articlesController@updateActive', $article->id]]) !!}
                                 {!! Form::hidden('id', $article->id) !!}
                                 {!! Form::select('availability', ['1'=>'Active','0'=>'Inactive'],$article->active ) !!}
                                 {!! Form::submit('Update') !!}
                                 {!! Form::close() !!}
                                  </td>
                                  <td class="center">
                                  <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    <a href="{{URL::to('admin/articles/'.$article->id.'/edit')}}" class="btn btn-primary tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i>Edit</a>
                                    </div>
                                  </td>

                                  <td>
                                            {!! Form::open(['url'=>'admin/articles/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['articlesController@delete', $article->id]]) !!}
                                            {!! Form::hidden('id', $article->id) !!}
                                            {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                  </td>
                                  <div class="visible-xs visible-sm hidden-md hidden-lg">
                                    <div class="btn-group">
                                      <a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                      </a>
                                      <ul role="menu" class="dropdown-menu pull-right dropdown-dark">
                                        <li>
                                          <a role="menuitem" class="btn btn-primary tooltips" tabindex="-1" href="#">
                                            <i class="fa fa-edit"></i> Edit
                                          </a>
                                        </li>
                                 
                                        <li>

                                        </li>
                                      </ul>
                                    </div>
                                  </div></td>
                                </tr>
                               @endforeach
                              </tbody>
                            </table>
                       </div><!-- /.box-body -->
              </div><!-- /.box -->



      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Article Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('articles.edit')
                @else
                 @include('articles.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->

@stop
        
