                        <h3>Create a new Article</h3>
                      @if($errors->has())
                      <div id="form-errors">
                        <p>The following errors have occured</p>
                        <ul>
                          @foreach($errors->all() as $error)
                           <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->
                      @endif


                {!! Form::open(['url'=>'admin/articles','files'=> true, 'class'=>'login-form']) !!}

                {!! Form::hidden('user_id', 1)!!}
                <div class="form-group">
                      <div class="col-sm-9">
                        {!! Form::label('title','Title')!!}
                        {!! Form::text('title', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div>

                
                <div class="form-group">
                      <div class="col-sm-9">
                        {!! Form::label('excerpt','A short version of the main details below')!!}
                        {!! Form::textarea('excerpt',null,['class'=>'form-control' , 'id'=>'form-field-1','cols'=>'4', 'rows'=>'2'])!!}
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-sm-9">
                        {!! Form::label('body','Body')!!}
                        {!! Form::textarea('body',null,['class'=>'ckeditor form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                  </div>
                  <div class="col-sm-4">
                  <div class=" form-group">
                       {!! Form::label('images','Article Image')!!}
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail"><img src="{{url('images/default-upload.png')}}" alt=""/>
                          </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <div>
                             <span class="btn btn-primary btn-file">
                                 <span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span>
                                 <span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                     
                                  {!! Form::file('main_image')!!}
                              </span>
                               <a href="#" class="btn fileupload-exists btn-primary" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                       </div>
                   </div>
             </div> 
             <div class="form-group">
                      <div class="col-sm-5">
                        {!! Form::label('image_caption','Caption for the Image')!!}
                        {!! Form::textarea('image_caption',null,['class'=>'form-control' , 'id'=>'form-field-1','cols'=>'4', 'rows'=>'2'])!!}
                      </div>
                  </div>
  <div class="form-group">
                      <div class="col-sm-9">
                        {!! Form::label('published_at','Published At')!!}
                        {!! Form::input('date','published_at',date('Y-m-d'),['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div>
                      
        </div> <!--Closing box-body div from index file-->
              
                 <div class="box-footer">
                    {!! Form::submit('Save Article', ['class'=>'btn btn-primary', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}
