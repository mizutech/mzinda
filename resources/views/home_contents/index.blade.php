@extends('adminApp')
@section('css')
<link rel="stylesheet" href="{{ asset('/assets/css/slider-images.css') }}">
@stop


@section('header')
<h1>
     HomePage Content
    <small>Manage the content on the front page</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')


           
            <?php $count = 0;?>
               <div class="col-sm-12">
                        <!-- start: TABLE WITH slider_images PANEL -->
                   @foreach($home_contents as $home_content)
                  <?php $count++ ?>
                    <div class="box box-primary collapsed-box">
                       <div class="box-header with-border">
                          <h3 class="box-title">Section {{$count}} / {{$home_content->heading}} </h3>
                          <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                          </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        
                          <div class="box-body">
                           <table class="table table-striped table-hover" id="sample-table-2">
                              <thead>

                                <tr>
                             
                                  <th>Change Details(NB:Tick remove image to delete image without adding another)</th>
                                
                                  <th class="hidden-xs">Active</th>
                                  <th></th>
                                </tr>

                              </thead>
                              <tbody>
                               
                                <tr>
                               


                                {!! Form::model($home_content, ['method' => 'PATCH','files'=> true,'action' => ['HomeContentsController@update', $home_content->id]]) !!}
                                   {!! Form::hidden('id', $home_content->id) !!}
                            


                                  <td >

                                    <div class="form-group">


                                        <div class="col-sm-8">
                                             {!! Form::label('image','Upload an image(dimensions are multiples of 200 x 200 or 689 x 414,both will work fine)')!!}
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail"><img src="{{url($home_content->image)}}" alt=""/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div>
                                              <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select/Change image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                 
                                                  {!! Form::file('image',array('multiple'=>true))!!}
                                              </span>
                                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                  <i class="fa fa-times"></i> Remove
                                                </a>
                                              </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                  <div class="form-group">                                     
                                          {!! Form::label('remove_image','Remove image')!!}
                                          {!! Form::checkbox('remove_image',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                <div class="col-sm-6">
                                  <div class="form-group">                                     
                                          {!! Form::label('heading','Heading')!!}
                                          {!! Form::text('heading',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                 <div class="col-sm-6">
                                      <div class="form-group">      
                                          {!! Form::label('sub_heading','Sub Heading')!!}
                                          {!! Form::text('sub_heading',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                  <div class="col-sm-12">
                                      <div class="form-group">   
                                          {!! Form::label('content','Content')!!}
                                          {!! Form::textarea('content',null,['class'=>' ckeditor form-price form-control'])!!}
                                      </div>
                                  </div>

                                  <div class="col-sm-6">
                                      <div class="form-group">   
                                          {!! Form::label('button_active','Button Viewable')!!}
                                          {!! Form::select('button_active', ['1'=>'Active','0'=>'Inactive'],$home_content->text_active,['class'=>'form-price form-control'] ) !!}
                                        
                                      </div>
                                  </div>

                                  <div class="col-sm-6">
                                      <div class="form-group">      
                                          {!! Form::label('button_text','Button Text')!!}
                                          {!! Form::text('button_text',null,['class'=>'form-price form-control'])!!}
                                      </div>
                                  </div>

                                                             
                              </td>

                               


                              <td class="hidden-xs">


                                 {!! Form::select('active', ['1'=>'Active','0'=>'Inactive'],$home_content->active ) !!}

                                 
                                  </td>
                                  <td class="center">
                                  <div class="visible-md visible-lg hidden-sm hidden-xs">

                                  {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                  <p> </p>

                                 {!! Form::close() !!}
                                    
                                            {!! Form::open(['url'=>'admin/home-content/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['HomeContentsController@delete', $home_content->id]]) !!}
                                            {!! Form::hidden('id', $home_content->id) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                  </div>
                                  <div class="visible-xs visible-sm hidden-md hidden-lg">
                                    <div class="btn-group">
                                      <a class="btn btn-green dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                      </a>
                                      <ul role="menu" class="dropdown-menu pull-right dropdown-dark">
                                        <li>
                                          <a role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-edit"></i> Edit
                                          </a>
                                        </li>
                                        {{-- <li>
                                          <a role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-share"></i> Share
                                          </a>
                                        </li> --}}
                                        <li>

                                        </li>
                                      </ul>
                                    </div>
                                  </div></td>
                                </tr>
                              
                              </tbody>
                            </table>
                          </div>
                       
                    </div>
                     @endforeach
                   
            </div>
             
                    



</div><!--end admin-->


@stop
