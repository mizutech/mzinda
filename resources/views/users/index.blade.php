@extends('adminApp')

@section('css')

<link rel="stylesheet" href="{{asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">

@stop

@section('header')
<h1>
     Users
    <small>Manage the users in your store</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop

@section('content')

<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">User List</h3>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="users" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Company</th>
                        <th>State</th>
                        <th>Role</th>
                        <th>Verified</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                      <tr> 
                      
                        <td>{{$user->firstname}}</td>
                        <td>{{$user->lastname}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->company}}</td>
                        <td>{{$user->state}}</td>
                        <td>{{$user->role}}</td>
                        <td>{{$user->verified}}</td>

                        <td><a href="{{url('admin/users/'.$user->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a></td>
                        <td>   {!! Form::open(['url'=>'admin/users/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['usersController@delete', $user->id]]) !!}
                                            {!! Form::hidden('id', $user->id) !!}
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                                                               
                                            {!! Form::close() !!}
                        </td>
                      </tr>
                      
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Company</th>
                        <th>State</th>
                        <th>Role</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->



@stop

@section('scripts')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"> </script>
<script src="{{asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"> </script>

<script>
      $(function () {
        $("#users").DataTable();
        });
    </script>
@stop

