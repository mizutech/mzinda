
                        <h3>Edit a Poll:{{$edit_poll->name}}</h3>
                      @if($errors->has())
                      <div id="form-errors">  
                        <p>The following errors have occured</p> 
                        <ul>
                          @foreach($errors->all() as $error)
                            <li class="callout callout-danger">{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->    
                      @endif
                
                
            {!! Form::model($edit_poll,['method' => 'PATCH','files'=>true,'action' => ['PollsController@update', $edit_poll->id]]) !!}
         
               

                   <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('Specify the category in the box below',null,['class'=>'form-control']) !!}
                        {!! Form::select('category_id',$categories,array($edit_poll->category_id),['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>

               

                <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('title','Poll Title')!!}
                        {!! Form::text('title', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                 <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('description')!!}
                        {!! Form::textarea('description',null,['class'=>'form-control' ,'rows'=>'2', 'id'=>'form-field-1'])!!}
                      </div>
                  </div>

                  <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('image','Choose an image(Leave blank if not neccesary)')!!}
                        {!! Form::file('image',null,['class'=>'form-price form-control'])!!}
                        </div>
                  </div>

                  <div class="col-sm-9">
                    <div class="form-group"> 
                          {!! Form::label('comments_active','Enable Comments') !!}
                          {!! Form::radio('comments_active', '1') !!} Yes
                          {!! Form::radio('comments_active', '0') !!}   No
                       </div>
                  </div>
              
               

                  <div class="col-sm-9">
                    <div class="form-group"> 
                          {!! Form::label('active','Published') !!}
                          {!! Form::radio('active', '1') !!} Publish Poll
                          {!! Form::radio('active', '0') !!} Unpublish Poll
                    </div>
                   </div>

                  <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('start_date','Start Date:') !!}
                         {!! Form::input('date','start_date',date('Y-m-d'),['class' => 'required form-input']) !!}
                         </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('end_date','End Date:') !!}
                         {!! Form::input('date','end_date',date('Y-m-d'),['class' => 'required form-input']) !!}
                         </div>
                    </div>  
                  <?php $count=0 ?>
                  @foreach($poll_options as $option)
                  <?php $count++?>
                    <div class="col-sm-6">
                          <div class="form-group">
                            <label name="poll_option[{{$count}}]['title']" value="Option {{$count}}">{{$option->title}}</label>
                            <input type="text" name="poll_options[{{$count}}][title]" class="form-control" value="{{$option->title}}"/>
                            <input type="hidden" name="poll_options[{{$count}}][id]" class="form-control" value="{{$option->id}}"/>
                          </div>
                    </div>
                  @endforeach
           
              
                 <div class="form-bottom">
                            <span class="pull-left"></span>
                    {!! Form::submit('Save Poll', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}
               