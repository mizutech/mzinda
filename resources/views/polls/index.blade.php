@extends('adminApp')

@section('header')
<h1>
     Poll Content
    <small>Manage the polls</small>
</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
              </ol>
@stop
@section('content')
<div class="col-xs-12">  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title">POLL LIST</h3>
                  <div class="alert alert-info">
                  <h4>Using InActive and Archived to hide polls</h4>
                  <h5>Polls set to <b> Inactive</b> will not show up <b>anywhere<b> on the site(including the archive section).</h5>
                  <h5>Polls set to <b> Archived</b> will not show up on the <b>current poll list</b> but will be viewable in the archive section.</h5>
                  </div>
                  <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                   </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
      <div class="box-body">
                           <table class="table table-striped table-hover" id="sample-table-2">
                              <thead>
                               
                                <tr>
                                  <th class="center">Image</th>
                                  <th>Poll</th>
                                  <th class="hidden-xs">Poll Active</th>
                               
                                  <th class="hidden-xs">Poll Archived</th>
                                  <th></th>
                                </tr>

                              </thead>
                              <tbody>
                               @foreach($polls as $poll) 
                                <tr>
                                               
                                  <td class="center"> {!!Html::image($poll->image ,'No Image',array('width'=>'50'))  !!}</td>
                                  <td>{{$poll->title}}</td>
                                  <td class="hidden-xs">
                                    {!! Form::open(['url' => 'admin/polls/updateActive' ,'method' => 'POST']) !!}
                                 {!! Form::hidden('id', $poll->id) !!}
                                 {!! Form::select('active', ['1'=>'Active','0'=>'InActive'],$poll->active,['class'=> 'form-control',' onchange' => 'this.form.submit()']) !!}
                                 {!! Form::close() !!} 
                                </td>
                                  
                                
                                  <td class="hidden-xs">
                                 {!! Form::open(['url' => 'admin/polls/updateArchived' ,'method' => 'POST']) !!}
                                 {!! Form::hidden('id', $poll->id) !!}
                                 {!! Form::select('archived', ['1'=>'Force Archive','0'=>'UnArchived'],$poll->archived,['class'=> 'form-control','onchange' => 'this.form.submit()'] ) !!}
                                 {!! Form::close() !!} 
                                  </td>


                                  <td>
                                  <a href="{{url('admin/polls/'.$poll->id.'/edit')}}" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i>Edit</a>
                                  </td>
                                  <td>   
                                  {!! Form::open(['url'=>'admin/polls/destroy', 'class'=>'form-inline','method' => 'DELETE', 'action' => ['PollsController@delete', $poll->id]]) !!}
                                    
                                    {!! Form::hidden('id', $poll->id) !!}                                    
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                     
                                     {!! Form::close() !!}
                                  </td>
                      

                                </tr>
                               @endforeach 

                              </tbody>
                                <tfoot>
                                   <tr>
                                     
                                     {!! str_replace('/?', '?', $polls->render()) !!}
                                  </tr>
                                </tfoot>
                           </table>
      </div>
    </div>
                        <!-- end: TABLE WITH IMAGES PANEL -->


                    
 
      <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Poll Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                 <div class="box-body">                   
                @if(isset($edit))
                 @include('polls.edit')
                @else
                 @include('polls.create')
                @endif
                     
     </div><!-- Full box primary -->
</div><!-- /.col-xs-12 -->

   

@stop