
                        <h3>Create new Poll</h3>
                      @if($errors->has())
                      <div id="form-errors">  
                        <p>The following errors have occured</p> 
                        <ul>
                          @foreach($errors->all() as $error)
                            <li class="callout callout-danger">{{ $error }}</li>
                          @endforeach
                          </ul>
                         </div><!--end form errors-->    
                      @endif
                
                
                {!! Form::open(['url'=>'admin/polls','files'=> true, 'class'=>'login-form']) !!}
              
            <div class="row">
               <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('Specify the category in the box below',null,['class'=>'form-control']) !!}
                        {!! Form::select('category_id',$categories,null,['class'=>'form-control' , 'id'=>'form-field-1']) !!}
                        </div> 
                </div>

               

                <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('title','Poll Title')!!}
                        {!! Form::text('title', null,['class'=>'form-control' , 'id'=>'form-field-1'])!!}
                      </div>
                </div> 
                 <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('description')!!}
                        {!! Form::textarea('description',null,['class'=>'form-control' ,'rows'=>'2', 'id'=>'form-field-1'])!!}
                      </div>
                  </div>

                  <div class="col-sm-9">
                    <div class="form-group"> 
                        {!! Form::label('image','Choose an image(Leave blank if not neccesary)')!!}
                        {!! Form::file('image',null,['class'=>'form-price form-control'])!!}
                        </div>
                  </div>            
               

                  <div class="col-sm-9">
                    <div class="form-group"> 
                          {!! Form::label('active','Published') !!}
                          {!! Form::radio('active', '1') !!} Publish Poll
                          {!! Form::radio('active', '0') !!} Unpublish Poll
                    </div>
                   </div>

                  <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('start_date','Start Date:') !!}
                         {!! Form::input('date','start_date',date('Y-m-d'),['class' => 'required form-input']) !!}
                         </div>
                    </div>

                    <div class="col-sm-6">
                    <div class="form-group"> 
                        {!! Form::label('end_date','End Date:') !!}
                         {!! Form::input('date','end_date',date('Y-m-d'),['class' => 'required form-input']) !!}
                         </div>
                    </div>
              </div> 
              </hr>     
              <div class="row">
                <div class="text-center">
                <h4>At least two options must be filled in the answers section</h4>
                </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <label name="poll_options[]" value="Option 1">Option 1</label>
                        <input type="text" name="poll_options[]" class="form-control" />
                      </div>
                </div>

                <div class="col-sm-6">
                
                      <div class="form-group">
                      <label name="poll_options[]" value="Option 2">Option 2</label>
                        <input type="text" name="poll_options[]" class="form-control" />
                      </div>
                </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <label name="poll_options[]" value="Option 3">Option 3</label>
                        <input type="text" name="poll_options[]" class="form-control" />
                      </div>
                </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <label name="poll_options[]" value="Option 4">Option 4</label>
                        <input type="text" name="poll_options[]" class="form-control" />
                      </div>
                </div>
              </div>
                
                  


                 <div class="box-footer">
                    {!! Form::submit('Create Poll', ['class'=>'btn btn-info pull-right', 'id'=> 'Login']) !!}
                 </div>
                  {!! Form::close() !!}               
                  